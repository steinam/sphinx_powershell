Powershell und Klassendiagramm
===============================

Vorwort
--------


In der objektorientierten Programmierung ist das Klassendiagramm ein zentrales Diagramm der Modellierung. Viele Programmiersprachen lehnen sich eng an dieses Konzept an und bieten innerhalb ihrer Syntax eine entsprechende Umsetzung der jeweiligen Begriffe des Klassendiagramms an.

Dieses Skript ist deshalb zweigeteilt: Es werden sowohl die grundlegenden Begriffe der OOP bzgl. des Klassendiagramms erläutert als auch der Versuch unternommen, diese realtitätsnah und praxisbezogen mit Hilfe von Powershell darzustellen. 

Der exemplarische Einsatz der Programmiersprache Powershell hat dabei allerdings einen Nachteil. Powershell kann von seiner Syntax nicht alle Konzepte des KD 1:1 abbilden bzw. man muss dies über Mechanismen erreichen, die in der reinen Welt von java/php/c#/c++ eben nicht notwendig wären.

Da Powershell auf das .NET-Framework aufsetzt, ist prinzipiell jede Feature von OOP umsetzbar; häufig wird ein Skripter aber gar nicht die Notwendigkeit sehen, diese Feature einsetzen zu wollen. Allerdings wird er häufig mit Objekten aus der .NET-Welt arbeiten und damit viele Elemente des Klassendiagramm nutzen (ohne es wissen zu müssen bzw zu wollen)


ERM - Klassendiagramm
----------------------


Da in der 11. Klasse das Thema **Entity Relationship Model** bereits behandelt wurde, fällt ein Einstieg in die Begriffswelt des Klassendiagrammes relativ leicht. 

.. admonition:: Einfache Definition Klassendiagramm

	Ein Klassendiagramm ist ein Entity Relationship Modell, welches um Methoden erweitert wird.
	
Diese Aussage trifft in seiner Kürze des Kern und ist für einen Skripter völlig ausreichend. Das Klassendiagramm übernahm alle Konzepte des ER-Modells und fügte ihm einige Punkte hinzu. Diese wären beispielsweise

- Erweiterung der Klasse um Methoden
- Abstrakte Klasse / Interface
- Mehrfachvererbung
- Detailiertere Darstellung der Beziehungen zwischen Objekten


In der Summe sollte man deshalb folgende Begriffe der OOP/Klassendiagrammes kennen:



- Klasse / Objekt / Instanziierung
- Konstruktor/Destruktor
- Statische Attribute und Methoden
- Attribute / Methoden / Properties
- Geheimnisprinzip (public/private/protected/internal/….)

- Vererbung
   - Basisklasse / Abgeleitete KLasse
   - Einfach-, Mehrfachvererbung
   - Abstrakte Klasse / Interface
   - Überschreiben von Methoden/Polymorphie / Late vs. early Binding
   
-Beziehungen zwischen Klasse
   - Assoziation
   - Aggregation
   - Komposition
  





Klasse / Objekt / Instanziierung
--------------------------------

Die **Klasse** ist der grundlegende Begriff in der OOP.

Über die Klasse definiert man zusammengehörende Informationen in einem gemeinsamen Container, der Klasse.

Die Klasse erhält einen Namen, die zu speichernden Informationen bezeichnet man als Attribute der Klasse.

Die Manipulation, d.h. das Schreiben, Lesen und Ändern der Daten, obliegt ebenfalls dem Verantwortungsbereich der Klasse und wird . Neben diesen Aufgaben kann eine Klasse noch weitere Fähigkeiten besitzen. Diese Fähigkeiten werden in der Klasse durch Methoden definiert.

Eine Klasse besteht deshalb zumindest aus 3 Bereichen.

- Dem Klassennamen
- Der Liste der Attribute
- Der Liste der Methoden

.. image:: images/klasse/klasse.png

Neben den wirklichen Fähigkeiten, im obigen Beispiel sprechen, muss eine Klasse häufig über Verwaltungsmethoden verfügen, die eine Manipulation der internen Attribute ermöglichen. Man nennt diese Methode häufig getter/setter-Methoden. Da ein Methodenname lediglich einmal genutzt werden darf, muss man deshalb häufig 2 Methodennamen benutzen.

.. image:: images/klasse/klasse_attribut_methode_property.jpg


Die Klasse definiert als Bauplan lediglich die Art der gehaltenen Informationen bzw. die Methoden, die zur Manipulation der Daten bzw. zur Funktionalität der Klasse notwendig sind. Doch wie kann man nun diese Klasse nutzen ? Da wir ja Informationen zu einem realen Gegenstand des Systems erheben wollen, müssen wir den Bauplan der Klasse einem realen Objekt zuordnen. Nur ein reales Objekt kann Daten speichern. Dieser Vorgang wird Instanziierung genannt und läuft in folgenden Schritten ab:

- Deklaration einer Variablen vom Typ der Klasse
- Instanziierung des Objektes mit Hilfe des new-Operators.

Der new-Operator erzeugt ein sog. Objekt der Klasse. Dieses Objekt ist einzigartig und ist nun in der Lage, die Informationen aufzunehmen und zu verarbeiten. Bei allen Instanziierungen wird immer ein Konstruktor aufgerufen. Er lautet wie der Name der Klasse und kann überladen sein, d.h. in verschiedenen Versionen existieren. Durch ihn ist das Objekt in der Lage, Zustände seiner Variablen bei der Erzeugung zu kontrollieren. Definiert man eigene Konstruktoren, so muss der parameterlose Standardkonstruktor ebenfalls angegeben werden, wenn man ihn zur Verfügung stellen will.

Erst nach dem Erzeugen kann man nun die Fähigkeiten des Objektes benutzen, d.h. man kann die Methoden der Klasse benutzen. Merke: Methoden werden auf Klassenebene definiert, aber auf Objektebene genutzt ! (Es gibt aber eine Ausnahme ! Welche ?) 




Innerhalb der Klasse gibt es häufig eine spezielle Methode, den sog. Konstruktor. Sie hat den gleichen Namen wie die Klasse selbst und sie kann in verschiedenen Variationen vorliegen. Beim Erzeugen eines Objektes (s.u.) wird diese Methode als Erstes aufgerufen. Wird diese Methode nicht innerhalb der Klasse definiert, so wird ein sog. Standardkonstruktor benutzt. Er besteht lediglich aus dem Methodennamen ohne irgendwelche Parameter. Werden eigene Konstrukoren geschrieben, so muss der Standardkonstruktor explizit definiert werden, sonst ist er nicht mehr vorhanden.


In einem zusammenhängenden Stück Quellcode könnte eine Programmiersprache die oben dargestellten Konzepte wie folgt umsetzen.

.. code-block:: csharp

	class Person   
	{
	    int mAlter;
	    string mName;
	    string mVorname;
	 
	    //Standardkonstruktor
	    public Person()
	    {
	 
	    }
	 
	    //Übeladener Konstruktor
	    public Person(string _Name)
	    {
	       mName = _Name;
	    }
	 
	    public void sprechen (string Aussage)
	    {
	      Console.WriteLine(Aussage);
	    }
	 
	    public void setName(string _name)
	    {
	       mName = _name;
	    }
	 
	   public string getName(string _name)
	   {
		return Name;
	   }
	 
	 
	   //Destruktor
	   //nicht nötig; wird beim Zerstören eines Objektes aufgerufen
	   ~Person()
	   {
	     Console.WriteLine("Aargh");
	   }
	}
	
	
	class Program
	{
	
		public static void Main()
		{
			Person STE = new Person();
			Person SYC = new Person();
			Person SMA = new Person();
		}
	
	
	}
	


Powershell und (Klasse/Attribut/Methode/Instanziierung)
-------------------------------------------------------

.. code-block:: csharp



Die Skriptsprache Powershell hat im Bezug auf die Konzepte "Klasse, Objekt, Attribut, Methode"  andere Umsetzungen gewählt. Insbesondere das Konzept der Klasse zur Definition von Eigenschaften u. Methoden ist unbekannt und es gibt mehrere Möglichkeiten, wie man Objekte erzeugen kann. Diesem werden dann beim Erzeugen die entsprechenden Attribute und Methoden zugewiesen.

.. code-block:: csharp

	# Neues leeres Objekt erstellen
	$Obj= New-object -TypeName PSObject

	# Wert an das Objekt anfügen
	Add-Member -InputObject $Obj -Name Alter -Value 50 -MemberType NoteProperty
	# Wert an das Objekt anfügen
	Add-Member -InputObject $Obj -Name Name -Value "STE" -MemberType NoteProperty
	# Wert an das Objekt anfügen
	Add-Member -InputObject $Obj -Name Vorname-Value "Kurt" -MemberType NoteProperty

Powershell vermischt auf diese Weise die Definition der Klasse mit dem Erzeugen eines Objektes. Dies kann dadurch vermieden werden, dass man Module als Definition von Klassenstrukturen benutzt.


Geheimnisprinzip
-----------------

So wie Objekte im realen Leben auch nicht jedes Geheimnis nach draußen preisgeben, so gilt dies auch in der OOP. Das Objekt sollte prinzipiell den Zustand seiner Attribute versteckt halten, d.h. keinen direkten Zugriff auf seine Attribute erlauben.

Durch das zur Verfügung stellen von entsprechenden getter/setter-Methoden bzw. Properties kann das Objekt den Zugriff auf seine Attribute kontrollieren.

Der generelle Zugriffsmöglichkeit auf Attribute und Methoden wird in der OOP über die **Sichtbarkeiten** definiert. Diese stehen vor dem Attribut bzw. der Methode und definieren den möglichen Zugriff von außerhalb auf die Attribute und Methoden. Gängige Definitionen von Sichtbarkeiten sind:


- public (+): Öffentlich sichtbar, von überall aufruf- und damit manipulierbar
- private (-): Nur innerhalb des Objektes selbst benutzbar
- protected/internal (#): Nur innerhalb des gleichen Namespaces bzw. über Vererbungsmechanismen sichtbar.


Des Weiteren kann in der PowerShell auch ein Gültigkeitsbereich für die Variablen festgelegt
werden.
Es gibt folgende Gültigkeitsbereiche:
-
Global: Überall sichtbar
-
Script: In allen
Bereichen der Skriptdatei sichtbar
-
Local: Nur im aktuellen Bereich und Unterbereichen sichtbar
-
Private: Nur im aktuellen Bereich sichtbar
Wird kein Gültigkeitsbereich angegeben für eine Variable gilt per Default „Local“ als
Gültigkeitsbereich. Möchte man z
.B. eine Variable haben die überall sichtbar ist geht das wie folgt:
$
global:z
= "
A
"



