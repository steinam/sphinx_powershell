.. include:: Abbrev.txt


.. index:: Objekt

PS und Objektorientierung
**************************




Objektorientierung
==================

Objektorientierte Programmierung ist ein Programmierstil, der seit vielen Jahrzehnten als grundlegendes 
Programmierparadigma vorherrscht. **Alle** Programmiersprachen sind heutzutage objektorientiert; auch powerShell. 
Leider hat man es bis zur Version 5 der Powershell dem Systemintegrator schwer gemacht, die gängigen objektorientierten 
Konzepte in der Powershell einzusetzen, weil einfach die sprachlichen Konstrukte in PowerShell nicht vorhanden waren.



Dies hat sich aber nun grundleg.

.. uml::

   Alice -> Bob: Hi!
   Alice <- Bob: How are you?
   
jhd ds jfds jf
