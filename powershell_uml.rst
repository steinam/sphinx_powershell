.. include:: Abbrev.txt

UML
**********


.. sidebar:: Zusammenfassung

    :Release: |release|
    :Datum: |today|
    :Autor: **Steinam**
    :Target: Schüler FI SYS
    :status: In Bearbeitung
    :vollständig: 20 %


Neben dem Diagrammen der strukturierten Programmierung (Struktogramm) finden heute innerhalb des Programmierens eine Vielzahl weiterer Diagramme Verwendung . Dazu gehören auch die Diagramme der **UML** (Unified Modelling Language). 

Sie betrachten das zu lösende Software-Problem aus verschiedenen Blickwinkeln (Struktur, Ablauf, Kundensicht) und werden häufig in der Analyse-Phase eingesetzt.



Anwendungsfalldiagramm (UseCase-Diagramm)
=========================================

.. index:: Anwendungsfalldiagramm, UseCase

Das Anwendungsfalldiagramm wird in der ersten Phase der Analyse eingesetzt, insbesondere beim Kunden. Es dient der Definition der Anforderungen/Funktionalitäten, die der Nutzer eines Systems an das System stellt. Durch seine einfache Notation ist es das ideale Diagramm zur Kommunikation zwischen den (programmtechnisch unbedarften) Kunden (Nutzer) und dem Entwickler des Systems.

Es besteht aus folgenden Komponenten:

- Akteur: Nutzer des Systems
- UseCase: Szenario/Workflow/Funktionalität
- Beziehungen: Zwischen den UseCases können Abhängigkeiten bestehen, z.B. eine Funktion nutzt andere Funktionen bzw. eine Funktion wird von mehreren anderen Funktionen genutzt.


Beispiel:

Der Teamleiter wünscht sich ein Programm, mit dem er die Rechner seiner IT-Landschaft inventarisieren kann. Folgende Informationen sollen beschafft werden:

- Modell, Hersteller, Seriennummer des Herstellers
- BIOS-Version und Hersteller
- Typ und Geschwindigkeit der CPU
- Typ und Version des Betriebssystems
- Grööe des Arbeitsspeichers
- Grööe und Typ der Festplatten
- IP und MAC-Adresse der Netzwerkarten


Die Ausgabe doll zunächst nur auf der Konsole erfolgen; für den realen Einsatz soll die Ausgabe in eine Textdatei geschrieben werden.

Die auszulesenden Computer sollen zunächst auf der Kommandozeile übergeben werden; die Eingabe über eine Textdatei bzw. per Active Directory.

Erstellen Sie zu diesem Sachverhalt ein Anwendungsfalldiagramm !


.. image:: images/uml.png
