Powershell und Datenbanken
==========================

.. image:: images/net_data_architecture.png


MySQL
-----

Für MySQL muss man den .NET-Datenbanktreiber installieren, den man auf der MySQL-Website herunterladen kann.

.. code-block:: sql
	
	## MYSQL Connection
	## This requires mysql connector net
	
	## All variables will need changing to suit your environment
	$server= "localhost 3306"
	$username= "root"
	$password= "yourpassword"
	$database= "mydatabase"
	
	## The path will need to match the mysql connector you downloaded
	[void][system.reflection.Assembly]::LoadFrom("C:\Program Files\MySQL\MySQL Connector Net 6.0.2\Assemblies\MySQL.Data.dll")
	
	function global:Set-SqlConnection ( $server = $(Read-Host "SQL Server Name"), $username = $(Read-Host "Username"), $password = $(Read-Host "Password"), $database = $(Read-Host "Default Database") ) {
		$SqlConnection.ConnectionString = "server=$server;user id=$username;password=$password;database=$database;pooling=false;Allow Zero Datetime=True;"
	}
	
	function global:Get-SqlDataTable( $Query = $(if (-not ($Query -gt $null)) {Read-Host "Query to run"}) ) {
		if (-not ($SqlConnection.State -like "Open")) { $SqlConnection.Open() }
		$SqlCmd = New-Object MySql.Data.MySqlClient.MySqlCommand $Query, $SqlConnection
		$SqlAdapter = New-Object MySql.Data.MySqlClient.MySqlDataAdapter
		$SqlAdapter.SelectCommand = $SqlCmd
		$DataSet = New-Object System.Data.DataSet
		$SqlAdapter.Fill($DataSet) | Out-Null
		$SqlConnection.Close()
		return $DataSet.Tables[0]
	}
	
	Set-Variable SqlConnection (New-Object MySql.Data.MySqlClient.MySqlConnection) -Scope Global -Option AllScope -Description "Personal variable for Sql Query functions"
	Set-SqlConnection $server $username $password $database
	
	$mysqltest = Get-SqlDataTable 'SHOW STATUS'
	
	echo $mysqltest
	

	
	$global:Query = 'SELECT `field1`, `field2`, `field3` FROM table'
	$mysqlresults = Get-SqlDataTable $Query
	
	ForEach ($result in $mysqlresults){
		echo $($result.field1)
		echo $($result.field2)
		echo $($result.field3)
	}


	
Ein anderes Beispiel:

.. code-block:: c#

	[void][system.reflection.Assembly]::LoadWithPartialName("MySql.Data")
 
	# DB-Verbindung aufbauen
	$strConnectionString = "server=strServer; port=3306; uid=strBenutzer; pwd=strKennwort; database=strDatenbank; Pooling=False"
	$objConnection = New-Object MySql.Data.MySqlClient.MySqlConnection($strConnectionString)
	$objConnection.Open()
 
	# Objekte MySqlCommand und MySqlDataAdapter erzeugen
	$strStatement = "SELECT * FROM kunden"
	$objCommand = New-Object MySql.Data.MySqlClient.MySqlCommand($strStatement, $objConnection)
	$objDataAdapter = New-Object MySql.Data.MySqlClient.MySqlDataAdapter($objCommand)
 
	# Dataset erzeugen
	$objDataSet = New-Object System.Data.DataSet
	$objDataAdapter.Fill($objDataSet) > $null
 
	# Ergebnis ausgeben
	$Ergebnis = $objDataSet.Tables[0]
	$Ergebnis | Format-Table *
	
	==============================================================
	#obiges Beispiel refactorisiert
	[void][system.reflection.Assembly]::LoadWithPartialName("MySql.Data")

	function db_connect([string]$server, [string]$user, [string]$password, [string]$db)
	{
		# DB-Verbindung aufbauen
		$strConnectionString = "server=$server; port=3306; uid=$user; pwd=$password; database=$db; Pooling=False;"
		Write-host $strConnectionString
		
		$objConnection = New-Object MySql.Data.MySqlClient.MySqlConnection($strConnectionString)
		
		
		$objConnection.Open()
		return $objConnection
	}
	
	
	function db_select([string]$strstatement, $objconnection)
	{
		# Objekte MySqlCommand und MySqlDataAdapter erzeugen
		$objCommand = New-Object MySql.Data.MySqlClient.MySqlCommand($strStatement, $objConnection)
		$objDataAdapter = New-Object MySql.Data.MySqlClient.MySqlDataAdapter($objCommand)
	
		# Dataset erzeugen
		$objDataSet = New-Object System.Data.DataSet
		$objDataAdapter.Fill($objDataSet) > $null
		return $objDataSet.Tables[0]
	}
	
	
	$connection = db_connect "192.168.0.12" "root" "passwort" "mysql"
	$Ergebnis = db_select "select @@version" $connection
	
	
	# Ergebnis ausgeben
	
	
	
	
	$Ergebnis | Format-Table *
	
	==============================================================
	
	Backup-Skript aller Datenbanken
	
	#http://www.codeproject.com/Tips/234492/MySQL-DB-backup-using-powershell-script


	#Step 1
	# Core settings - you will need to set these 
	$mysql_server = "192.168.0.12"
	$mysql_user = "root" 
	$mysql_password = "passwort" 
	$backupstorefolder= "c:\BackupPath\" 
	$dbName = "wikidb"
	
	
	#Step 2
	$pathtomysqldump = "C:\Program Files\MySQL\MySQL Server 5.5\bin\mysqldump.exe"
	
	#Step 3
	cls
	# Determine TodayÂ´s Date Day (monday, tuesday etc)
	$timestamp = Get-Date -format yyyyMMddHHmmss
	Write-Host $timestamp 
	
	#Step 4
	
	
	#Step 5
	# Connect to MySQL database 'information_schema'
	[system.reflection.assembly]::LoadWithPartialName("MySql.Data")
	$cn = New-Object -TypeName MySql.Data.MySqlClient.MySqlConnection
	$cn.ConnectionString = "SERVER=$mysql_server;DATABASE=information_schema;UID=$mysql_user;PWD=$mysql_password"
	$cn.Open()
	
	
	#Step 6
	# Query to get database names in asceding order
	$cm = New-Object -TypeName MySql.Data.MySqlClient.MySqlCommand
	$sql = "SELECT DISTINCT CONVERT(SCHEMA_NAME USING UTF8) AS dbName, CONVERT(NOW() USING UTF8) AS dtStamp FROM SCHEMATA ORDER BY dbName ASC"
	$cm.Connection = $cn
	$cm.CommandText = $sql
	$dr = $cm.ExecuteReader()
	 
	# Loop through MySQL Records
	while ($dr.Read())
	{
	 # Start By Writing MSG to screen
	 $dbname = [string]$dr.GetString(0)
	 if($dbname -match $dbName)
	 {
	 write-host "Backing up database: " $dr.GetString(0)
	 
	 # Set backup filename and check if exists, if so delete existing
	 $backupfilename = $timestamp + "_" + $dr.GetString(0) + ".sql"
	 $backuppathandfile = $backupstorefolder + "" + $backupfilename
	 If (test-path($backuppathandfile))
	 {
	 write-host "Backup file '" $backuppathandfile "' already exists. Existing file will be deleted"
	 Remove-Item $backuppathandfile
	 }
	 
	 # Invoke backup Command. /c forces the system to wait to do the backup
	 cmd /c " `"$pathtomysqldump`" -h $mysql_server -u $mysql_user -p$mysql_password $dbname > $backuppathandfile "
	 If (test-path($backuppathandfile))
	 {
	 write-host "Backup created. Presence of backup file verified"
	 }
	 }
	 
 
	
	
    ==============================================================
	#insert-statement in mysql

	function ConnectMySQL([string]$user,[string]$pass,[string]$MySQLHost,[string]$database) {
 
	# Load MySQL .NET Connector Objects
	[void][system.reflection.Assembly]::LoadWithPartialName("MySql.Data")
 
	# Open Connection
	$connStr = "server=" + $MySQLHost + ";port=3306;uid=" + $user + ";pwd=" + $pass + ";database="+$database+";Pooling=FALSE"
	$conn = New-Object MySql.Data.MySqlClient.MySqlConnection($connStr)
	$conn.Open()
	$cmd = New-Object MySql.Data.MySqlClient.MySqlCommand("USE $database", $conn)
	return $conn
 
	}
 
	function WriteMySQLQuery($conn, [string]$query) {
 
		$command = $conn.CreateCommand()
		$command.CommandText = $query
		$RowsInserted = $command.ExecuteNonQuery()
		$command.Dispose()
		if ($RowsInserted) {
				return $RowInserted
			} else {
				return $false
			}
    	}
 
    	# setup vars
    	$user = 'myuser'
    	$pass = 'mypass'
    	$database = 'mydatabase'
    	$MySQLHost = 'database.server.com'
 
    	# Connect to MySQL Database
    	$conn = ConnectMySQL $user $pass $MySQLHost $database
 
    	# Read all the records from table
    	$query = 'INSERT INTO test (id,name,age) VALUES ("1","Joe","33")'
    	$Rows = WriteMySQLQuery $conn $query
    	Write-Host $Rows " inserted into database"


MSSQL
-----


.. code-block:: csharp

	$SQLServer = "MySQLServer" #use Server\Instance for named SQL instances! 
	$SQLDBName = "MyDBName"
	$SqlQuery = "select * from authors WHERE Name = 'John Simon'"
	
	$SqlConnection = New-Object System.Data.SqlClient.SqlConnection
	$SqlConnection.ConnectionString = "Server = $SQLServer; Database = $SQLDBName; Integrated Security = True"
	
	$SqlCmd = New-Object System.Data.SqlClient.SqlCommand
	$SqlCmd.CommandText = $SqlQuery
	$SqlCmd.Connection = $SqlConnection
	
	$SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
	$SqlAdapter.SelectCommand = $SqlCmd
	
	$DataSet = New-Object System.Data.DataSet
	$SqlAdapter.Fill($DataSet)
	 
	$SqlConnection.Close()
	
	clear
	
	$DataSet.Tables[0]
	


Fragen
-------

Ausgangspunkt für die Arbeit ist die Datei **servers.txt**, in denen die IP-Adressen der vorhandenen MySQL-Server ihres Systems gespeichert sind.
Weiterhin existiert die Datei **users.txt** mit allen erlaubten Users eines Systems.


#. Zeigen Sie die Versionen der verwendeten MySQL-DB-Server


	::
		SELECT version();
		SELECT @@version;

		SHOW GLOBAL VARIABLES LIKE '%version%';
		mysql> STATUS;
		
		
#. Erzeugen Sie die Datei **users.txt**, indem Sie die Informationen ihres DB-Servers abfragen. Alle User ihres Systems sollen darin gelistet werden.

	:: 
	
		SELECT * from mysql.user;

		
#. Welche user haben globale Rechte		
		

#. Erzeugen Sie eine Datei user_grants.txt, in der die Rechte eines Users angezeigt werden. Vergleichen Sie diese Datei mit einer in der Vorwoche erzeugten Datei, um UNterschiede zu sehen ("Hinweis auf gehackten Rechner")

#. Löschen Sie alle User, die lediglich Connection-Rechte haben (usage)
