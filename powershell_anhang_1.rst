.. include:: Abbrev.txt

Anhang
======


.. _urls:

Links
-----

 - http://www.it-visions.de/scripting/powershell/PowerShellCodeBeispiele.aspx




.. _schleifen:

Schleifen
---------

kjckjk


.. _schleife_for:

for
^^^

.. code-block:: sh
  	
    for ($a = 1; $a -le 3 ; $a++) {"hello"}
	
    [int]$intPing = 10
    [string]$intNetwork = "127.0.0."

    for ($i=1;$i -le $intPing; $i++)
    {
    	$strQuery = "select * from win32_pingstatus where address = '" + $intNetwork + $i + "'"
    	$wmi = get-wmiobject -query $strQuery
    	"Pinging $intNetwork$i ... "
    	if ($wmi.statuscode -eq 0)
    	{
    	  "success"
    	}
    	else
        {
          "error: " + $wmi.statuscode + " occurred"
        }
    }
		
.. _schleife_do_while:
    
do - while
^^^^^^^^^^

.. code-block:: sh

   $dtmTime = get-date -h 04 -mi 23 -s 00

   do 
   {
	$dtmCurrent = Get-Date -DisplayHint time
	"The current time is " + $dtmCurrent "counting to " + $dtmtime
	start-sleep -s 2
   } 
   while ($dtmCurrent -lt $dtmTime)
   "time reached Ã "

  
.. _schleife_do_until:
   
do - until
^^^^^^^^^^


.. code-block:: sh

   //Endlosschleife

   $strTxtFile = "c:\mytest\loopbackprocesses.txt"
   $i = 0
   $mytext = Get-Content $strTxtFile
   do {
   	$mytext[$i]
   	$i ++
   } until ($i -eq $mytext.length)



   
   
   
.. _Auswahl:

Auswahl
-------

.. _auswahl_if_elseif_else:

if-elseif
^^^^^^^^^

.. code-block:: sh

   $wmi = get-wmiObject win32_processor
   if ($wmi.Architecture -eq 0)
   	{"This is an x86 computer"}
   elseif($wmi.architecture -eq 1)
      {"This is an MIPS computer"}
   elseif($wmi.architecture -eq 2)
      {"This is an Alapha computer"}
   elseif($wmi.architecture -eq 3)
      {"This is an PowerPC computer"}
   elseif($wmi.architecture -eq 6)
      {"This is an IPF computer"}
   elseif($wmi.architecture -eq 9)
      {"This is an x64 computer"}
   else
      {$wmi.architecture + " is not a cpu type I am familiar with"}
      
   "Current clockspeed is : " + $wmi.CurrentClockSpeed + " MHZ"
   "Max clockspeed is : " + $wmi.MaxClockSpeed  + " MHZ"
   "Current load percentage is: " + $wmi.LoadPercentage + " Percent"
   "The L2 cache size is: " + $wmi.L2CacheSize + " KB"


   
.. _auswahl_switch:
   
switch
^^^^^^

.. code-block:: sh

   $a=5;switch ($a) { 4{"four detected"} 5{"five detected"} }

   $wmi = get-wmiobject win32_computersystem
   "computer " + $wmi.name + " is: "
   
   switch ($wmi.domainrole)
   {
   	0 {"`t Stand alone workstation"}
   	1 {"`t Member workstation"}
   	2 {"`t Stand alone server"}
   	3 {"`t Member server"}
   	4 {"`t Back up domain controller"}
   	5 {"`t Primary domain controller"}
   	default {"`t The role can not be determined"}
   }



   
Darstellungsmöglichkeiten
-------------------------

Programmablaufplan
------------------

kdjfksdjf


Struktogramm 
------------


.. rubric:: Footnotes

.. [#] FÃ¼r case- bzw. case-insensitive Versionen gibt es noch::
	-ccontains, -icontains  -cnotcontains, -inotcontains
.. [#] Restwert einer Division

