.. Powershell für Systemadministratoren documentation master file, created by
   sphinx-quickstart on Sun Aug  8 15:30:28 2010.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Powershell für Systemadministratoren !
======================================

.. sidebar:: Summary

    :Release: |release|
    :Date: |today|
    :Authors: **STE**
    :Target: developers and administrators
    :status: development

    .. todolist::

.. toctree::
   :maxdepth: 1
   :numbered:

   powershell_variable_datentyp.rst
   powershell_operator.rst
   powershell_bedingung.rst
   powershell_schleifen.rst
   powershell_arrays.rst
   powershell_funktionen.rst
   Aufgaben.rst
   Loesungen.rst

..   powershell_customobjects.rst
   powershell_fehlerbehandlung.rst
   powershelloop.rst
   powershell_klassendiagramm.rst

   powershell_console.rst
   powershell_interactive
   powershell_struktogramm.rst
   powershell_pipeline.rst
   


..   powershell_editor.rst
..   glossar.rst
.. powershell_uml.rst
.. powershell_anhang_1.rst
.. powershell_activedirectory.rst
.. powershell_datenbanken.rst
.. powershell_custom.rst



http://www.computerperformance.co.uk/powershell/index_real_life.htm





Indizes und Tabellen
====================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
