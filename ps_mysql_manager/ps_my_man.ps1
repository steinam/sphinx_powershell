$DebugPreference = "Continue"

#Bibliothek einbinden
[void][system.reflection.Assembly]::LoadFrom("c:\Program Files (x86)\MySQL\MySQL Connector Net 6.9.9\Assemblies\v4.0\MySql.Data.dll");



#Erste Einrichtung der Datenbankverbindung
#$connstring = "Server=10.161.8.17;Uid='root';Pwd='steinam';Database=dr_watson";
$connstring = "Server=localhost;Uid='root';Pwd='patricia1234';Database=drwatson";


$con = New-Object Mysql.Data.MysqlClient.MysqlConnection;
$con.ConnectionString = $connstring;

$reader = New-Object Mysql.Data.MySqlClient.MySqlDataReader;
$DataSet = New-Object System.Data.DataSet
$SqlAdapter = New-Object MySql.Data.MySqlClient.MySqlDataAdapter



function verbindungOeffnen()
{
	$con.Open();
	Write-Debug "Datenbankverbindung geöffnet"
}

#Verbindung schließen
function verbindungschliessen()
{
	$con.Close();
	Write-Debug "Datenbankverbindung geschlossen"
}
function select_query()
{
    Param(
    [parameter(Mandatory=$true, Position=1)]
    [string]
    $query,
    
    [string]$Hostname="localhost",

    [parameter(Mandatory=$true,Position = 0)]
    [string] $Database ,
    
    [string] $Username="root",
    [parameter(Mandatory=$true)]
    [string] $password="" 
    )



    Write-Host $Database
    Write-Debug $query


    $Userlist = @();

    verbindungOeffnen

    #SQL-Statement eingeben
	$SqlQuery = "select Anmeldename from tbl_user;"

	#Commandobjekt anlegen und Connectionobjekt sowie Abfrage zuordnen
	$SqlCmd = New-Object MySql.Data.MySqlClient.MySqlCommand;
	$SqlCmd.CommandText = $SqlQuery
	$SqlCmd.Connection = $con

	#Datenadaptar instantiieren und Commandreferenz zuweisen
	$reader = $SqlCmd.ExecuteReader();
	
    
    
	while($reader.Read())
	{
	
		$Userlist += $reader["Anmeldename"]

	}
    verbindungschliessen
	return $Userlist


}

select_query -Database "watson" -query "select * from watson" -password " " 
[xml]$configFile= get-content .\config.xml
$configFile.configuration.hostname
$configFile.configuration.username
