.. include:: Abbrev.txt

.. index:: Fehlerbehandlung

Fehlerbehandlung
****************

Je größer Skripts werden, desto größer ist die Wahrscheinlichkeit, dass sich Fehler in den Code einschleichen.




.. index:: What_if

What-if
=======

Um zu sehen, welche Effekte ein Skript hat, kann man es mit dem sog. What-if-Paramter versehen.


.. code-block:: sh

	# What exactly would happen if Stop-Process
	# ended all processes beginning with "c"?
	Stop-Process -Name c* -WhatIf

	PS C:\> Stop-Process -Name c* -WhatIf
	WhatIf: Ausführen des Vorgangs "Stop-Process" für das Ziel "conhost (1012)".
	WhatIf: Ausführen des Vorgangs "Stop-Process" für das Ziel "conhost (2000)".
	WhatIf: Ausführen des Vorgangs "Stop-Process" für das Ziel "csrss (368)".
	WhatIf: Ausführen des Vorgangs "Stop-Process" für das Ziel "csrss (428)".


What-IF kann auch in eigene Funktionen integriert werden.

.. code-block:: sh

	function MapDrive([string]$driveletter, [string]$target, [switch]$whatif)
	{
		If ($whatif)
		{
			Write-Host "WhatIf: creation of a network drive " +
			"with the letter ${driveletter}: at destination $target"
		}
		Else
		{
			New-PSDrive $driveletter FileSystem $target
		}
	}

	MapDrive k \\127.0.0.1\C$ -whatif
	WhatIf: creation of a network drive  + with the letter k: at destination \\127.0.0.1\C$


.. index:: confirm

Confirm
=======

Mit Hilfe des -confirm - Parameters wird vor der Ausführung eines Befehls eine Sicherheitsabfrage formuliert.

.. code-block:: sh

	Stop-Service a* -Confirm

	Bestätigung
	Möchten Sie diese Aktion wirklich ausführen?
	Ausführen des Vorgangs "Stop-Service" für das Ziel "Anwendungserfahrung
	(AeLookupSvc)".
	[J] Ja  [A] Ja, alle  [N] Nein  [K] Nein, keine  [H] Anhalten  [?] Hilfe
	(Standard ist "J"):n

	Bestätigung
	Möchten Sie diese Aktion wirklich ausführen?
	Ausführen des Vorgangs "Stop-Service" für das Ziel "Gatewaydienst auf
	Anwendungsebene (ALG)".
	[J] Ja  [A] Ja, alle  [N] Nein  [K] Nein, keine  [H] Anhalten  [?] Hilfe
	(Standard ist "J"):


Da manche Befehle "kritischer" als andere Befehle sind, haben die Schöpfer der |PS| verschiedene Confirm-Level (Low, Medium, High, None) eingebaut und diese an bestimmte Befehle gebunden. So würde die Bestätigungsabfrage beim Löschen einer User-Mail per exchange-cmdlet auch dann eine Sicherheitsabfrage hervorrufen, wenn der -confirm - Parameter nicht übergeben wird. Dies kann über die Variable $ConfirmPreference gesteuert werden. Wenn diese auf "Low" gesetzt wird, wird immer einer Bestätigung verlangt, bei "None" findet keine bestätigung statt.

.. index:: ErrorAction; SilentlyContinue; Continue; Stop; Inquire


.. code-block:: sh

	 Calculator may be started and stopped without being called
	 # into question because Stop-Process is in the Medium category:
	 Calc
	 Stop-Process -Name calc
	 # If the default setting is changed from High to Low,
	 # PowerShell will automatically question every action:
	 $ConfirmPreference = "Low"
	 calc
	 Stop-Process -Name calc







ErrorAction
===========


Auf einen Fehler kann Powershell mit verschiedenen Verhaltensweisen reagieren.

- SilentlyContinue: Einfach weitermachen
- Continue: Fehlermeldung anzeigen aber weitermachen
- Stop: Ausführung des Skriptes anhalten
- Inquire: Nachfragen, wie es sich verhalten soll.

Das Verhaltensweisen können mit Hilfe des Parameter *-ErrorAction* bzw. der globalen Variable *$ErrorActionPreference* gesteuert werden.

.. code-block:: sh


	Del "nosuchthing"; Write-Host "Done!"

	Del "nosuchthing" -ErrorAction "SilentlyContinue"; Write-Host "Done!"

	$script:ErrorActionPreference = "Stop"  #globales Anhalten bei Fehlern


Erkennen und Behandeln von Fehlern
==================================

Häufig will man auf Fehler auch reagieren können. Eine Möglichkeit ist es, den Fehlerstatus eines Programms auszuwerten. Dieser kann über die globale Variable **$?** abgefragt werden. Wenn ein Fehler aufgetreten ist, hat diese Variable den Wert **True**.

.. code-block:: sh

	 Del "nosuchthing" -ErrorAction "SilentlyContinue"
	 If (!$?) { "Didn't work!"; break }; "Everything's okay!"

	 Del "nosuchthing" -ErrorAction "SilentlyContinue"
	 If (!$?) { "Error: $($error[0])"; break }; "Everything's okay!"
	 Error: Cannot find path "u:\nosuchthing" because it does not exist.

Wenn Sie wissen wollen, welcher Fehler tatsächlich aufgetreten ist, kann mit mit Hilfe der $Error-Variable den tatsächlichen Fehler herausfinden.
$Errror ist ein Array, der an der Indexstelle 0 den jeweils aktuellsten Fehler hat




Benutzen von Traps
===================

Als Alternative können sog. "**Traps**" verwendet werden. Wenn bekannt ist, dass ein bestimmter Befehl eventuell
nicht zur Laufzeit korrekt funktioniert, dann kann man dies mit Hilfe eines Traps definieren, was im Fehlerfall passieren soll.

.. code-block:: bash

  Trap { "A dreadful error has occurred!"} 1/$null
  A dreadful error has occurred!
  Attempted to divide by zero.
  At line:1 char:53
  + Trap { "A dreadful error has occurred!"} 1/$ <<<< null


-  Traps Require Unhandled Exceptions


Traps funktionieren nur mit Fehlern, die nicht von anderer Stelle "gehandelt" werden. Das untere Statement gibt deshalb
keine Fehlemeldung aus, der der Compiler den Teilungsversuch der beiden Konstanten selbst behandln kann. Ebenfalls werden
Commandlets häufig eine eigene Fehlerbehandlung haben. Diese kann man durch Setzen der $ErrorAction - Variable beeinflussen.



.. code-block:: sh

    Trap { "A dreadful error has occurred!" } Del "nosuchthing"


    del : Der Pfad "C:\Users\steinam\nocsuchthing" kann nicht gefunden werden, da er nicht vorhan..
    In Zeile:1 Zeichen:42
    + Trap { "A dreadful error has occurred!"} del "nocsuchthing"
    +                                          ~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : ObjectNotFound: (C:\Users\steinam\nocsuchthing:String) [Remove-...
    + FullyQualifiedErrorId : PathNotFound,Microsoft.PowerShell.Commands.RemoveItemCommand




    Trap { "A dreadful error has occurred!" } `
    Del "nosuchthing" -ErrorAction "Stop"


    A dreadful error has occurred!
    Del : Der Pfad "C:\Users\steinam\nosuchthing" kann nicht gefun...



-  Using Break and Continue to Determine What Happens after an Error
-  Finding Out Error Details
-  Error Records: Error Details
-  Error Record by Redirection
-  Table 11.3: Properties of an error record
-  Error Record(s) Through the -ErrorVariable Parameter
-  Error Records Through $Error
-  Error Record Through Traps


Try - catch -finally
=====================

http://blogs.technet.com/b/heyscriptingguy/archive/2010/03/11/hey-scripting-guy-march-11-2010.aspx




Exceptions verstehen
====================

Fehler werden in modernen Programmiersprachen häufig mit dem neutralen Wort **Ausnahmen** bezeichnet. Beim Auftreten eines Fehlers wird eine Ausnahme ausgelößt, die von einer Stelle im Quellcode behandelt werden muss; ansonsten kommt es letztlich zu einer roten Fehlermeldung auf der Konsole.

Ausnahmen sind letztlich spezielle Fehlerklassen des .NET-Frameworks; für bestimmte Fehler gibt es jeweils spezielle Fehlerklassen.

In Abhängigkeit von bestimmten Fehler kann ein Skript nun unterschiedlich reagieren. Dies wird mit Hilfe des **try -- catch -- finally** - Konstruktes möglich.

.. code-block;; sh

   $client = New-Object System.Net.WebClient try { $client.DownloadFile(‘http://www/files/file.txt’, file.txt) catch [System.Net.WebException] {
      # $_ is set to the ErrorRecord of the
      exception Out-Log $_.Exception.Message
   }

   finally {
      # wird immer ausgeführt

   }




	- Handling Particular Exceptions
	- Throwing Your Own Exceptions
	- Catching Errors in Functions and Scripts
	- Stepping Through Code: Breakpoints
	- Table 11.4: Settings for $DebugPreference
	- Table 11.5: Fine adjustments of the PowerShell console
	- Tracing: Displaying Executed Statements
	- Stepping: Executing Code Step-by-Step


Catching Errors in Functions and Scripts
========================================

-  Stepping Through Code: Breakpoints
-  Table 11.4: Settings for $DebugPreference
-  Table 11.5: Fine adjustments of the PowerShell console
-  Tracing: Displaying Executed Statements
-  Stepping: Executing Code Step-by-Step
-  Summary




Links
======

https://blogs.msdn.microsoft.com/kebab/2013/06/09/an-introduction-to-error-handling-in-powershell/
