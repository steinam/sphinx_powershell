PowerShell CustomObjects
*************************
 
.. code-block:: sh

	#Möglichkeiten der Objekterzeugung in PS v2.0


	#per select-object
	#$item = "" | Select Name, RAM, CPU
	#$item.Name = "blah"
	#$item.Ram = 4096
	#$item.Cpu = 1
	#$item
	
	#per new-object
	#$item2 = new-object -type PSObject -Property @{
	#    Name = "blah"
	#    RAM = 4096
	#    CPU = 1
	#}
	#$item2
	
	
	
	$item3 = New-Object psobject
	$item3 | Add-Member -MemberType NoteProperty -Name Alter -Value 10
	$item3.Alter
	$item3 | Add-Member -MemberType ScriptMethod -Name hello -value {Write-Host "Hello World"}
	$item3.hello()


Einsatzmöglichkeiten
---------------------
