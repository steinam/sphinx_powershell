<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<attribute_registry SHOW_ATTRIBUTES="hide"/>
<node CREATED="1281262104046" ID="ID_915977709" MODIFIED="1281263999131" TEXT="FI SYS Programmierung">
<hook NAME="MapStyle">
<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode COLOR="#000000" LOCALIZED_TEXT="default" MAX_WIDTH="600" STYLE="as_parent">
<font/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge/>
<cloud/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode COLOR="#18898b" LOCALIZED_TEXT="styles.topic" STYLE="fork">
<font/>
</stylenode>
<stylenode COLOR="#cc3300" LOCALIZED_TEXT="styles.subtopic" STYLE="fork">
<font/>
</stylenode>
<stylenode COLOR="#669900" LOCALIZED_TEXT="styles.subsubtopic">
<font/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode COLOR="#000000" LOCALIZED_TEXT="AutomaticLayout.level.root">
<font/>
</stylenode>
<stylenode COLOR="#0033ff" LOCALIZED_TEXT="AutomaticLayout.level,1">
<font/>
</stylenode>
<stylenode COLOR="#00b439" LOCALIZED_TEXT="AutomaticLayout.level,2">
<font/>
</stylenode>
<stylenode COLOR="#990000" LOCALIZED_TEXT="AutomaticLayout.level,3">
<font/>
</stylenode>
<stylenode COLOR="#111111" LOCALIZED_TEXT="AutomaticLayout.level,4">
<font/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node CREATED="1281263940566" ID="ID_1716701429" MODIFIED="1281264281256" POSITION="right" TEXT="10. Klasse">
<cloud COLOR="#72ed8f"/>
<attribute_layout NAME_WIDTH="56" VALUE_WIDTH="56"/>
<attribute NAME="Stunden" VALUE="7"/>
<node CREATED="1281262127765" ID="ID_868288908" MODIFIED="1281262132438" TEXT="Woche 1">
<node CREATED="1281262252780" ID="ID_1335357904" MODIFIED="1284070181577" TEXT="Organisation">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Vorstellung der Person
    </p>
    <p>
      Vorstellung der Unterrichtsinhalte
    </p>
    <p>
      Abfragen &#xfc;ber Vorkenntnisse
    </p>
    <p>
      
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1281262339030" ID="ID_1250886020" MODIFIED="1281262473521" TEXT="PowerShell Console">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">Topics Covered: </font>
    </p>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      <font size="4">Starting PowerShell </font>
    </p>
    <p>
      <font size="4">&#x2022; Figure 1.1: How to always open PowerShell with administrator rights </font>
    </p>
    <p>
      <font size="4">First Steps with the Console </font>
    </p>
    <p>
      <font size="4">&#x2022; Figure 1.2: First commands in the PowerShell console </font>
    </p>
    <p>
      <font size="4">&#x2022; Incomplete and Multi-line Entries </font>
    </p>
    <p>
      <font size="4">&#x2022; Important Keyboard Shortcuts </font>
    </p>
    <p>
      <font size="4">&#x2022; Deleting Incorrect Entries </font>
    </p>
    <p>
      <font size="4">&#x2022; Overtype Mode </font>
    </p>
    <p>
      <font size="4">&#x2022; Command History: Reusing Entered Commands </font>
    </p>
    <p>
      <font size="4">&#x2022; Automatically Completing Input </font>
    </p>
    <p>
      <font size="4">&#x2022; Scrolling Console Contents </font>
    </p>
    <p>
      <font size="4">&#x2022; Selecting and Inserting Text </font>
    </p>
    <p>
      <font size="4">&#x2022; QuickEdit Mode </font>
    </p>
    <p>
      <font size="4">&#x2022; Figure 1.3: Marking and copying text areas in QuickEdit mode </font>
    </p>
    <p>
      <font size="4">&#x2022; Standard Mode </font>
    </p>
    <p>
      <font size="4">Customizing the Console </font>
    </p>
    <p>
      <font size="4">&#x2022; Opening Console Properties </font>
    </p>
    <p>
      <font size="4">&#x2022; Figure 1.4: Opening console properties </font>
    </p>
    <p>
      <font size="4">&#x2022; Defining Options </font>
    </p>
    <p>
      <font size="4">&#x2022; Figure 1.5: Defining the QuickEdit and Insert modes </font>
    </p>
    <p>
      <font size="4">&#x2022; Specifying Fonts and Font Sizes </font>
    </p>
    <p>
      <font size="4">&#x2022; Figure 1.6: Specifying new fonts and font sizes </font>
    </p>
    <p>
      <font size="4">&#x2022; Setting Window and Buffer Size </font>
    </p>
    <p>
      <font size="4">&#x2022; Figure 1.7: Specifying the size of the window buffer </font>
    </p>
    <p>
      <font size="4">&#x2022; Selecting Colors </font>
    </p>
    <p>
      <font size="4">&#x2022; Figure 1.8: Select better colors for your console </font>
    </p>
    <p>
      <font size="4">&#x2022; Directly Assigning Modifications in PowerShell </font>
    </p>
    <p>
      <font size="4">&#x2022; Saving Changes </font>
    </p>
    <p>
      <font size="4">Piping and Routing </font>
    </p>
    <p>
      <font size="4">&#x2022; Piping: Outputting Information Page by Page </font>
    </p>
    <p>
      <font size="4">&#x2022; Redirecting: Storing Information in Files </font>
    </p>
    <p>
      <font size="4">Summary </font>
    </p>
    <p>
      <font size="4">&#x2022; Table 1.1: Important keys and their meaning in the PowerShell console </font>
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1281262604019" ID="ID_194874806" MODIFIED="1281262610940" TEXT="PowerShell-Editor"/>
</node>
<node CREATED="1281263295595" ID="ID_935411145" MODIFIED="1281263302841" TEXT="Woche 2">
<node CREATED="1281263308650" ID="ID_509152879" MODIFIED="1281264132679" TEXT="Interactive Powershell">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="5">&#x2022; </font>
    </p>
    <p>
      <font size="5">&#x2022; </font>
    </p>
    <p>
      <font size="5">&#x2022; </font>
    </p>
    <p>
      <font size="5">&#x2022; </font>
    </p>
    <p>
      <font size="5">&#x2022; </font>
    </p>
    <p>
      <font size="5">&#x2022; </font>
    </p>
    <p>
      <font size="5">&#x2022; </font>
    </p>
    <p>
      <font size="5">PowerShell as a Calculator </font>
    </p>
    <p>
      <font size="5">&#x2022; Calculating with Number Systems and Units </font>
    </p>
    <p>
      <font size="5">&#x2022; Table 2.1: Arithmetic operators </font>
    </p>
    <p>
      <font size="5">Executing External Commands </font>
    </p>
    <p>
      <font size="5">&#x2022; Starting the "Old" Console </font>
    </p>
    <p>
      <font size="5">&#x2022; Discovering Useful Console Commands </font>
    </p>
    <p>
      <font size="5">&#x2022; Figure 2.1: Run PowerShell as administrator </font>
    </p>
    <p>
      <font size="5">&#x2022; Security Restrictions at Program Start </font>
    </p>
    <p>
      <font size="5">&#x2022; Trustworthy Subdirectories </font>
    </p>
    <p>
      <font size="5">Cmdlets: "Genuine" PowerShell Commands </font>
    </p>
    <p>
      <font size="5">&#x2022; Table 2.2: The most important standard actions and their descriptions </font>
    </p>
    <p>
      <font size="5">&#x2022; Using Parameters </font>
    </p>
    <p>
      <font size="5">&#x2022; Using Named Parameters </font>
    </p>
    <p>
      <font size="5">&#x2022; Switch Parameter </font>
    </p>
    <p>
      <font size="5">&#x2022; Positional Parameters </font>
    </p>
    <p>
      <font size="5">&#x2022; Common Parameters </font>
    </p>
    <p>
      <font size="5">&#x2022; Table 2.3: Common parameters in effect for (nearly) all cmdlets </font>
    </p>
    <p>
      <font size="5">Aliases: Giving Commands Other Names </font>
    </p>
    <p>
      <font size="5">&#x2022; Resolving Aliases </font>
    </p>
    <p>
      <font size="5">&#x2022; Creating Your Own Aliases </font>
    </p>
    <p>
      <font size="5">&#x2022; Removing&#x2014;or Permanently Retaining&#x2014;an Alias </font>
    </p>
    <p>
      <font size="5">&#x2022; Overwriting Alias Definitions and Deleting Them Manually </font>
    </p>
    <p>
      <font size="5">Functions: "Expanded" Aliases </font>
    </p>
    <p>
      <font size="5">&#x2022; Calling Commands with Arguments </font>
    </p>
    <p>
      <font size="5">&#x2022; Creating Shortcut Commands </font>
    </p>
    <p>
      <font size="5">Invoking Files and Scripts </font>
    </p>
    <p>
      <font size="5">&#x2022; Starting Scripts </font>
    </p>
    <p>
      <font size="5">&#x2022; Running Batch Files </font>
    </p>
    <p>
      <font size="5">&#x2022; Running VBScript Files </font>
    </p>
    <p>
      <font size="5">&#x2022; Running PowerShell Scripts </font>
    </p>
    <p>
      <font size="5">Summary </font>
    </p>
  </body>
</html></richcontent>
</node>
<node COLOR="#ff0000" CREATED="1281262579581" ID="ID_221463297" MODIFIED="1281263433163" TEXT="1. Stegreifaufgabe">
<font NAME="Liberation Sans" SIZE="12"/>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1281262137396" ID="ID_957164117" MODIFIED="1382281188006" TEXT="Woche 3">
<node CREATED="1281262484858" ID="ID_1675139135" MODIFIED="1281263359046" TEXT="Variable und Datentypen">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">Topics Covered: </font>
    </p>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      
    </p>
    <p>
      <font size="4">Your Own Variables </font>
    </p>
    <p>
      <font size="4">&#x2022; Selecting Variable Names </font>
    </p>
    <p>
      <font size="4">&#x2022; Assigning and Returning Values </font>
    </p>
    <p>
      <font size="4">&#x2022; Populating Several Variables with Values Simultaneously </font>
    </p>
    <p>
      <font size="4">&#x2022; Exchanging the Contents of Variables </font>
    </p>
    <p>
      <font size="4">&#x2022; Assigning Different Values to Several Variables </font>
    </p>
    <p>
      <font size="4">&#x2022; Overview of Variables in Use </font>
    </p>
    <p>
      <font size="4">&#x2022; Finding Variables </font>
    </p>
    <p>
      <font size="4">&#x2022; Verify Whether a Variable Exists </font>
    </p>
    <p>
      <font size="4">&#x2022; Deleting Variables </font>
    </p>
    <p>
      <font size="4">&#x2022; Using Special Variable Cmdlets </font>
    </p>
    <p>
      <font size="4">&#x2022; Table 3.1: Cmdlets for managing variables </font>
    </p>
    <p>
      <font size="4">&#x2022; Write-Protecting Variables: Creating Constants </font>
    </p>
    <p>
      <font size="4">&#x2022; Variables with Description </font>
    </p>
    <p>
      <font size="4">"Automatic" PowerShell Variables </font>
    </p>
    <p>
      <font size="4">Environment Variables </font>
    </p>
    <p>
      <font size="4">&#x2022; Reading Particular Environment Variables </font>
    </p>
    <p>
      <font size="4">&#x2022; Searching for Environment Variables </font>
    </p>
    <p>
      <font size="4">&#x2022; Creating New Environment Variables </font>
    </p>
    <p>
      <font size="4">&#x2022; Deleting and Modifying Environment Variables </font>
    </p>
    <p>
      <font size="4">&#x2022; Permanent Modifications of Environment Variables </font>
    </p>
    <p>
      <font size="4">Drive Variables </font>
    </p>
    <p>
      <font size="4">&#x2022; Directly Accessing File Paths </font>
    </p>
    <p>
      <font size="4">&#x2022; Table 3.2: Variable areas made available by external providers </font>
    </p>
    <p>
      <font size="4">&#x2022; Ad-hoc Variables: Sub-Expressions </font>
    </p>
    <p>
      <font size="4">Scope of Variables </font>
    </p>
    <p>
      <font size="4">&#x2022; Automatic Restriction </font>
    </p>
    <p>
      <font size="4">&#x2022; Changing Variable Visibility </font>
    </p>
    <p>
      <font size="4">&#x2022; Advantage of Lifting Visibility Restrictions: Clear and Unambiguous Start Conditions </font>
    </p>
    <p>
      <font size="4">&#x2022; Setting the Scope of Individual Variables </font>
    </p>
    <p>
      <font size="4">&#x2022; Table 3.3: Variable scopes and validity of variables </font>
    </p>
    <p>
      <font size="4">&#x2022; Table 3.4: Practical usage of scope allocations </font>
    </p>
    <p>
      <font size="4">Variable Types and "Strongly Typing" </font>
    </p>
    <p>
      <font size="4">&#x2022; Assigning Fixed Types </font>
    </p>
    <p>
      <font size="4">&#x2022; The Advantages of Specialized Types </font>
    </p>
    <p>
      <font size="4">&#x2022; Table 3.5: Variable types </font>
    </p>
    <p>
      <font size="4">Variable Management: Behind the Scenes </font>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1281262152659" ID="ID_723315741" MODIFIED="1382281186758" TEXT="Woche 4">
<node CREATED="1281262667175" ID="ID_450506587" MODIFIED="1281263077682" TEXT="Bedingungen">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      <font size="4">Formulating Conditions </font>
    </p>
    <p>
      <font size="4">&#x2022; Table 7.1: Comparison operators </font>
    </p>
    <p>
      <font size="4">&#x2022; Carrying Out a Comparison </font>
    </p>
    <p>
      <font size="4">&#x2022; "Reversing" Comparisons </font>
    </p>
    <p>
      <font size="4">&#x2022; Combining Comparisons </font>
    </p>
    <p>
      <font size="4">&#x2022; Table 7.2: Logical operators </font>
    </p>
    <p>
      <font size="4">&#x2022; Comparisons with Arrays and Collections </font>
    </p>
    <p>
      <font size="4">&#x2022; Verifying Whether an Array Contains a Particular Element </font>
    </p>
    <p>
      <font size="4">Where-Object </font>
    </p>
    <p>
      <font size="4">&#x2022; Filtering Results in the Pipeline </font>
    </p>
    <p>
      <font size="4">&#x2022; Formulating a Condition </font>
    </p>
    <p>
      <font size="4">&#x2022; Using Alias </font>
    </p>
    <p>
      <font size="4">If-ElseIf-Else </font>
    </p>
    <p>
      <font size="4">Switch </font>
    </p>
    <p>
      <font size="4">&#x2022; Testing Range of Values </font>
    </p>
    <p>
      <font size="4">&#x2022; No Applicable Condition </font>
    </p>
    <p>
      <font size="4">&#x2022; Several Applicable Conditions </font>
    </p>
    <p>
      <font size="4">&#x2022; Using String Comparisons </font>
    </p>
    <p>
      <font size="4">&#x2022; Case Sensitivity </font>
    </p>
    <p>
      <font size="4">&#x2022; Wildcard Characters </font>
    </p>
    <p>
      <font size="4">&#x2022; Regular Expressions </font>
    </p>
    <p>
      <font size="4">&#x2022; Processing Several Values Simultaneously </font>
    </p>
    <p>
      <font size="4">Summary </font>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1281262161058" ID="ID_375923076" MODIFIED="1382281185661" TEXT="Woche 5">
<node CREATED="1281262703660" ID="ID_1846315774" MODIFIED="1281263022372" TEXT="Schleifen">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      <font size="4">ForEach-Object </font>
    </p>
    <p>
      <font size="4">&#x2022; Evaluating Pipeline Objects Separately </font>
    </p>
    <p>
      <font size="4">&#x2022; Integrating Conditions </font>
    </p>
    <p>
      <font size="4">&#x2022; Invoking Methods </font>
    </p>
    <p>
      <font size="4">Foreach </font>
    </p>
    <p>
      <font size="4">Do and While </font>
    </p>
    <p>
      <font size="4">&#x2022; Continuation and Abort Conditions </font>
    </p>
    <p>
      <font size="4">&#x2022; Using Variables as Continuation Criteria </font>
    </p>
    <p>
      <font size="4">&#x2022; Endless Loops without Continuation Criteria </font>
    </p>
    <p>
      <font size="4">For </font>
    </p>
    <p>
      <font size="4">&#x2022; For Loops: Just Special Types of the While Loop </font>
    </p>
    <p>
      <font size="4">&#x2022; Unusual Uses for the For Loop </font>
    </p>
    <p>
      <font size="4">Switch </font>
    </p>
    <p>
      <font size="4">&#x2022; Processing File Contents Line by Line </font>
    </p>
    <p>
      <font size="4">Exiting Loops Early </font>
    </p>
    <p>
      <font size="4">&#x2022; Continue: Skipping Loop Cycles </font>
    </p>
    <p>
      <font size="4">&#x2022; Nested Loops and Labels </font>
    </p>
    <p>
      <font size="4">Summary </font>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1281262170418" ID="ID_986943739" MODIFIED="1382281184105" TEXT="Woche 6">
<node COLOR="#ff0000" CREATED="1281262832299" ID="ID_1372260544" MODIFIED="1281263429620" TEXT="1. Schulaufgabe">
<font NAME="Liberation Sans" SIZE="12"/>
<icon BUILTIN="yes"/>
</node>
<node CREATED="1281262798142" ID="ID_706160753" MODIFIED="1281263447129" TEXT="Funktionen">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      <font size="4">Creating New Functions </font>
    </p>
    <p>
      <font size="4">&#x2022; First Example: Shorthand Functions </font>
    </p>
    <p>
      <font size="4">&#x2022; Second Example: Combining Several Steps </font>
    </p>
    <p>
      <font size="4">&#x2022; Comfortably Entering Functions of Several Lines </font>
    </p>
    <p>
      <font size="4">&#x2022; Reducing a Function to a Single Line </font>
    </p>
    <p>
      <font size="4">&#x2022; Using Text Editors </font>
    </p>
    <p>
      <font size="4">&#x2022; Understanding NextFreeDrive </font>
    </p>
    <p>
      <font size="4">&#x2022; Processing and Modifying Functions </font>
    </p>
    <p>
      <font size="4">&#x2022; Removing Functions </font>
    </p>
    <p>
      <font size="4">Passing Arguments to Functions </font>
    </p>
    <p>
      <font size="4">&#x2022; $args: Arbitrary Arguments </font>
    </p>
    <p>
      <font size="4">&#x2022; Using the Argument Parser of $args </font>
    </p>
    <p>
      <font size="4">&#x2022; Setting Parameters </font>
    </p>
    <p>
      <font size="4">&#x2022; Arguments Having Predefined Default Values </font>
    </p>
    <p>
      <font size="4">&#x2022; Using Strongly Typed Arguments </font>
    </p>
    <p>
      <font size="4">&#x2022; Only Numbers Allowed </font>
    </p>
    <p>
      <font size="4">&#x2022; Date Required </font>
    </p>
    <p>
      <font size="4">&#x2022; "Switch" Parameter Is Like a Switch </font>
    </p>
    <p>
      <font size="4">Specifying Return Values of a Function </font>
    </p>
    <p>
      <font size="4">&#x2022; One or More Return Values? </font>
    </p>
    <p>
      <font size="4">&#x2022; The Return Statement </font>
    </p>
    <p>
      <font size="4">&#x2022; Accessing Return Values </font>
    </p>
    <p>
      <font size="4">&#x2022; Excluding Output from the Function Result </font>
    </p>
    <p>
      <font size="4">&#x2022; Excluding Text Output from the Result </font>
    </p>
    <p>
      <font size="4">&#x2022; Using Debugging Reports </font>
    </p>
    <p>
      <font size="4">&#x2022; Suppressing Error Messages </font>
    </p>
    <p>
      <font size="4">Inspecting Available Functions </font>
    </p>
    <p>
      <font size="4">&#x2022; Table 9.1: Predefined PowerShell functions </font>
    </p>
    <p>
      <font size="4">&#x2022; Prompt: A Better Prompt </font>
    </p>
    <p>
      <font size="4">&#x2022; Outputting Information Text at Any Location </font>
    </p>
    <p>
      <font size="4">Table of Contents | About PowerShell Plus </font>
    </p>
    <p>
      <font size="4">243 </font>
    </p>
    <p>
      <font size="4">Sponsors | Resources | &#xa9; BBS Technologies </font>
    </p>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      <font size="4">&#x2022; </font>
    </p>
    <p>
      <font size="4">&#x2022; Using the Windows Title Bar </font>
    </p>
    <p>
      <font size="4">&#x2022; Administrator Warning </font>
    </p>
    <p>
      <font size="4">&#x2022; Clear-Host: Deleting the Screen Buffer </font>
    </p>
    <p>
      <font size="4">&#x2022; Predefined Functions Once Again: A:, B:, C: </font>
    </p>
    <p>
      <font size="4">Functions, Filters and the Pipeline </font>
    </p>
    <p>
      <font size="4">&#x2022; The Slow Sequential Mode: $input </font>
    </p>
    <p>
      <font size="4">&#x2022; Filter: Rapid Streaming Mode </font>
    </p>
    <p>
      <font size="4">&#x2022; Devel </font>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1281262180881" ID="ID_501165685" MODIFIED="1382281190231" TEXT="Woche 7">
<node CREATED="1281262811269" ID="ID_1399627412" MODIFIED="1281262830164" TEXT="&#xdc;bung mit Bed., Schleifen, Funktionen"/>
</node>
<node CREATED="1281262127765" ID="ID_205711633" MODIFIED="1382281192534" TEXT="Woche 8">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Topics Covered:
    </p>
    <p>
      &#x2022;
    </p>
    <p>
      &#x2022;
    </p>
    <p>
      &#x2022;
    </p>
    <p>
      &#x2022;
    </p>
    <p>
      &#x2022;
    </p>
    <p>
      &#x2022;
    </p>
    <p>
      &#x2022;
    </p>
    <p>
      Using the PowerShell Pipeline
    </p>
    <p>
      
    </p>
    <p>
      &#x2022; Object-oriented Pipeline
    </p>
    <p>
      &#x2022; Text Not Converted Until the End
    </p>
    <p>
      &#x2022; Table 5.1: Typical pipeline cmdlets and functions
    </p>
    <p>
      &#x2022; Streaming: Real-time Processing or Not?
    </p>
    <p>
      &#x2022; "Blocking" Pipeline Commands
    </p>
    <p>
      Converting Objects into Text
    </p>
    <p>
      &#x2022; Making Object Properties Visible
    </p>
    <p>
      &#x2022; Formatting Pipeline Results
    </p>
    <p>
      &#x2022; Displaying Particular Properties
    </p>
    <p>
      &#x2022; Using Wildcard Characters
    </p>
    <p>
      &#x2022; Scriptblocks and "Synthetic" Properties
    </p>
    <p>
      &#x2022; Changing Column Headings
    </p>
    <p>
      &#x2022; Optimizing Column Width
    </p>
    <p>
      &#x2022; PropertySets and Views
    </p>
    <p>
      Sorting and Grouping Pipeline Results
    </p>
    <p>
      &#x2022; Sort Object and Hash Tables
    </p>
    <p>
      &#x2022; Grouping Information
    </p>
    <p>
      &#x2022; Using Grouping Expressions
    </p>
    <p>
      &#x2022; Using Formatting Cmdlets to Form Groups
    </p>
    <p>
      Filtering Pipeline Results
    </p>
    <p>
      &#x2022; Filtering Objects Out of the Pipeline
    </p>
    <p>
      &#x2022; Selecting Object Properties
    </p>
    <p>
      &#x2022; Limiting Number of Objects
    </p>
    <p>
      &#x2022; Processing All Pipeline Results Simultaneously
    </p>
    <p>
      &#x2022; Removing Doubles
    </p>
  </body>
</html></richcontent>
<node CREATED="1281263150581" ID="ID_1274544471" MODIFIED="1284059655476" TEXT="Pipeline"/>
<node COLOR="#ff0000" CREATED="1281263503780" ID="ID_46832049" MODIFIED="1281263512542" TEXT="2. Stegreifaufgabe">
<font NAME="Liberation Sans" SIZE="12"/>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1281262137396" ID="ID_1100365934" MODIFIED="1382281193660" TEXT="Woche 9">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      
    </p>
    <p>
      Analyzing and Comparing Results
    </p>
    <p>
      &#x2022; Statistical Calculations
    </p>
    <p>
      &#x2022; Comparing Objects
    </p>
    <p>
      &#x2022; Comparing Before-and-After Conditions
    </p>
    <p>
      &#x2022; Detecting Changes to Objects
    </p>
    <p>
      &#x2022; Comparing File Contents
    </p>
    <p>
      &#x2022; Saving Snapshots for Later Use
    </p>
    <p>
      Exporting Pipeline Results
    </p>
    <p>
      &#x2022; Suppressing Results
    </p>
    <p>
      &#x2022; Changing Pipeline Formatting
    </p>
    <p>
      &#x2022; Forcing Text Display
    </p>
    <p>
      &#x2022; Excel: Exporting Objects
    </p>
    <p>
      &#x2022; HTML Outputs
    </p>
    <p>
      The Extended Type System (Part One)
    </p>
    <p>
      &#x2022; Rendering Text as Text and Only Text
    </p>
    <p>
      &#x2022; Your Wish Has Priority
    </p>
  </body>
</html></richcontent>
<node CREATED="1281263157317" ID="ID_1641839201" MODIFIED="1284059661294" TEXT="Pipeline-&#xdc;bungen"/>
</node>
<node CREATED="1281262161058" ID="ID_1292174570" MODIFIED="1382281194757" TEXT="Woche 10">
<node CREATED="1281263609333" ID="ID_1347559959" MODIFIED="1281263646767" TEXT="Fehlerbehandlung">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      
    </p>
    <p>
      <font size="5">"What-if" Scenarios </font>
    </p>
    <p>
      <font size="5">&#x2022; Dry Runs: Simulating Operations </font>
    </p>
    <p>
      <font size="5">&#x2022; Stepped Confirmation: Separate Queries </font>
    </p>
    <p>
      <font size="5">&#x2022; Table 11.1: Selection options in stepped confirmation </font>
    </p>
    <p>
      <font size="5">&#x2022; Automatic Confirmation of Dangerous Actions </font>
    </p>
    <p>
      <font size="5">Defining Fault tolerance </font>
    </p>
    <p>
      <font size="5">&#x2022; Table 11.2: Setting options for ErrorAction and $ErrorActionPreference </font>
    </p>
    <p>
      <font size="5">Recognizing and Responding to Errors </font>
    </p>
    <p>
      <font size="5">&#x2022; Error Status in $? </font>
    </p>
    <p>
      <font size="5">&#x2022; Using Traps </font>
    </p>
    <p>
      <font size="5">&#x2022; Traps Require Unhandled Exceptions </font>
    </p>
    <p>
      <font size="5">&#x2022; Using Break and Continue to Determine What Happens after an Error </font>
    </p>
    <p>
      <font size="5">&#x2022; Finding Out Error Details </font>
    </p>
    <p>
      <font size="5">Error Records: Error Details </font>
    </p>
    <p>
      <font size="5">&#x2022; Error Record by Redirection </font>
    </p>
    <p>
      <font size="5">&#x2022; Table 11.3: Properties of an error record </font>
    </p>
    <p>
      <font size="5">&#x2022; Error Record(s) Through the -ErrorVariable Parameter </font>
    </p>
    <p>
      <font size="5">&#x2022; Error Records Through $Error </font>
    </p>
    <p>
      <font size="5">&#x2022; Error Record Through Traps </font>
    </p>
    <p>
      <font size="5">Understanding Exceptions </font>
    </p>
    <p>
      <font size="5">&#x2022; Handling Particular Exceptions </font>
    </p>
    <p>
      <font size="5">&#x2022; Throwing Your Own Exceptions </font>
    </p>
    <p>
      <font size="5">Catching Errors in Functions and Scripts </font>
    </p>
    <p>
      <font size="5">Stepping Through Code: Breakpoints </font>
    </p>
    <p>
      <font size="5">&#x2022; Table 11.4: Settings for $DebugPreference </font>
    </p>
    <p>
      <font size="5">&#x2022; Table 11.5: Fine adjustments of the PowerShell console </font>
    </p>
    <p>
      <font size="5">&#x2022; Tracing: Displaying Executed Statements </font>
    </p>
    <p>
      <font size="5">&#x2022; Stepping: Executing Code Step-by-Step</font>
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1281262170418" ID="ID_1582693354" MODIFIED="1382281196047" TEXT="Woche 11">
<node CREATED="1281263657395" ID="ID_1776234361" MODIFIED="1281263662399" TEXT="Wiederholung"/>
<node COLOR="#ff0000" CREATED="1281262922477" ID="ID_1312768820" MODIFIED="1281264151073" TEXT="2. Schulaufgabe">
<font NAME="Liberation Sans" SIZE="12"/>
<icon BUILTIN="yes"/>
</node>
</node>
<node CREATED="1281262180881" ID="ID_625794899" MODIFIED="1281274597894" TEXT="Woche_12"/>
</node>
<node CREATED="1281263969980" FOLDED="true" ID="ID_1308859049" MODIFIED="1382281181100" POSITION="right" TEXT="11. Klasse">
<cloud COLOR="#92b0ef"/>
<attribute_layout NAME_WIDTH="56" VALUE_WIDTH="56"/>
<attribute NAME="Stunden" VALUE="8"/>
<node CREATED="1281263876202" FOLDED="true" ID="ID_1933364534" MODIFIED="1281265014858" TEXT="Woche 1">
<node CREATED="1281264965119" ID="ID_175892000" MODIFIED="1281264997135" TEXT="ERM"/>
</node>
<node CREATED="1281263901545" ID="ID_1842590011" MODIFIED="1281265027033" TEXT="Woche_2"/>
<node CREATED="1281263901545" ID="ID_1503645445" MODIFIED="1281265090990" TEXT="Woche_3"/>
<node CREATED="1281263901545" FOLDED="true" ID="ID_689068238" MODIFIED="1281265094191" TEXT="Woche_4">
<node CREATED="1281264980734" ID="ID_658213349" MODIFIED="1281264988140" TEXT="ERM"/>
</node>
<node CREATED="1281263901545" FOLDED="true" ID="ID_649270317" MODIFIED="1281265097475" TEXT="Woche_5">
<node CREATED="1281264980734" ID="ID_124512560" MODIFIED="1281264988140" TEXT="ERM"/>
</node>
<node CREATED="1281263901545" FOLDED="true" ID="ID_782007010" MODIFIED="1281265100410" TEXT="Woche_6">
<node CREATED="1281264980734" ID="ID_757929234" MODIFIED="1281264988140" TEXT="ERM"/>
</node>
<node CREATED="1281263901545" FOLDED="true" ID="ID_1390149371" MODIFIED="1281265103356" TEXT="Woche_7">
<node CREATED="1281264980734" ID="ID_384895331" MODIFIED="1281264988140" TEXT="ERM"/>
</node>
<node CREATED="1281263901545" FOLDED="true" ID="ID_554081091" MODIFIED="1281265106321" TEXT="Woche_8">
<node CREATED="1281264980734" ID="ID_326801636" MODIFIED="1281264988140" TEXT="ERM"/>
</node>
<node CREATED="1281263901545" FOLDED="true" ID="ID_1340973434" MODIFIED="1281265109271" TEXT="Woche_9">
<node CREATED="1281264980734" ID="ID_1802494283" MODIFIED="1281264988140" TEXT="ERM"/>
<node CREATED="1281263901545" FOLDED="true" ID="ID_1766004117" MODIFIED="1281265027033" TEXT="Woche_2">
<node CREATED="1281264980734" ID="ID_191519607" MODIFIED="1281264988140" TEXT="ERM"/>
</node>
<node CREATED="1281263901545" FOLDED="true" ID="ID_898215315" MODIFIED="1281265027033" TEXT="Woche_2">
<node CREATED="1281264980734" ID="ID_67471647" MODIFIED="1281264988140" TEXT="ERM"/>
</node>
</node>
<node CREATED="1281263901545" FOLDED="true" ID="ID_1866796860" MODIFIED="1281265112589" TEXT="Woche_10">
<node CREATED="1281264980734" ID="ID_688464387" MODIFIED="1281264988140" TEXT="ERM"/>
<node CREATED="1281263901545" FOLDED="true" ID="ID_809965694" MODIFIED="1281265027033" TEXT="Woche_2">
<node CREATED="1281264980734" ID="ID_1191990047" MODIFIED="1281264988140" TEXT="ERM"/>
</node>
<node CREATED="1281263901545" FOLDED="true" ID="ID_1590295434" MODIFIED="1281265027033" TEXT="Woche_2">
<node CREATED="1281264980734" ID="ID_1311789910" MODIFIED="1281264988140" TEXT="ERM"/>
</node>
</node>
<node CREATED="1281263901545" FOLDED="true" ID="ID_695521113" MODIFIED="1281265135339" TEXT="Woche_11">
<node CREATED="1281264980734" ID="ID_678705273" MODIFIED="1281264988140" TEXT="ERM"/>
</node>
<node CREATED="1281263901545" FOLDED="true" ID="ID_1409681629" MODIFIED="1281265139964" TEXT="Woche_12">
<node CREATED="1281264980734" ID="ID_1234675103" MODIFIED="1281264988140" TEXT="ERM"/>
</node>
</node>
<node CREATED="1281264005761" FOLDED="true" ID="ID_301252938" MODIFIED="1382281220182" POSITION="left" TEXT="12. Klasse">
<cloud COLOR="#f29494"/>
<attribute_layout NAME_WIDTH="56" VALUE_WIDTH="56"/>
<attribute NAME="Stunden" VALUE="7"/>
<node CREATED="1281263876202" ID="ID_110179529" MODIFIED="1382281215663" TEXT="Woche 1">
<node CREATED="1281264965119" ID="ID_901879512" MODIFIED="1281264997135" TEXT="ERM"/>
</node>
<node CREATED="1281263901545" ID="ID_613934704" MODIFIED="1281265027033" TEXT="Woche_2"/>
<node CREATED="1281263901545" ID="ID_663637463" MODIFIED="1281265090990" TEXT="Woche_3"/>
<node CREATED="1281263901545" ID="ID_1961038065" MODIFIED="1382281217904" TEXT="Woche_4">
<node CREATED="1281264980734" ID="ID_561357412" MODIFIED="1281264988140" TEXT="ERM"/>
</node>
<node CREATED="1281263901545" FOLDED="true" ID="ID_57091040" MODIFIED="1281265097475" TEXT="Woche_5">
<node CREATED="1281264980734" ID="ID_1404074997" MODIFIED="1281264988140" TEXT="ERM"/>
</node>
<node CREATED="1281263901545" FOLDED="true" ID="ID_1049586507" MODIFIED="1281265100410" TEXT="Woche_6">
<node CREATED="1281264980734" ID="ID_133912926" MODIFIED="1281264988140" TEXT="ERM"/>
</node>
<node CREATED="1281263901545" FOLDED="true" ID="ID_1946324563" MODIFIED="1281265103356" TEXT="Woche_7">
<node CREATED="1281264980734" ID="ID_1158892176" MODIFIED="1281264988140" TEXT="ERM"/>
</node>
<node CREATED="1281263901545" FOLDED="true" ID="ID_227559441" MODIFIED="1281265106321" TEXT="Woche_8">
<node CREATED="1281264980734" ID="ID_1815252690" MODIFIED="1281264988140" TEXT="ERM"/>
</node>
<node CREATED="1281263901545" FOLDED="true" ID="ID_1277538171" MODIFIED="1281265109271" TEXT="Woche_9">
<node CREATED="1281265219653" ID="ID_1298830285" MODIFIED="1281265226415" TEXT="Allgemeine Themen"/>
</node>
<node CREATED="1281263901545" FOLDED="true" ID="ID_1225925773" MODIFIED="1281265112589" TEXT="Woche_10">
<node CREATED="1281264980734" ID="ID_1658273213" MODIFIED="1281264988140" TEXT="ERM"/>
<node CREATED="1281263901545" FOLDED="true" ID="ID_887267536" MODIFIED="1281265027033" TEXT="Woche_2">
<node CREATED="1281264980734" ID="ID_638418023" MODIFIED="1281264988140" TEXT="ERM"/>
</node>
<node CREATED="1281263901545" FOLDED="true" ID="ID_1633000202" MODIFIED="1281265027033" TEXT="Woche_2">
<node CREATED="1281264980734" ID="ID_587853715" MODIFIED="1281264988140" TEXT="ERM"/>
</node>
</node>
<node CREATED="1281263901545" ID="ID_1680506633" MODIFIED="1281265135339" TEXT="Woche_11"/>
</node>
</node>
</map>
