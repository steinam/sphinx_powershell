.. include:: /Abbrev.txt

PowerShell für ActiveDirectory
******************************

Beispiel Creativ GmbH
=====================

Im folgenden Beispiel wird für eine beispielhafte Firma Creativ GmbH der Aufbau eines ActiveDirectory dargestellt.

.. image:: images/adcreativ.png
.. image:: image	s/ad_groups.png

Innerhalb der Domäne steinam.test werden folgende Aktionen durchgeführt

- OU Creative
-  OU Auftragsbearbeitung
-  OU Geschaeftsleitung
-  OU ServiceUndSupport
- - Multimedia
- - Training
-  SoftwareUndTrainig
- OU Groups
-  mehrere globale Gruppen
- User
-  Anlegen von mehreren Usern in der OU Geschaeftsleitung
-  Zuweisen der User zu diversen Gruppen


.. code-block:: sh

	Import-Module ActiveDirectory

	$OUListe = @("Geschaeftsleitung", "Auftragsbearbeitung", "ServiceUndSupport", "SoftwareUndTraining")
	$Domain = [ADSI]""
	
	
	#Erzeugen der OUs
	$Company = $Domain.create("organizationalUnit", "OU=Creative")
	$Company.setInfo()
	
	foreach($element in $OUListe)
	{
		$OU = $Company.create("organizationalUnit", "OU=" + $element)
		$OU.setInfo()
	}
	
	$UnterOUListe = @("MultiMedia", "Training")
	
	#Referenz auf ServiceUndSupport-OU holen
	#Wäre auch mit Variable $company möglich gewesen, da diese auf Creative verweist
	$ldap = "OU=ServiceUndSupport, OU=Creative"
	$dn = $Domain.distinguishedName
	$SuS = [ADSI]"LDAP://$ldap, $dn"
	
	#Iteration durch Arry mit Unter-OUs
	foreach($element in $UnterOUListe)
	{
		$UnterOU = $SuS.create("organizationalUnit", "OU=" + $element)
		$UnterOU.setInfo()
	}
	
	
	
	#Anlegen einer Gruppe
	#1. Anlegen einer OU
	#2. Referenz auf OU holen bzw. nutzen
	#3. In OU mit Hilfe von Create("group", "CN=Gruppenname") die Gruppe anlegen
	#4. und Referenz auf Gruppe in Variable halten
	#5. Über die Variable den Gruppentyp festlegen
	#6. Gruppe mit Hilfe von SetInfo() in das AD schreiben
	
	
	#Liste mit anzulegenden Gruppen
	$grpListe = @("Creativ.G", "Geschaeftsleitung.G", "Management.G", "Personal.G","Software.G","Support.G", "Training.G")
	
	#1
	$GruppenOU = $Domain.create("organizationalUnit", "OU=Groups")
	$GruppenOU.setInfo()
	foreach($element in $grpListe)
	{
		$gruppe = $GruppenOU.create("group",  "CN=" + $element)
		#globale Gruppe anlegen
		$gruppe.psbase.InvokeSet("groupType", 2)
		$gruppe.SetInfo()
	}
	
	
	
	#Referenz auf OU Geschaeftsleitung holen
	$OU_GL = "OU=Geschaeftsleitung, OU=Creative"
	$dn = $Domain.distinguishedName
	
	$GL = [ADSI]"LDAP://$OU_GL, $dn"
	
	#diee Vorgehensweise erzeugt in Win2008 die "preliminary Win2000-User-Version"
	$user = $GL.Create("User", "CN=Mahn")
	$user.SetInfo()
	$user.Description = "Wolfgang Mahn"
	$user.SetPassword("TopSecret99")
	
	$user.Put(“sAMAccountName”,"mahn")
	$user.psbase.InvokeSet('AccountDisabled', $false)
	$user.SetInfo()
	
	#so legt man einen User mit den AD-Commandlets von PS an
	New-ADUser –sAMAccountName „Yusuf“ –UserPrincipalName Yusuf@ad.dikmenoglu.de –givenname “Yusuf” –Surname “Dikmenoglu” –displayName “Yusuf Dikmenoglu” –Name “Yusuf Dikmenoglu” –scriptpath “login.bat” –Enabled $true –Path "OU=Geschaeftsleitung, OU=Creative,DC=steinam,DC=test” –AccountPassword (ConvertTo-Securestring “Pa$$w0rd!” –asplaintext –Force)
	
	
	#User steinam der Gruppe Creativ.G hinzufügen
	
	$Ou_creativ = "CN=Creativ.G, OU=Groups, DC=steinam, DC=test"
	$creativgruppe = [ADSI]"LDAP://$Ou_creativ"
	$creativgruppe.member.add("CN=Mahn,OU=Geschaeftsleitung,OU=Creative,DC=steinam,DC=test")
	$creativgruppe.setinfo()
	


Zuordnen eines Homeverzeichnisses pro User
==========================================

Siehe dazu: http://www.powershell.nu/2009/04/27/part-116-adding-homefolder-through-powershell/


Das Anlegen eines Homeverzeichnisses für einen User kann in folgenden Schritten erfolgen:

    * Anlegen einer Freigabe auf dem Server
    * Anlegen eines Ordners für jeden User innerhalb der Freigabe
    * Angemessene Rechtevergabe für den jeweiligen Ordner
    * Zuweisen des Homeverzeichnisses im jeweiligen User-Objekt des AD



See :download:`Add-STShare.ps1 </material/Add-STShare.ps1>`.

:abbr:`LIFO (last-in, first-out)`

:menuselection:`Start --> Programs`




.. todo:: Skript um CVS-Import der User erweitern  
	
Useranlage über CSV-Dateien, etc
================================

In größeren Systemen werden User nicht einzeln angelegt; die Informationen gelangen aus Datenbanken bzw. Textdateien in das AD.

Im folgenden Beispiel wird eine csv-Datei genommen, um die notwendigen Informationen zur Useranlage zu erhalten.

.. code-block:: sh

	Aufbau der csv-Datei
	
	Name	, Login, Passwort, Gruppe, OU
	STE, steinam, P@sswOrt, Creativ.G, Geschaeftsleitung
	SIE, sierl, sIeRl, Support.G, SoftwareUndService
   WAL, wallner, w@llNer, Management.G , Auftragsbearbeitung
	.....


Filter
======

Beispiel 1: Auflisten aller Objekte einer OU (ohne Rekursion)

.. code-block:: sh

	$OU=([adsi]"LDAP://ou=User,ou=scripting,dc=dom1,dc=intern")
	$ou.children | foreach {$_.name}

	
Beispiel 2: Suchen ab einem Eintiegspunkt nach einem Namensbestandteil (mit Rekursion)

.. code-block:: sh

	#DN der Domaäne und PDCe bestimmen
	function getRootDSEandPDCe {  
    	$rootDSE = [ADSI]"LDAP://rootDSE"
		$fndomainDN = $rootDSE.defaultNamingContext
		$fndomainDN  #erstes Suchergebnis
	
		$domain=[System.DirectoryServices.ActiveDirectory.Domain]::getcurrentdomain()
		$fnPDCe=$domain.PdcRoleOwner.Name
		$fnPDCe
	}

	#Rekursiv die Domäne durchsuchen. Einstiegspunkt der Suche ist fnEntry z.B. DC=Dom1,DC=Intern
	function findEntriesInSubOUs($fnEntry){  
    		$AD=[ADSI]"LDAP://$fnEntry" #andere Schreibweisen im Beispiel von Kap. 2.1
         if($fnentry -like "*$SearchObject*"){
           $fnEntry   #die Suchergebnisse werden als Array gespeichert
         }
         $AD.children | foreach {  #Rekursion
         findEntriesInSubOUs($_.distinguishedname)}
    }

	$ergebnis=getRootDSEandPDCe
	$DomainDN=$ergebnis[0]
	$PDCe=$ergebnis[1]

	write-host -foregroundcolor blue  "DN der Domäne: $DomainDN"
	write-host -foregroundcolor darkyellow "PDCEmulator:   $PDCe"
	#weitere Farben unter "get-help write-host" -> Syntax
	""
	$Searchobject="test"	
	$SearchErgebnis=findEntriesInSubOUs($domaindn) #domaindn ist der Startpunkt der Suche
	$SearchErgebnis | foreach {write-host -foregroundcolor red $_}

	#Ausgabe

	DN der Domäne: DC=Dom1,DC=intern
	PDCEmulator:   DC1.Dom1.intern

	CN=Share-Test1,OU=scripting,DC=Dom1,DC=intern
	CN=test-2,OU=scripting,DC=Dom1,DC=intern
	CN=test-leer,OU=scripting,DC=Dom1,DC=intern


Queries mit [ADSISearcher] oder System.Directory.DirectorySearcher
==================================================================

[ADSISearcher] oder gleichbedeutend " System.Directoryservices.Directorysearcher" bietet eine Vielzahl an Methoden und Eigenschaften an, um LDAP-Queries ganz genau auf seine Anforderungen hinzu designen. Allerdings erfordert die durch diese Vielzahl erreichte Flexibilität sowohl ein gewisses Maß an Verständnis der Materie wie auch sorgfätige Tests.

Beispiel 1: vier verschiedene Syntaxvarianten für die .Net-Klasse System.Directoryservices.Directorysearcher

.. code-block:: sh

	([ADSISearcher]"(|(Name=*Test*)(Name=*Napf*))").findall() | select path 
	
	([system.directoryservices.directorysearcher]"(|(Name=*Test*)(Name=*napf*))").findall() | select path
	
	$ds=new-object -typename system.directoryservices.directorysearcher
	$ds.filter="(|(Name=*Test*)(Name=*Napf*))"
	$ds.findall()  | select path
	
	$ds=([ADSISearcher]"LDAP://")  #meine favorisierte Schreibweise
	$ds.filter="(|(Name=*Test*)(Name=*napf*))"
	$ds.findall()  | select path
	
	 
	
	# Ausgabe, wenn Objekte vorhanden sind, deren Name "Napf" oder "Test" enthält
	#die Ausgabe erscheint viermal identisch
	
	Path
	----
	LDAP://CN=Napf,CN=Users,DC=Dom1,DC=intern
	LDAP://CN=test-leer,OU=scripting,DC=Dom1,DC=intern
	LDAP://CN=test-2,OU=scripting,DC=Dom1,DC=intern
	LDAP://CN=Share-Test1,OU=scripting,DC=Dom1,DC=intern

	
	
Suchanfragen in PS
==================


PS kann über verschiedene Wege Suchanfragen an einen AD-Server stellen. Die folgenden Beispiele stellen einige Szenarien sowie deren unterschiedliche Lösungsansätze dar.

Suche nach einem User
---------------------

Mit Hilfe von ADO
~~~~~~~~~~~~~~~~~

The first example uses ADO in a PowerShell script. The steps are very similar to those that would be used in a VBScript program. We create ADO connection and command objects, assign properties like Page Size and Timeout, then assign an LDAP query with the same four clauses used in a VBScript program. The first clause specifies the "base" of the query, the second clause is an LDAP filter, the third clause is a comma delimited list of attributes, and the fourth clause specifies the scope. This script will work in PowerShell v1 or v2.


.. code-block:: sh

	# FindUser1.ps1
	# Specify Common Name of user.
	$strName = "James K. Smith"
	
	$strDomain = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain()
	$strRoot = $strDomain.GetDirectoryEntry()
	
	$adoConnection = New-Object -comObject "ADODB.Connection"
	$adoCommand = New-Object -comObject "ADODB.Command"
	$adoConnection.Open("Provider=ADsDSOObject;")
	$adoCommand.ActiveConnection = $adoConnection
	$adoCommand.Properties.Item("Page Size") = 100
	$adoCommand.Properties.Item("Timeout") = 30
	$adoCommand.Properties.Item("Cache Results") = $False
	
	$strBase = $strRoot.distinguishedName
	$strAttributes = "distinguishedName"
	$strScope = "subtree"
	
	$strFilter = "(cn=$strName)"
	$strQuery = "<LDAP://$strBase>;$strFilter;$strAttributes;$strScope"
	$adoCommand.CommandText = $strQuery
	$adoRecordset = $adoCommand.Execute()
	
	Do
	{
		$adoRecordset.Fields.Item("distinguishedName") | Select-Object Value
		$adoRecordset.MoveNext()
	} Until ($adoRecordset.EOF)
	
	$adoRecordset.Close()
	$adoConnection.Close()



	
Mit Hilfe des DirectorySearchers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


The next program uses the System.DirectoryServices.DirectorySearcher class to query Active Directory. We still are able to specify Page Size, the base of the query, and the LDAP filter. We use the PropertiesToLoad property to specify the attributes values to be retrieved. If we don't use this property, PowerShell will retrieve all attribute values, which will slow the program. This script will work in PowerShell v1 or v2.

.. code-block:: sh

	# FindUser2.ps1
	# PowerShell script to query AD for user with specified Common Name.
	#
	# ----------------------------------------------------------------------
	# Copyright (c) 2011 Richard L. Mueller
	# Hilltop Lab web site - http://www.rlmueller.net
	# Version 1.0 - January 9, 2011
	#
	# This program demonstrates how to use the 
	# System.DirectoryServices.DirectorySearcher class to query Active
	# Directory. This example finds the Distinguished Name of all objects
	# (there could be more than one) that have a specified Common Name.
	#
	# Specify Common Name of user.
	$strName = "James K. Smith"
	
	$strDomain = New-Object System.DirectoryServices.DirectoryEntry
	$objSearcher = New-Object System.DirectoryServices.DirectorySearcher
	$objSearcher.SearchRoot = $strDomain
	$objSearcher.PageSize = 100
	$objSearcher.SearchScope = "subtree"
	
	# Specify attribute values to retrieve.
	$arrAttributes = @("distinguishedName")
	ForEach($strAttribute In $arrAttributes)
	{
		$objSearcher.PropertiesToLoad.Add($strAttribute) > $Null
	}
	
	# Filter on object with specified Common Name.
	$objSearcher.Filter = "(cn=$strName)"
	
	$colResults = $objSearcher.FindAll()
	ForEach ($strResult In $colResults)
	{
		$strDN = $strResult.Properties.Item("distinguishedName")
		Write-Host $strDN
	}




ActiveDirectory CmdLet
~~~~~~~~~~~~~~~~~~~~~~


Finally we have a PowerShell script that uses the new Active Directory cmdlets in PowerShell v2 installed with Windows Server 2008 R2. This example uses the Get-ADObject cmdlet. We use the LDAPFilter parameter to specify our LDAP filter. This script requires PowerShell v2.

.. code-block:: sh

	# FindUser3.ps1
	# This program demonstrates how to use the Active Directory cmdlet
	# Get-ADObject to query Active Directory. This example finds the
	# Distinguished Name of all objects (there could be more than one) that
	# have a specified Common Name.
	#
	# Specify Common Name of user.
	
	$strName = "James K. Smith"
	
	Get-ADObject -LDAPFilter "(cn=$strName)" -Properties distinguishedName | Format-Table distinguishedName




Get-ADUser-Cmdlet
~~~~~~~~~~~~~~~~~

.. code-block:: sh

	Import-module ActiveDirectory

	#To query for user accounts, use the Get-ADUser cmdlet. For example, here is #how you would query against your domain for all user accounts:
	
	Get-ADUser -Filter * -SearchBase "DC=ad,DC=company,DC=com"
	
	#If you wanted to query for all of the user accounts with the last name #“Collicott”, you would run the following:
	
	Get-ADUser -Filter {Surname -eq "Collicott"} -SearchBase "DC=ad,DC=company,DC=com"
	
	#To export the e-mail addresses for all user accounts to a CSV file, you #could run the following:
	Get-ADUser -Filter * -SearchBase "DC=ad,DC=company,DC=com" -Properties mail | Select mail | Export-CSV "Email Addresses.csv"
	
	#You can also find additional examples by viewing the help on the cmdlet:
	Get-Help Get-ADUser -examples


Weitere Beispiele
~~~~~~~~~~~~~~~~~

http://blog.dikmenoglu.de/CategoryView,category,Active%2BDirectory,AD-Powershell.aspx

http://technet.microsoft.com/en-us/library/ff730967.aspx (Searching Active Directory with Windows PowerShell)

:download:`Searching_AD_with_PS.pdf <material/Searching_AD_with_PS.pdf>`



Url
===

RSAT Tools für Win7: http://www.microsoft.com/en-us/download/details.aspx?id=7887

Eigenschaften aller AD-Klassen: http://msdn.microsoft.com/en-us/library/ms680987%28v=VS.85%29.aspx http://technet.microsoft.com/en-us/library/cc755809%28WS.10%29.aspx http://technet.microsoft.com/en-us/library/cc780455%28v=ws.10%29.aspx http://technet.microsoft.com/en-us/library/cc759550%28v=ws.10%29.aspx







