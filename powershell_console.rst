PowerShell auf der Konsole
**************************

Während Microsoft bereits seit vielen Jahren für seine Anwender- und Betriebssystemsoftware bekannt ist, hat sie relativ lange Zeit wenig Resoourcen in die Bereitstellung von Scriptingumgebungen gesteckt.

Relativ lange war die DOS-Konsole mit seinen wenigen Befehlen die einzige Möglichkeit, Windows von der Konsole her anzupacken. Mit Beginn von Windows 95 wurde deshalb mit der Scriptsprache **vbscript** die Möglichkeit geschaffen, für den Systemadministrator eine Plattform zur Verfügung zu stellen, um Windows Betriebssysteme automatisieren zu können.

Mit dem Erscheinen der .NET-Programmierplattform und dem Wechsel von der alten COM-Welt zu dieser neuen Plattform gab es allerdings zunächst keine Unterstützung dieses Frameworks aus Sicht des Systemadministrators.

Powershell schließt diese Lücke. Es baut auf dem .NET-Framework 2.0 auf und ermöglicht den Zugriff sowohl auf die alte COM-Welt als auch auf die Klassen des .NET-Frameworks.


.. image:: images/ps4admin01.jpg
.. image:: images/ps4admin02.jpg


.. image:: images/powershell_architecture.jpg


Installation von Powershell
---------------------------

PowerShell ist seit Windows 7 und Server 2008 ein integraler Bestandteil des Betriebssystems. Unter früheren Systemen kann man es nachinstallieren.


Hilfe und Tutorials
-------------------

Nach der Installation von Powershell findet man eine lokalisierte Hilfe als CHM-Datei.


.. image:: images/powershell_chmhelp.jpg


Weiterhin gibt es mittlerweile viele Webseiten bzw. Tutorials, die sich mit der PowerShell beschäftigen.

  * http://technet.microsoft.com/en-us/scriptcenter/dd742419.aspx
  * http://powershellcommunity.org/
  * http://powershell.com/
  * http://channel9.msdn.com/tags/PowerShell/

  Mittlerweile gibt es eine ganze Reihe von Büchern, Amazon listet u.a. diese

  * http://www.amazon.de/registry/wishlist/D9P356SKZ575?reveal=unpurchased&filter=3&sort=date-added&layout=standard&x=2&y=1


Umgang mit der Konsole und GUI
------------------------------

Der erste Eindruck von Powershell wird sicherlich von dem blauen Konsolenfenster bestimmt, welches sich nach einem Klick auf Powershell Ã¶ffnet.

.. image:: images/powershell_console.jpg

Während PS in der Version 1.0 über keine weiteren *grafischen* Elemente verfügte, hat Microsoft der Version 2.0 eine grafische Oberfläche spendiert.

.. image:: images/powershell_gui.jpg

Daneben gibt es eine Reihe weiterer Programme, die ein grafische Benutzeroberfläche incl. Systaxhighlightning und CodeCompleting bieten. Im Folgenden werden wir das Programm (`PowerGUI <http://www.quest.com>`__) der Firma Quest Software benutzen. Neben dem kostenlosen Skripteditor bietet die Firma auch einige interessante Commandlets zur Manipulation des ActiveDirectory und des ExchangeServers.


(`PowerGui <http://www.powergui.org>`__) ist ein Freeware-Programm, welches sowohl über eine Konsole als auch ein Skriptfenster verfügt. Das Skriptfenster verfügt über Code-Completing und Syntax-Highlightning und ist deshalb für umfangreichere Programme die erste Wahl.

.. image:: images/powergui.png

Die Wahl des Editors ist letztlich eine Geschmacksfrage.





Erste Schritte auf der Konsole:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- Mulit-Line-Entries
- Wichtige Keyboard-Shortcuts
- Command-History
- Automatische Eingabe-Vervollständigung
- Text auswählen und einfügen


After PowerShell starts, its console window opens, and you see a blinking text prompt, asking for your input with no icons or menus. PowerShell is a command console and almost entirely operated via keyboard input. The prompt begins with "PS" and after it is the path name of the directory where you are located. Start by trying out a few commands. For example, type:


Anpassen der Konsole
^^^^^^^^^^^^^^^^^^^^^

- Konsole-Eigenschaften
- Fonts und Fontgröße
- Window- und Buffersize
- Farben


Piping and Redirecting
^^^^^^^^^^^^^^^^^^^^^^

Häufig werden die Informationen, die die PowerShell ausgibt, über mehrere Bildschirmseiten verteilt. Um dies besser kontrollieren zu können, kann man mit Hilfe des sog. *Piping* eine seitenweise Darstellung erreichen. Geben Sie zum besseren Verständnis einfach einmal folgende Befehle ein:


.. code-block:: sh

	Get-Process
	Get-Process | more (Enter)

Im 2. Befehl wird das *Piping*-Symbol | benutzt, um eine seitenweise Darstellung zu erreichen. Im späteren Verlauf wird auf dieses mächtige Konzept noch näher eingegangen.


.. image:: images/piping_more.png




Häufig will man die Ausgabe nicht nur auf dem Bildschirm sehen, sondern die Ergebnisse in einer Datei festhalten. Dazu dient das ">" - Symbol, welches die Ausgabe in eine Datei umlenkt.


.. code-block:: sh

	help > help.txt (Return)

Um nun die Datei in Notepad zu öffnen, muss man lediglich auf der Konsole folgenden Befehl eingeben.

.. code-block:: sh

	.\help.txt


.. image:: images/redirecting.png


Die Ausgabe kann auch in eine bereits bestehende Datei umgeleitet werden. Der Text dieser Datei wird damit nicht überschrieben, sondern erweitert. Dies erfolgt mit dem Zeichen ">>".


.. code-block:: sh

	cmd /c help >> help.txt (Return)

Will man das Ergebnis eines Befehls weiterverarbeiten, muss man nicht nur mit redirecting arbeiten. Powershell kann das Ergebnis jedes Befehls in einer Variable speichern, auf die man innerhalb der bestehenden Powershell-Session Zugriff hat.

.. code-block:: sh

	 $result = ipconfig
	 $result

.. image:: images/redirecting_variable.png

Auto-Completing
^^^^^^^^^^^^^^^

Powershell kennt viele eingebaute Befehle, die natürlich schwer zu merken sind. Um Tippfehler auszuschließen, können Sie nach Eingabe der ersten Buchstaben die *TAB*-Taste drücken. Sie erhalten dann einen Vorschlag zur Vervollständigung. Weiteres Drücken der *TAB*-Taste gibt einen weiteren Vorschlag.


Tastenkürzel
^^^^^^^^^^^^

==================== =========================================================
Taste  	             Bedeutung
==================== =========================================================
(ALT)+(F7) 			 Deletes the current command history
(PgUp), (PgDn) 		 Display the first (PgUp) or last (PgDn) command you used in current session
(Enter) 			 Send the entered lines to PowerShell for execution
(End)				 Moves the editing cursor to the end of the command line
(Del) 				 Deletes the character to the right of the insertion point
(Esc)				 Deletes current command line
(F2) 				 Moves in current command line to the next character corresponding to specified character
(F4) 				 Deletes all characters to the right of the insertion point up to specified character
(F7)			   	 Displays last entered commands in a dialog box
(F8) 				 Displays commands from command history beginning with the character that you already entered in the command line
(F9) 				 Opens a dialog box in which you can enter the number of a command from your command history to return the command.
(Arrow Left/Right)	 Move one character to the left or right respectively
(Arrow up, down)	 Repeat the last previously entered command
(Home) 				 Moves editing cursor to beginning of command line
(Backspace) 		 Deletes character to the left of the insertion point
(Ctrl)+(C)			 Cancels command execution
(Ctrl)+(End)		 Deletes all characters from current position to end of command line
(Ctrl)+(Arrow left), (Ctrl)+(Arrow right)			Move insertion point one word to the left or right respectively
(Ctrl)+(Home)		 Deletes all characters of current position up to beginning of command line
(Tab)				 Automatically completes current entry, if possible
==================== =========================================================



Fragen
^^^^^^

* Wie startet man die Powershell
* Welche beiden Varianten der Powershell gibt es
* Welchen Nutzen hat die TAB-Taste
* Wie kann man sich die zuletzt eingegebenen Befehle anzeigen lassen
* Wo kann man  Informationen im Internet zur Powershell finden
* Was ist Piping
* Was ist Redirection
* Wie kann man die Farbe in der Powershell-Shell ändern


.. Aufgaben
.. ^^^^^^^^


.. Tafelbild
.. ^^^^^^^^^
