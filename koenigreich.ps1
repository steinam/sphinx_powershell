﻿cls

class Einwohner{
    [string] $Rang
    [int] $einkommen
    [string] $name
    [int] $steuer
    [double] $steuersatz = 0.1
    [int] $minimum = 1
    [int] $Freibetrag = 0

    Einwohner([int] $e, [string] $name){
        $this.einkommen = $e
        $this.name = $name
        $this.Rang = $this.GetType().ToString()
    
    }

    [int]zuVersteuerndesEinkommen(){
        if(($this.einkommen - $this.freibetrag) -gt 0){
            return ($this.einkommen - $this.freibetrag)
        }
        else{
            return 0
        }
    }

    [int] steuern(){
        $this.steuer = $this.zuVersteuerndesEinkommen() * $this.steuersatz    
        if($this.minimum -gt $this.steuer)
        {
            $this.steuer = $this.minimum
        }
            return $this.steuer
        }
}

class Koenig : Einwohner{
    
    Koenig([int] $e, [string] $name) :base($e,$name){
   
    }
    [int]zuVersteuerndesEinkommen(){
        return 0
    }

    [int]steuern(){
        return 0
    }
}

class Adel : Einwohner{
    [int] $minimum = 20

    Adel([int] $e, [string] $name) :base($e,$name){

    }
}

class Bauer : Einwohner{

    Bauer([int] $e, [string] $name) :base($e,$name){
    
    }

}

class Leibeigene : Bauer{
    [int] $Freibetrag = 12
    
    Leibeigene([int] $e, [string] $name) :base($e,$name){
    
    }

}
class Druid : Einwohner{
    [int] $minimum = 10

    Druid([int] $e, [string] $name) :base($e,$name){

    }
}

class Koenigreich{
    
    $Adel = [System.Collections.Generic.List[Adel]]::new()
    $Koenig = [System.Collections.Generic.List[Koenig]]::new()
    $Bauer = [System.Collections.Generic.List[Bauer]]::new()
    $Leibeigene = [System.Collections.Generic.List[Leibeigene]]::new()
    $Druid = [System.Collections.Generic.List[Druid]]::new()
    $Einwohner

    Koenigreich($Einwohner){
    $this.Einwohner = $Einwohner
    $this.TeileKoenigreich()
    }

    TeileKoenigreich(){
    for($i = 0; $i -lt $this.Einwohner.count;$i++){
        $tempType = $this.Einwohner[$i].GetType().ToString()
        if($tempType -eq "Adel"){
            $this.Adel.add($this.Einwohner[$i])
        }
        if($tempType -eq "Koenig"){
            $this.Koenig.add($this.Einwohner[$i])
        }
        if($tempType -eq "Bauer"){
            $this.Bauer.add($this.Einwohner[$i])
        }
        if($tempType -eq "Leibeigene"){
            $this.Leibeigene.add($this.Einwohner[$i])
        }
        if($tempType -eq "Druid"){
            $this.Druid.add($this.Einwohner[$i])
        }
    
    }
    
    
    }
    

}

$einwohnerliste = [System.Collections.Generic.List[Einwohner]]::new()
$first = "Adrian Aimée Albert Alexander Alexandra Alexis Benjamin Bennet Benno Bent Bente Berenike Leo Leon Leonardo Leonie Lewin Leyla Liam Lias Liem Lien Lilli Lilliana Lina Linus Lisa Loreen Louis Louisa Luca Lucia Luciano Maximilian Melanie Melchior Melina Melissa Melody Melvin Merlin Mette Mia Michael Michaela Michel Pepe Per Percival PeterPetra Phil Philipp Pia Pietro Pius Sophia Sophie Stefan Suri Sven Tabea Tamara Tanja Theodora Theresa Thomas Thorsten Till Tim Tobias Udo Uli Ulla Ulrich Ulrika Uriel Ursula Uta Ute Uwe Valentin Valentina Vanessa Vera Veronika Viktor Viktoria Vilmar Vinzent Vinzenz Viola Walter Wanda Wendy Werner Wibke Wilhelm Wilhelma Willi Wilma Wolfram Xana Xander Xandra Xaver Xaviera Xena Xenia Yafa Yamina Yannick Yasemin Yelena Yorick Yuma Yvonne Zacharias Zahra Zander Zarah Zarif Zeno Zoe Zohra Zora Zyprian Noah Sophia Liam Emma Jacob Olivia Mason Isabella William Ava Ethan Mia Michael Emily Alexander Abigail Jayden Madison Daniel Elizabeth".Split(" ")
function CreateName{
    return $first[ (Get-Random $first.count ) ]
}


$RandomListe = $true
$Koenigreich = (1,30,800,199,20)

if($RandomListe -eq $true){
    [int]$tempAdel = Get-Random -Minimum 1 -Maximum 50
    [int]$tempBauern = Get-Random -Minimum 100 -Maximum 1000
    [int]$tempLeibeigene = Get-Random -Minimum 50 -Maximum 400
    [int]$tempDruid = Get-Random -Minimum 1 -Maximum 20
    $Koenigreich = (1, $tempAdel,$tempBauern,$tempLeibeigene,$tempDruid)

}

$Klassen = ("Koenig","Adel","Bauer","Leibeigene", "Druid")

for($i = 0; $i -le $Koenigreich.Count; $i++){

    for($j = 0; $j -lt $Koenigreich[$i];$j++){
        
        if($Klassen[$i] -eq "Koenig"){
            [int]$temp = Get-Random -Minimum 1000 -Maximum 10000
            $tempN = CreateName
            $einwohner = [Koenig]::new($temp,$tempN)
            $einwohnerliste.add($einwohner)    
        }
        if($Klassen[$i] -eq "Adel"){
            [int]$temp = Get-Random -Minimum 150 -Maximum 1000
            $tempN = CreateName
            $einwohner = [Adel]::new($temp,$tempN)
            $einwohnerliste.add($einwohner)    
        }
        if($Klassen[$i] -eq "Leibeigene"){
            [int]$temp = Get-Random -Minimum 0 -Maximum 100
            $tempN = CreateName
            $einwohner = [Leibeigene]::new($temp,$tempN)
            $einwohnerliste.add($einwohner)    
        }
        if($Klassen[$i] -eq "Bauer"){
            [int]$temp = Get-Random -Minimum 50 -Maximum 150
            $tempN = CreateName
            $einwohner = [Bauer]::new($temp,$tempN)
            $einwohnerliste.add($einwohner)    
        }
        if($Klassen[$i] -eq "Druid"){
            [int]$temp = Get-Random -Minimum 50 -Maximum 150
            $tempN = CreateName
            $einwohner = [Druid]::new($temp,$tempN)
            $einwohnerliste.add($einwohner)    
        }
    }

}

foreach($buerger in $einwohnerliste){
    write-host $buerger.name ist $buerger.GetType() Versteuert $buerger.zuVersteuerndesEinkommen() und zahlt $buerger.steuern() Taler steuern
}
$Koenigreich = [Koenigreich]::new($einwohnerliste)
$Koenigreich.Adel
$Koenigreich.Koenig


#--------------------------------------------------------------------------------------------------------------