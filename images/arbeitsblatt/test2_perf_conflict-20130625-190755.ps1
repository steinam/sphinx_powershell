﻿[int[]]$objects = 1,2,5,10
[int]$cycles = 50000
$report = @()
ForEach ($obj in $objects) {
    Switch ($obj) {
    1   {
            $temp = “” | Select Name, Objects,Cycles, Seconds
            Write-Host -fore Green “Beginning performance test for Add-Member custom objects using $cycles iterations and $($obj) objects”
            $run = $(Measure-Command {
                        for($i = 0;$i -lt $cycles;$i++)  {
                            $a = new-Object psobject
                            $a | add-member -membertype noteproperty -name test1 -value "test1"
            }}).TotalSeconds
            Write-Host -ForegroundColor Cyan "$($run) seconds"
            $temp.Name = “Add-Member”
            $temp.Objects = $obj
            $temp.Cycles = $cycles
            $temp.seconds = $run
            $report += $temp
        }
    2   {
            $temp = "" | Select Name, Objects,Cycles, Seconds
            Write-Host -fore Green "Beginning performance test for Add-Member custom objects using $cycles iterations and $($obj) objects"
            $run = $(Measure-Command {for($i = 0;$i -lt $cycles;$i++)  {
                        $a = new-Object psobject
                        $a | add-member -membertype noteproperty -name test1 -value “test1"
                        $a | add-member -membertype noteproperty -name test2 -value “test2"
                    }}).TotalSeconds
            Write-Host -ForegroundColor Cyan "$($run) seconds"
            $temp.Name = “Add-Member”
            $temp.Objects = $obj
            $temp.Cycles = $cycles
            $temp.seconds = $run
            $report += $temp
        }
    5   {
            $temp = "" | Select Name, Objects,Cycles, Seconds
            Write-Host -fore Green “Beginning performance test for Add-Member custom objects using $cycles iterations and $($obj) objects”
            $run = $(Measure-Command {
                        for($i = 0;$i -lt $cycles;$i++)  {
                            $a = new-Object psobject
                            $a | add-member -membertype noteproperty -name test1 -value "test1"
                            $a | add-member -membertype noteproperty -name test2 -value "test2"
                            $a | add-member -membertype noteproperty -name test3 -value "test3"
                            $a | add-member -membertype noteproperty -name test4 -value "test4"
                            $a | add-member -membertype noteproperty -name test5 -value "test5"
                    }}).TotalSeconds
            Write-Host -ForegroundColor Cyan “$($run) seconds”
            $temp.Name = "Add-Member"
            $temp.Objects = $obj
            $temp.Cycles = $cycles
            $temp.seconds = $run
            $report += $temp
        }
    10  {
            $temp = "" | Select Name, Objects,Cycles, Seconds
            Write-Host -fore Green "Beginning performance test for Add-Member custom objects using $cycles iterations and $($obj) objects"
            $run = $(Measure-Command {for($i = 0;$i -lt $cycles;$i++)  {
                        $a = new-Object psobject
                        $a | add-member -membertype noteproperty -name test1 -value "test1"
                        $a | add-member -membertype noteproperty -name test2 -value “test2"
                        $a | add-member -membertype noteproperty -name test3 -value "test3"
                        $a | add-member -membertype noteproperty -name test4 -value "test4"
                        $a | add-member -membertype noteproperty -name test5 -value "test5"
                        $a | add-member -membertype noteproperty -name test6 -value "test6"
                        $a | add-member -membertype noteproperty -name test7 -value "test7"
                        $a | add-member -membertype noteproperty -name test8 -value "test8"
                        $a | add-member -membertype noteproperty -name test9 -value "test9"
                        $a | add-member -membertype noteproperty -name test10 -value "test10"
                    }}).TotalSeconds
            Write-Host -ForegroundColor Cyan "$($run) seconds"
            $temp.Name = "Add-Member"
            $temp.Objects = $obj
            $temp.Cycles = $cycles
            $temp.seconds = $run
            $report += $temp
        }
    }
    
    Switch ($obj) { 
    1 {
        $temp = "" | Select Name, Objects,Cycles, Seconds
        Write-Host -fore Green "Beginning performance test for Select-Object custom objects using 50000 iterations and $($obj) objects"
        $run = $(Measure-Command {for($i = 0;$i -lt 50000;$i++) {
            $a = 1 |Select-Object test1,test2,test3,test4,test5,test6,test7,test8,test9,test10
            $a.test1 = "test1"
        }}).TotalSeconds
        Write-Host -ForegroundColor Cyan “$($run) seconds”
        $temp.Name = "Select-Object"
        $temp.Objects = $obj
        $temp.Cycles = $cycles
        $temp.seconds = $run
        $report += $temp
      }
    2 {
        $temp = "" | Select Name, Objects,Cycles, Seconds
        Write-Host -fore Green "Beginning performance test for Select-Object custom objects using $cycles iterations and $($obj) objects"
        $run = $(Measure-Command {for($i = 0;$i -lt $cycles;$i++) {
                $a = 1 |Select-Object test1,test2,test3,test4,test5,test6,test7,test8,test9,test10
                $a.test1 = "test1"; $a.test2 = "test2"
        }}).TotalSeconds
        Write-Host -ForegroundColor Cyan "$($run) seconds"
        $temp.Name = "Select-Object"
        $temp.Objects = $obj
        $temp.Cycles = $cycles
        $temp.seconds = $run
        $report += $temp
      }
    5 {
        $temp = "" | Select Name, Objects,Cycles, Seconds
        Write-Host -fore Green "Beginning performance test for Select-Object custom objects using $cycles iterations and $($obj) objects"
        $run = $(Measure-Command {for($i = 0;$i -lt $cycles;$i++) {
            $a = 1 |Select-Object test1,test2,test3,test4,test5,test6,test7,test8,test9,test10
            $a.test1 = "test1"; $a.test2 = "test2"; $a.test3 = "test3"; $a.test4 = "test4"; $a.test5 = "test5"
        }}).TotalSeconds
        Write-Host -ForegroundColor Cyan "$($run) seconds"
        $temp.Name = "Select-Object"
        $temp.Objects = $obj
        $temp.Cycles = $cycles
        $temp.seconds = $run
        $report += $temp
      }
    10{
        $temp = "" | Select Name, Objects,Cycles, Seconds
        Write-Host -fore Green "Beginning performance test for Select-Object custom objects using $cycles iterations and $($obj) objects"
        $run = $(Measure-Command {for($i = 0;$i -lt $cycles;$i++) {
            $a = 1 |Select-Object test1,test2,test3,test4,test5,test6,test7,test8,test9,test10
            $a.test1 = "test1"; $a.test2 = "test2"; $a.test3 = "test3"; $a.test4 = "test4"; $a.test5 = "test5"
            $a.test6 = "test6"; $a.test7 = "test7"; $a.test8 = "test8"; $a.test9 = "test9"; $a.test10 = “test10"
        }}).TotalSeconds
        Write-Host -ForegroundColor Cyan "$($run) seconds"
        $temp.Name = “Select-Object”
        $temp.Objects = $obj
        $temp.Cycles = $cycles
        $temp.seconds = $run
        $report += $temp
      }
    }
    
    Switch ($obj) { 
    1 {
        $temp = "" | Select Name, Objects,Cycles, Seconds
        Write-Host -fore Green "Beginning performance test for Hash Table custom objects using $cycles iterations and $($obj) objects"
        $run = $(Measure-Command {for($i = 0;$i -lt $cycles;$i++) {
                    [pscustomobject] @{
                    test1 = "test1"
                    }
                }}).TotalSeconds
        Write-Host -ForegroundColor Cyan "$($run) seconds"
        $temp.Name = "HashTable"
        $temp.Objects = $obj
        $temp.Cycles = $cycles
        $temp.seconds = $run
        $report += $temp
      }
    2 {
        $temp = "" | Select Name, Objects,Cycles, Seconds
        Write-Host -fore Green "Beginning performance test for Hash Table custom objects using $cycles iterations and $($obj) objects"
        $run = $(Measure-Command {for($i = 0;$i -lt $cycles;$i++) {
                    [pscustomobject] @{
                      test1 = "test1"; test2 = "test2"
                    }
        }}).TotalSeconds
        Write-Host -ForegroundColor Cyan "$($run) seconds"
        $temp.Name = "HashTable"
        $temp.Objects = $obj
        $temp.Cycles = $cycles
        $temp.seconds = $run
        $report += $temp
       }
    5 {
        $temp = "" | Select Name, Objects,Cycles, Seconds
        Write-Host -fore Green "Beginning performance test for Hash Table custom objects using $cycles iterations and $($obj) objects"
        $run = $(Measure-Command {for($i = 0;$i -lt $cycles;$i++) {
            [pscustomobject] @{
            test1 = "test1"; test2 = "test2"; test3 = "test3"; test4 = "test4"; test5 = "test5"
        }
        }}).TotalSeconds
        Write-Host -ForegroundColor Cyan “$($run) seconds”
        $temp.Name = "HashTable"
        $temp.Objects = $obj
        $temp.Cycles = $cycles
        $temp.seconds = $run
        $report += $temp
      }
    10{
        $temp = ""| Select Name, Objects,Cycles, Seconds
        Write-Host -fore Green "Beginning performance test for Hash Table custom objects using $cycles iterations and $($obj) objects"
        $run = $(Measure-Command {for($i = 0;$i -lt $cycles;$i++) {
            New-Object PSObject -Property @{
                test1 = "test1"; test2 = "test2"; test3 = "test3"; test4 = "test4"; test5 = "test5"
                test6 = "test6"; test7 = "test7"; test8 = "test8"; test9 = "test9"; test10 = "test10"
            }
        }}).TotalSeconds
        Write-Host -ForegroundColor Cyan "$($run) seconds"
        $temp.Name = “HashTable”
        $temp.Objects = $obj
        $temp.Cycles = $cycles
        $temp.seconds = $run
        $report += $temp
       }
    }

    
}
$report