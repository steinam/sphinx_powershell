Objekte
============

Aufgabe 1
---------


Passen Sie das folgende Skriot so an, dass die Informationen nicht lediglich per Write-Host ausgegeben werden, sondern in Form eines Objektes gespeichert werden.

.. code-block:: sh

	     Function SysInfo
     {
             $colItems = Get-WmiObject Win32_ComputerSystem `
             -Namespace "root\CIMV2" -ComputerName $strComputer

             foreach($objItem in $colItems) {
               Write-Host "Computer Manufacturer: " $objItem.Manufacturer
               Write-Host "Computer Model: " $objItem.Model
               Write-Host "Total Memory: " $objItem.TotalPhysicalMemory "bytes"
             }
     }

     Function BIOSInfo {

             $colItems = Get-WmiObject Win32_BIOS -Namespace "root\CIMV2" `
             -computername $strComputer
             foreach($objItem in $colItems) {
                     Write-Host "BIOS:"$objItem.Description
                     Write-Host "Version:"$objItem.SMBIOSBIOSVersion"."
                     $objItem.SMBIOSMajorVersion"."$objItem.SMBIOSMinorVersion
                     Write-Host "Serial Number:" $objItem.SerialNumber
             }
     }


     Function OSInfo
     {
             $colItems = Get-WmiObject Win32_OperatingSystem -Namespace ` "root\CIMV2" -Computername $strComputer

             foreach($objItem in $colItems) {
                     Write-Host "Operating System:" $objItem.Name
             }
     }


     Function CPUInfo
     {
             $colItems = Get-WmiObject Win32_Processor -Namespace `
             "root\CIMV2" -Computername $strComputer

             foreach($objItem in $colItems) {
                     Write-Host "Processor:" $objItem.DeviceID $objItem.Name
             }
     }


     Function DiskInfo
     {
             $colItems = Get-WmiObject Win32_DiskDrive -Namespace "root\CIMV2" ` -ComputerName $strComputer

             foreach($objItem in $colItems) {
                     Write-Host "Disk:" $objItem.DeviceID
                     Write-Host "Size:" $objItem.Size "bytes"
                     Write-Host "Drive Type:" $objItem.InterfaceType
                     Write-Host "Media Type: " $objItem.MediaType
             }
     }


     Function NetworkInfo
     {
             $colItems = Get-WmiObject Win32_NetworkAdapterConfiguration ` -Namespace "root\CIMV2" -ComputerName $strComputer | where{$_.IPEnabled -eq "True"}

             foreach($objItem in $colItems) {
                     Write-Host "DHCP Enabled:" $objItem.DHCPEnabled
                     Write-Host "IP Address:" $objItem.IPAddress
                     Write-Host "Subnet Mask:" $objItem.IPSubnet
                     Write-Host "Gateway:" $objItem.DefaultIPGateway
                     Write-Host "MAC Address:" $ojbItem.MACAddress
             }
     }
     
     
     #*=============================================================
     #* SCRIPT BODY
     #*=============================================================
     #* Connect to computer
     $strComputer = "."

     #* Call SysInfo Function
     Write-Host "Sytem Information"
     SysInfo
     Write-Host

     #* Call BIOSinfo Function
     Write-Host "System BIOS Information"
     BIOSInfo
     Write-Host

     #* Call OSInfo Function
     Write-Host "Operating System Information"
     OSInfo
     Write-Host

     #* Call CPUInfo Function
     Write-Host "Processor Information"
     CPUInfo
     Write-Host

     #* Call DiskInfo Function
     Write-Host "Disk Information"
     DiskInfo
     Write-Host

     #* Call NetworkInfo Function
     Write-Host "Network Information"
     NetworkInfo
     Write-Host
     
     
     
Aufgabe 2
-----------

Ihn ihrer Firma wird ein Skript genutzt, welches die Aufrufe der einzelnen Webseiten pro Wochentag aus den Log-Files des Webservers erstellt und in einer Datei "weekly_Stats.csv" speichert. 

Die Struktur der Datei sieht wie folgt aus:


site_url, monday, tuesday, wednesday, thursday, friday, saturday, sunday
index.php,20000,14324,23453,43213,56432,23451,12343
login/login.php, 23432,34321,34567,43567,5432,43256,0
login/logout.php,12345,22345,32456,4232,3453,23456,1234
....

Insgesamt ist von ca 700 Zeilen in der Datei auszugehen.




Ihr Chef möchte nun als weitere Information für die jeweiligen urls den wöchentlichen Durchschnitt pro url berechnet bekommen und eine nach dem Durchschnitt sortierte Ausgabe in einer Textdatei speichern.

Erstellen Sie ein entsprechendes Skript. Benutzen Sie wenn möglich objektorientierte Ansätze sowie die Pipeline.

Aufgabe 3
---------

Gegeben ist folgendes PS-Skript.

.. code-block:: sh

	Function Get-NetworkConfiguration
	{
	    param (
		[parameter(
		    ValueFromPipeline=$true,
		    ValueFromPipelineByPropertyName=$true,
		    Position=0)]
		[Alias('__ServerName', 'Server', 'Computer', 'Name')]   
		[string[]]
		$ComputerName = $env:COMPUTERNAME,
		[parameter(Position=1)]
		[System.Management.Automation.PSCredential]
		$Credential
	    )
	    process
	    {
		$WMIParameters = @{
		    Class = 'Win32_NetworkAdapterConfiguration'
		    Filter = "IPEnabled = 'true'"
		    ComputerName = $ComputerName
		}
	       
		if ($Credential -ne $Null)
		{
		    $WmiParameters.Credential = $Credential
		}       
		foreach ($adapter in (Get-WmiObject @WMIParameters))
		{
		    $OFS = ', '
		    Write-Host "Server: $($adapter.DNSHostName)"
		    Write-Host "Adapter: $($adapter.Description)"
		    Write-Host "IP Address: $($adapter.IpAddress)"
		    Write-Host "Subnet Mask: $($adapter.IPSubnet)"
		    Write-Host "Default Gateway: $($adapter.DefaultIPGateway)"
		    Write-Host "DNS Servers: $($adapter.DNSServerSearchOrder)"
		    Write-Host "DNS Domain: $($adapter.DNSDomain)"
		    Write-Host
		}
	       
	    }
	}
	
	
- Wählen Sie statt der Write-Host-Ausgabe einen objektorientierten Ansatz

- Sie wollen nicht alle Informationen ausgeben, sondern beispielsweise nur die IP-Adresse und die Subnet-Maske

- Können Sie mit diesem Skript mehrere Rechner abfragen ?

- Welche Rechner ihres Netzes benutzen den gleichen DNS-Server mit der 
  IP-Adresse ***.***.***.***. Eine Liste der Computer erhalten Sie mit Hilfe des CommandLets get-adcomputer


- Wie viele Rechner benutzen den gleichen Default-Gateway

- Übersetzen Sie den folgenden Text:

    When you begin to output options from your functions, the reusability and extensibility options soar. Objects allow you to take advantage of the pipeline (which allows your objects to be the input to other commands), allow your commands to use incoming objects (via the Parameter attribute and ValueFromPipelineByPropertyName switch), and most importantly, allow you to leverage the formatting and output system that Windows PowerShell provides, which keeps you from having to design your own output options and saves your effort for the real work—getting your job done!
  	
  	

