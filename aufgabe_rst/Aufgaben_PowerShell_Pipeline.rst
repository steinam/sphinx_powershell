Pipeline
========

Lösungen sind hier :ref:`loesung_powershell_pipeline`:

- You need a report of the top consumers of virtual memory and RAM on a server.

- You need a report of the largest files in a subtree. 

- Die Ausgabe des folgenden Powershell-Befehls dauert u.U. sehr lange. Verbessern Sie das Statement im Hinblick auf die Ausführungsgeschwindigkeit 
                                                                    
  .. code-block:: sh
   
     # Foreach loop lists each element in a collection:
	 Foreach ($element in Dir C:\ -recurse) { $element.name }

- Welche Alias gibt es für das CmdLet get-process
   
- Welche Prozesse belegen mehr als 20 MByte  

- Sortieren Sie das obige Ergebnis nach der Größe

- Geben Sie die Firma, den Produkt, Namen und Version der laufenden Prozesse aus
  
- Ermitteln Sie alle Word-Dateien im Verzeichnis h:\Daten und seinen  Unterverzeichnissen. Beschränken Sie die Ergebnismenge auf diejenigen Objekte, bei denen das Attribut Length größer ist als 40 000 Bytes. Geben Sie lediglich die Attribute Name und Length aus und sortieren Sie die Ausgabe nach der Länge. Exportieren Sie  die Liste in eine .csv-Datei. 

- Geben Sie alle Unterordner bis zu einer bestimmten Tiefe aus, ausgehend vom jeweils aktuellen Ordner

- Sie wollen prüfen, ob auf bestimmten Rechnern innerhalb ihres LANs Software bestimmter Herstellner vorhanden ist. Schreiben Sie ein entsprechendes Skript

- Erweitern Sie obiges Skript um die Abfrage nach bestimmten Softwarenamen bzw. Versionsständen.

- I needed to merge 365 CSV files that represent daily weather data sets into one CSV file that contains all the data accumulated during one year. Each of the daily CSV files had a header row. The yearly file should only have one. Filtering out rows is a perfect application of the PowerShell filter functions. Übersetzen Sie den anglischen Text sinngemäß und schreiben Sie ein entsprechendes Skript.


- Suchen Sie die letzten 2000 Systemereignisse des Eventlogs, die vom Typ "Error" sind
		
- Sie schreiben ein Powershell-Skript, welches eine eigene Ereignisquelle im Eventlog schafft und darunter seine Ereignisse schreibt.
  		
- Schreiben Sie die Einträge der obigen Quelle in eine CSV-Datei. Es soll nur die jeweils letzte Stunde eingetragen werden
		
- A Real-life Example of Select-String

  My practical problem was that I wanted to search for instances a particular string in a directory with hundreds of file.  If a file had duplicate entries, then I needed to know.

  The PowerShell problem is how to create a stream of the files, then compare each file with my string value.  In the output I needed to know the filename.

  To solve the problem, I employed four main commands, which you can see in the following script:

- Auflöung einer IP-Range in deren Namen

	Erstellen Sie ein Skript, welches eine vordefinierte IP-Range durchläuft und zu jeder IP-Adresse bei Erreichbarkeit den Rechnernamen ausgibt.

	Versuchen Sie dabei mögliche Fehleingaben durch Programmierlogik abzufangen

	Speichern Sie anschließend die Kombination von IP-Adresse und Rechnername in  einer CSV-Datei

	Für die Implementierung können Sie folgende Funktionalität benutzen:

	Test-Connection: 
	Cmdlet, welches die Erreichbarkeit eines Rechners zurückgibt

	[System.Net.Dns]::GetHostbyAddress($IPAddress) 
	Funktionalität des .net-Frameworks, welches eine Namensauflösung für eine gegebene IP-Adresse vornimmt.
 
- Laptop oder Desktop

	Sie sind die IT-Leiter einer kleinen mittelständischen Firma. Sie müssen Inventur machen und Sie brauchen dazu die Anzahl der zur Zeit sich im Netz befindlichen Laptop-und Desktop-Computer in ihrer Firma.

	Um eine weitere Verarbeitung der Daten zu ermöglichen, sollte Sie den Computernamen, seine IP-Adresse, die Uhrzeit sowie die Aussage, ob es ein Laptop oder Desktop ist, speichern.

	Sie planen folgende Vorgehensweise:

	- Ihr Skript soll während des Login-Vorganges aufgerufen werden. Dies ist von Ihnen nicht zu programmieren.
	- Das Skript sucht eine Textdatei auf einem Netzlaufwerk und schreibt die entsprechenden Daten in die Datei
	  Es muss nicht darauf geachtet werden, dass evtl. ein gleichzeitiger Aufruf mehrer Clients möglich wäre.
	- Falls ihr Rechner in der Datei schon existiert, sollen die Daten    aktualisiert werden
        - Am Ende soll die Anzahl der Laptop- und Desktop-PC ermittelt und ausgegeben werden. Dies würde typischerweise in einem zweiten Skript stattfinden; sie können dies aber aus Vereinfachungsgründen auch in ihre vorhandene Lösung einbauen.
      
      
- Authentifizierung am Radius-Server

	Eine Schule plant, seinen Schülern einen definierten Gast-Zugang zum Internet mit eingeschränkten Rechten (http) per WLAN anzubieten. Dazu müssen die Access-Points mit den jeweiligen Login-Daten der Gäste versehen sein. Dies wird an den Access-Points mit Hilfe von Textdateien (CSV) realisiert, die den Usernamen, Passwort sowie das Datum der Erstellung beinhalten. Neue Gäste sollen jederzeit zu den bestehenden Gastzugängen hinzugefügt werden können. Diese müssen dann in die Textdatei aufgenommen werden; die Textdatei wird anschließend auf dem Accesspoint übeschrieben.
	
	An den Login-Namen wird folgende Anforderung gestellt:
	Erster Buchstabe des Vornamens + Nachname
	
	Das Passwort hat folgenden Aufbau:
	
	gast + 4 stellige Zufallszahl
	
	Das Datum soll im Format dd.mm.yyyy in der Textdatei gespeichert werden 
	
	
	Zur ersten Befüllung der Textdatei hat der Systemadministrator eine Textdatei mit dem Vornamen und Nachnamen der Schüler aus dem Acitve Directory extrahiert.
	
	Aufgabe:
	
	Erstellen Sie die Textdatei für die Authentifizierung am Access-Point.
	
