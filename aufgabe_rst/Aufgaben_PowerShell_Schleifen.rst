Schleifen
=========

Lösungen sind hier :ref:`loesung_powershell_schleifen`:


- **ZahlenRaten**

	Erstellen sie ein Skript, welches folgende Aufgabe erfüllt: Es muss eine Meldung anzeigen, in der der Benutzer aufgefordert wird, eine Zahl zwischen 1 und 50 einzugebeenn. Das Skript muss die vom Benutzer eingegebene Zahl mit einer zufällig generierten Zahl vergleichen. Wenn die Zahlen nicht übereinstimmen, muss das Skript eine Meldung anzeigen, in der angegeben wird, ob die geratene Zahl zu hoch oder zu niedrig war, und der Benutzer aufgefordert wird, noch einmal zu raten.

	Wenn der Benutzer richtig rät, muss das Skript die Zufallszahl sowie die Anzahl der Rateversuche anzeigen. An diesem Punkt ist das Spiel beendet, das Skript muss also auch beendet werden. 


- **Dateien kopieren**

	Diese Skripts sollen folgende Aufgaben ausführen:

	- Durchsuchen des Ordners "C:\Scripts" und dessen Unterordnern.
	- Durchsuchen jedes Ordners nach sämtlichen Textdateien (Dateien mit der Erweiterung .txt) und Prüfen des Erstellungsdatums jeder Datei.
	- Kopieren/Verschieben jeder .txt-Datei, die mehr als 10 Tage zuvor erstellt wurde, in den Ordner "C:\Old".
	- Ausgeben des Dateinamens (kein vollständiger Pfad, nur Dateiname) jeder kopierten Datei.
	- Ausgeben der Anzahl der kopierten Dateien. 

- **Fonts im System ermitteln**

	Bei dieser Aufgabe möchten wir herausfinden, welche Schriftarten auf einem Computer installiert sind. Wir geben Ihnen einen Hinweis: Die Schriftarten sind in der Registrierung unter HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion gespeichert. Ihr Skript soll jedoch nicht alle Schriftarten auslesen, sondern nur die TrueType-Schriftarten. Wie können Sie eine TrueType-Schriftart von einer anderen Schriftart unterscheiden? Das ist leicht: In der Registrierung sind TrueType-Schriftarten durch den Ausdruck TrueType in Klammern direkt nach dem Schriftartnamen gekennzeichnet. Eine TrueType-Schriftart sieht also wie folgt aus:
	*Bauhaus 93 (TrueType)*
	
	Zur Lösung dieser Aufgabe muss Ihr Skript Folgendes ausgeben:

	- Die Namen aller TrueType-Schriftarten auf dem Computer.
	- Die Anzahl der TrueType-Schriftarten auf dem Computer.
	- Die Gesamtanzahl der Schriftarten auf dem Computer.
	
	Das Ergebnis sollte in etwa so aussehen:
	
	
	.. code-block:: sh

		Lucida Bright (TrueType)
		Lucida Bright Demibold (TrueType)
		Lucida Bright Demibold Italic (TrueType)
		Lucida Bright Italic (TrueType)
		Lucida Calligraphy Italic (TrueType)
		Lucida Fax Regular (TrueType)

		TrueType: 419
		Total: 451

- **Ping** 
	Schreiben Sie eine Programm, welches Ihnen alle per PING erreichbaren Rechner eines Netzes ermittelt. Das Programm soll alle Adressen von 1 bis 255 pingen. Wenn ein PING erfolgreich war, soll dies als Ausgabe angezeigt werden.

	
- **Schleifen optimieren**

	Die Ausgabe des folgenden Powershell-Befehls dauert u.U. sehr lange. Verbessern Sie das Statement im Hinblick auf die Auführungsgeschwindigkeit

	.. code-block:: sh
	
		# Foreach loop lists each element in a collection:
		Foreach ($element in Dir C:\ -recurse) { $element.name }

		
- **Dateien per ftp hochladen**

	Sie sollen alle Dateien eines zu spezifizierenden Ordners auf einen ftp-server hochladen. Übergeben Sie den Ordnernamen auf der Kommandozeile,
	
- **Übersicht über laufende Dienste**

	Verschaffen Sie sich einen Überblick über die laufenden Prozesse. Falls die Prozesse einen bestimmten Namen besitzen, stoppen Sie diese Prozesse bzw. geben das Wort "Gefunden <Dienstname> aus.
	
	Erweitern Sie das Skript, indem Sie die laufenden Dienste in eine Datei schreiben, um sie später wie oben bereits geschehen, auszuwerten. Welche Schwierigkeiten können bei diesem Vorhaben auftreten.
	
- **Schätzen der Festplattenauslastung**

	Wie lange wird der Speicherplatz einer Festplatte ausreichen, wenn ihr Inhalt jeden Monat um ca. 7,5% wächst. Die Platte hat eine Kapazität von 2 TBiT, eine Startbelegung von 100 MBiT

- **Berechnung des Notendurchnschnitts und Notenverteilung eines Testes**

	Ein Test ergab folgende Einzelnoten:
	6,3,4,2,1,2,3,4,1,2,3,2,1,3,4,5,3,5,4,3,2,2,2,1,2,3
	
	Berechnen Sie die Anzahl jeder Note sowie den Durchschnitt

	
