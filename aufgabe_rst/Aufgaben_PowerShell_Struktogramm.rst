Struktogramm
============

:ref:`loesung_struktogramm`:

ZwiPruef: Frühjahr 2008

- Die alten Daten der Bims & Beton AG sollen in eine neue Datenbank übernommen   
  werden. Das neue Datumsformat = JJJ-MM-TT, das alte TT.MM.JJ. (T=Tag, M=Monat, J=Jahr)


  Die vorhandenen Daten reichen von 1940 bis 2010, daher sollen alle Jahreszahlen <= 10 mit einer "20" und alle Jahreszahlen > 10 mit einer "19" ergänzt werden. Dazu wurde nebenstehender Algorithmus entwickelt.


  .. image:: /images/strukto_bims.png


  .. note::

	- Die Funktion ctoi wandelt ein n.Zeichen einer Zeichenkette in eine Zahl um
	- Die Zeichenkette beginnt mit dem Index 0
	
	a) Eine der im Struktogramm mit 1,2,4,6 oder 7 gekennzeichneten Stellen stellt eine Bedingung dar. Welche ?
	
	b) Das Struktogramm enthät an einer der mit 1,3,5,6 oder 8 gekennzeichneten Stellen einen Fehler. 
	
- Viele der von der Softplus GmbH für die Maschinenbau AG entwickelten Programme enthalten kopfgesteuerte Schleifen. Welche Aussage ist richtig:
 
    #. Bei einer kopfgesteuerten Schleife wird die Anzahl der Durchläufe automatisch berechnet
    #. Eine kopfgesteuerte Schleife wird mindestens einmal durchlaufen
    #. Ein kopfgesteuerte Schleife kann nur durch eine Sprungsanweisung verlassen werden
    #. Wurde eine kopfgesteuerte Schleife einmal durchlaufen, wird die Bedingung nicht mehr geprüft
    #. Eine kopfgesteuerte Schleife wird nicht durchlaufen, wenn die Bedingung nicht erfüllt ist
    
    
- Die Softplus GmbH soll eine Software erstellen, mit der die für Projekte geleisteten Arbeitsstunden je Projekt ermittelt werden können. Die Arbeitsstunden für Projekte werden täglich in der Datei "Arbeitsstunden" gespeichert, deren Datensätze nach Projektnummern **aufsteigend sortiert** sind. Es sollen die Projekte aufgelistet erden, für die insgesamt mehr als 20 Arbeitsstunden geleistet wurden. Ein entsprechendes Programmodul wurde bereits nach unten stehendem Struktogramm erstellt. Ein Test zeigt, dass die Schleifenkopfbedingung "PRJNR := HPRJNR und nicht EOF" nicht zu dem geünschten Ergebnis führt.

	Welche der folgenden Bedingungen ist geeignet ?

	#. PRJNR = HPRJNR und nicht EOF
	#. PRJNR = HPRJNR oder nicht EOF
	#. PRJNR = HPRJNR oder EOF
	#. PRJNR <> HPRJNR und nicht EOF
	#. PRJNR <> HPRJNR oder nicht EOF
	#. PRJNR <> HPRJNR oderEOF
	
	
	
.. image:: /images/struktogramm_arbeitsstunden.png

- Die Softplus GmbH soll für das Warenwirtschaftssystem der Maschinenbau AG ein 
  Programm zur Prüfung des Lagerbestands erstellen. Der IST-"Ablauf der Lagerbestandsprüfung wird von einem Mitarbeiter der Maschinenbau AG wie folgt geschildert:
  
  "Ein Programm gleict wöchentlich die Bestandsdaten mit den Buchungsdaten ab und protokolliert Abweichungen in einer Fehlerliste. Ich bearbeite diese Fehlerliste und gebe die Korrekturen über die PC-Tastatur in das System ein. Danach ermittelt ein Programm die Lagerteile, bei denen der Meldebestand erreicht wurde, erzeugt Bestellungen im System und druckt Bestellschreiben.
  
  Vervollständigen Sie nebenstehenden Datenflussplan zum IST-Ablauf der Lagerbestandsprüfung, indem Sie das jeweilige Symbol den Positionen a) b) und c) zuordnen. Zur Bedeutung der Symbole informieren Sie sich bei wikipedia.


  .. image:: /images/dfplan_symbole.png

  .. image:: /images/datenflussplan.png
