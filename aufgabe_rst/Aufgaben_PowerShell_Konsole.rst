Konsole
=======

- Sie wollen, dass die Powershell automatisch im Administratormodus startet. Wie gehen Sie vor ?

- Beim Ausführen eines Skriptes erhalten Sie eine Fehlermeldung "Execution policy ....". Welche Bedeutung hat diese Meldung und wie können Sie den Fehler abstellen

- Stellen Sie den Bildschirmhintergrund der Konsole auf Weiß und die Textfarbe auf Schwarz.

- Wie können Sie die letzten Befehle einfach wiederholen ?

- Was versteht man unter Tab-Vervollständigung

- Suchen Sie die Hilfe zu Powershell in Form einer Online-Dokumentation

- Wie lautet die Einstiegs-Website von Microsoft mit Informationen zur Powershell ?

- Erläutern Sie die Befehlzeichen | und >. Worin bestehen die Unterschiede ?

- Wie kann man in der Powershell mehrzeilige Befehle eingeben ?


