Bedingungen
===========

Lösungen sind hier: :ref:`loesung_powershell_bedingungen`:


- Mit welchen Vergleichsoperatoren können Sie in Bedingungen arbeiten ?

	
- Folgende Variablen sind gegeben:

	.. code-block:: sh
	
		$user = 'steinam'
		$pass = 'passwort'

   Formulieren Sie eine Bedingung in Powershell, die gleichzeitig nach einem korrekten Usernamen und Passwort fragt.

- Ein Internetprovider hat folgende Staffelpreise (netto)

		- der 1. bis 3. GByte Traffic kostet jeweils 6ct pro Mbyte
		- der 3. bis 8. GByte Trafiic kostet jeweils 8 ct pro MByte
		- die 9. bis 20 GByte Traffic kostet jeweils 10 ct pro MByte
		- ab 20 GByte kostet der Traffic jeweils 12ct pro MByte

	Gehen Sie davon aus, dass ein GByte mit 1024 MByte gerechnet wird.

		Schreiben sie ein Programm, welches die Traffichöhe in MByte abfragt und daraus die Kosten berechnet.


- Mit Hilfe von WMI können Sie feststellen, welche Rolle ein Computer innerhalb einer Domäne spielt. Es gibt 6 verschiedene Rollen:

	- 0 = Stand alone workstation
	- 1 = Member Workstation
	- 2 = Stand Alone Server
	- 3 = Member Server
	- 4 = Backup Domain Controller
	- 5 = Primary Domain Controller
	
	Falls andere Werte zurückkommen, kann die Rolle nicht eindeutig definiert werden
	
	Erstellen Sie ein Skript, welches die jeweilige Rolle in Textform ausgibt.

- Drucker auf verschiedenen Betriebssystemen ermitteln

	Verschiedene Betriebssysteme nutzen manchmal unterschiedliche WMI Klassen und/oder Eigenschaften, so z.B. Windows XP, Windows 2003 und Windows 2000.
	Sie wollen die auf diesen Betriebssystemen verwendeten Drucker auflisten und benötigen ein universell einsetzbares Skript.
	Verwenden Sie ein if-Statement zum Ermitteln der korrekten OS-Version und verwenden Sie den dafür vorgesehenen Code.

	Benutzen Sie die WMI-Klasse win32_OperatingSystem zum Ermitteln der OS-Version sowie win32_Printer auf XP und 2003 und win32_PrintJob auf Win2000-Systemen
	


- Schreiben Sie eine Anwendung, die nach Eingabe einer ganzen Zahl ausgibt, ob    
  die Zahl gerade (restlos durch 2 teilbar, Modulo!) ist oder nicht.

- Schreiben Sie eine Anwendung, die nach Eingabe zweier Zahlen ausgibt, welche 
  der beiden Zahlen die größere und welche die kleinere der beiden ist.

- Erweitern Sie die Anwendung um das Erkennen der Gleichheit der beiden 
  Eingaben.

- Schreiben Sie eine Anwendung, die ermittelt, ob eine Kreditantrag aufgrund des 
  Alters des Antragstellers in eine besondere Prüfung muss. Ist der Antragsteller nicht volljährig oder schon älter oder gleich 65 Jahre, soll eine Meldung ausgegeben werden. Falls das Alter dazwischen liegt, soll eine entsprechende Meldung ausgegeben werden.
  
  
- Schreiben Sie eine Anwendung, die zu einer Eingabe den Absolutwert ausgibt.

- Schreiben Sie eine Anwendung, die nach Eingabe zweier Zahlen vom Anwender die 
  Summe, die Differenz, das Produkt und den Quotienten anfragt. Anschließend soll ausgegeben werden, welche Antwort falsch oder richtig waren (im Fehlerfall mit Lösung) und wie viel Prozent der Antworten richtig waren. Vergessen Sie nicht die Plausibilitätsprüfung der Eingaben (Division!).







