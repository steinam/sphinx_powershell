Variable_und_Datentyp
=====================

- Was ist das Ergebnis des folgenden Skriptes

  .. code-block:: sh

	   [int]$a = -7
	   [int]$b = 3
	   Write-Host($a/$b)
	   Write-Host([int]($a/$b))

- Welche Zeichen sind für das Erstellen eines Variablennamens möglich. Recherchieren Sie falls notwendig auch im Internet
   
		
- Welchen Unterschied macht es, ob sie als Kennzeichen für einen String das Zeichen " oder das Zeichen ' einsetzen.



- Wie kann man erfahren, welcher Datentyp eine Variable bzw. ein Wert hat
   
- Sie möchten einen String als Datum weiterverwenden, falls er ein reguläres Datum ist.

- Welche Vorteile hat das Casten eines Strings in ein DateTime-Objekt

- Welche Wertebereiche kann eine als int16 deklarierte Variable aufnehmen.

- Wie löscht man eine Variable innerhalb einer Powershell-Sitzung

- Welche Variablen sind beim Start von Powershell bereits vorhanden ? Wie kann man sie sichtbar machen

- Folgender Quellcode ist vorhanden

  .. code-block:: sh
   	
   	$a = "Hello"
   	$b = 123
   	
  - Bestimmen Sie die Länge des Wertes der Variable $a
  - An welcher Stelle im String kommt der Buchstabe "e" vor
  - Tauschen Sie den Buchstaben 'e' durch den Buchstaben 'a'
   

Blutalkohol
-----------

Mit dieser Aufgabe wollen wir Folgendes trainieren:

- Verwenden von Variablen und Datentypen
- Verwendung einfacher Grundrechenoperatoren
- Anpassung der Funktionsweise von Operatoren an Datentypen


**Beschreibung**

Mit der Widmark-Formel können wir die Blutalkoholkonzentration abschätzen. Sie wird mit dieser Formel berechnet:


::

      		A
	c =   	--
     		m*r

	mit

		A = V ∗ ϵ ∗ ρ

wobei

- c: Alkoholkonzentration im Blut in [g/kg]
- A: Aufgenommene Masse des Alkohols in [g]
- r : Verteilungsfaktor im Körper – Männer: r ≈ 0,7 – Frauen: r ≈ 0,6 – Kinder: r ≈ 0,8
- m: Masse der Person in [kg]
- V : Volumen des Getränks in [ml]
- ϵ: Alkoholvolumenanteil in [%] (z.B. Bier ≈ 0,05)
- ρ: Dichte von Alkohol [g/ml] → ρ ≈ 0,8 g/ml

**Aufgabenstellung**

Schreibe ein Programm, das die Blutalkoholkonzentration mit der Widmark-Formel berechnet. 
Die Eingabegrößen sollen dabei fleexibel einstellbar sein.

**Testfälle**

Ein 80 kg (m = 80) schwerer Mann (r ≈ 0,7) trinkt eine 0,5-l-Flasche Bier (A = 500ml ∗0,05∗ 0,8g/ml = 20g). 
Daraus ergibt sich nach der Widmark-Formel eine Blutalkoholkonzentration von ungefähr 0,35714 g/kg (≈ 0,36 Promille).

Überlege dir weitere Testfälle, die möglichst viele der möglichen Kombinationen aus Alkoholart, Geschlecht, Köpergewicht und Volumen abdecken. Hier noch eine Anregung von
uns, wie das aussehen kann:



Baumstammvolumen
----------------

Nachdem ein Baum gefällt und der Stamm aufgearbeitet wurde, möchte man in der Holzwirtschaft wissen, wie viel Holz der Stamm hat (Volumen in Festmeter). Dazu messen wir
die Länge (L in Meter) und den Mittendurchmesser (D in Zentimeter). Nach folgender Formel können wir anschließend das Volumen berechnen:

::

	              Pi      2
	Volumen = 	(----  * D ) * L /10000
                  4

**Aufgabe**

Schreibe ein Programm, das nach der oben stehenden Formel das Volumen eines Baumstamms berechnet.

**Testfälle**

- 10 Meter lang und 30 cm Durchmesser: 0.70685834
- 15 Meter lang und 32 cm Durchmesser: 1.2063715

**Algorithmische Tipps**

Wenn du stockst und nicht weiter weißt, dann versuch mal Folgendes:
- Überprüfe, ob die Kreiszahl PI in Powershell als Variable zur Verfügung steht.
- Definiere zuerst die Variablen für Länge und Durchmesser und berechne anschließend
das Volumen.
