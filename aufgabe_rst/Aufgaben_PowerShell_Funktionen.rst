


Funktionen
==========

Lösungen sind hier: :ref:`loesung_powershell_funktionen`:


- Schreiben Sie eine Funktion, die als Parameter einen Pfad erwartet und anschließend den Inhalt des Pfades ausgibt. Lösung ist hier :ref:`loesung_dir`.

- Erweitern Sie die obige Funktion um eine rekursive Variante. Sollte der Pfad Unterordner besitzen, sollen auch diese ausgegeben werden. :ref:`loesung_dir_rekursiv`

- Erweitern Sie die obige Funktion: Es soll jetzt möglich sein, nach bestimmten Dateikennungen die Ausgabe zu gestalten. :ref:`loesung_dir_rekursiv_extension`

- Wie überprüfen Sie, ob ein Array einen bestimmten Wert enthält

- Wie überprüfen Sie, ob zwei Arrays gleich sind	
	Folgende Funktion überprüft zwei Arrays auf Gleichheit
	
	
	
	.. code-block:: sh
	
		function AreArraysEqual($a1, $a2) {
			if ($a1 -isnot [array] -or $a2 -isnot [array]) {
			  throw "Both inputs must be an array"
			}
			if ($a1.Rank -ne $a2.Rank) {
			  return $false 
			}
			if ([System.Object]::ReferenceEquals($a1, $a2)) {
			  return $true
			}
			for ($r = 0; $r -lt $a1.Rank; $r++) {
			  if ($a1.GetLength($r) -ne $a2.GetLength($r)) {
					return $false
			  }
			}
		
			$enum1 = $a1.GetEnumerator()
			$enum2 = $a2.GetEnumerator()   
		
			while ($enum1.MoveNext() -and $enum2.MoveNext()) {
			  if ($enum1.Current -ne $enum2.Current) {
					return $false
			  }
			}
			return $true
		} 

- Folgendes Powershell-Skript berechnet den Wochentag mit Hilfe eines eigenen Algorithmus. Verbessern Sie die Lesbarkeit des Skriptes, indem Sie die verschiedenen Teile des Quellcode mit Hilfe von Funktionen kapseln.

	.. code-block:: sh
	
		#Berechnung des Wochentags

		[int]$t = Read-Host -prompt "Geben Sie einen Tag ein" 		
		[int]$m = Read-Host -prompt "Geben Sie einen Monat ein"
					
		$MerkeMonat = $m;
		
		[int]$j = Read-Host -prompt "Geben Sie das Jahr ein"
		
		if($m -le 2)
		{
			$m += 10;
			$j -= 1;
		}
		else
		{
			$m -= 2;
		}
		
		[int]$c = $j/100;
		[int]$y = $j%100;
		
		[int]$h = (((26 * $m -2)/10) + $t + $y + $y/4 + $c/4 -2 * $c)%7
		
		if($h -lt 0)
		{
			$h +=7;
		}
		
		[string]$Tag = "";
		switch($h)
		{
			0 { $Tag = "Sonntag"; break}
							
			1 { $Tag = "Montag"; break}
		
			2 { $Tag = "Dienstag"; break}
		
			3 { $Tag = "Mittwoch"; break}
		
			4 { $Tag = "Donnerstag";break}
		
			5 { $Tag = "Freitag"; break}
		
			6 { $Tag = "Samstag"; break}
		}
		
		Write-Host "Der " + $t +"." + $MerkeMonat + "." + $j + " ist ein " + $Tag
		
		
- Schreiben Sie eine Funktion, die ihnen die Größe des vorhandenen physikalischen und virtuellen RAMs ausgibt. Benutzen Sie zum Ermitteln der Größe die WMI-Klasse WIN32_OperatingSystem und deren Eigenschaften **FreePhysicalMemory** sowie **FreeVirtualMemory**. Als kleine "Zugabe" möchten Sie die Angabe der Größe in verschiedenen Formaten (Byte, KByte, MByte) ermöglichen. 


- Schreiben Sie eine Funktion zum Umbenennen eines Computernamens. Der Name soll entweder per Eingabeaufforderung übergeben werden können oder aus einer Textdatei gelesen werden, falls keine Eingabe vorgenommen wird. Kommentieren Sie die Funktion über die Powershell-typische Hilfesyntax. Prüfen Sie innerhalb der Funktion, ob Sie überhaupt die administrativen Rechte für die Umbenennung haben. Zum Abschluss des Vorgangs müssen Sie einen Reboot vornehmen.

- Powershell kann mit Hilfe des CommandLets **Get-Eventlog** die Ereignisanzeige von lokalen und remote-Computern anzeigen und filtern. Schreiben Sie eine Funktion *getLogInfos*, die Ihnen folgende Auswahlmöglichkeiten zulässt:
	- Wahl des Computers
	- Wahl des jeweiligen EventLogs
	- Wahl des jeweiligen Eintrags, z.B. Error, Warnig, Information
	
	Informieren Sie sich mit Hilfe von get-help über die weiteren Möglichkeiten von Get-Eventlog
	

- Verändern von Attributen einer Datei

  Sie haben als Administrator einige Dateien, auf die sie von Zeit zu Zeit zugreifen müssen. Um die Dateien vor versehentlichen Änderungen zu schützen, versehen Sie diese mit ReadOnly - Flag. Leider ist aber jetzt ein Bearbeiten dieser Dateien mühsam, weil Sie nun immer erst dieses Flag ändern müssen.
  
  Schreiben Sie ein parametrisiertes Skript, welches alle Dateien eines Ordners ReadOnly bzw. Nicht-ReadOnly setzen kann. Schreiben Sie zusätzlich eine Funktion, die Ihnen alle Dateien mit ReadOnly-Flag auflistet.
  Eine Hilfe-Funktion soll Sie bei einer falschen Eingabe unterstützen.
  
- Ein Außendienstmitarbeiter erhält ein neues Firmen-Laptop. Auf seinem alten Laptop sind viele WLAN-Profile gespeichert und Sie suchen einen Weg, um diese Profile auf den neuen Rechner ohne manuelles Neuanlegen zu übernehmen. Ein Kollege erzählt Ihnen, dass Sie mit Hilfe des Befehls netsh Profile sowie exportieren als auch importieren können.
  - Informieren Sie sich über die Möglichkeiten des Befehls netsh
  - Schreiben Sie ein Skript, welches sowohl den Export als auch den Import der WLAN-Profile ermöglicht.

- Kommentieren Sie die Elemente des folgenden Quellcodes
	
	.. code-block:: sh
	
		


- Aufruf von Remote-Desktop-Sitzungen

	Erstellen Sie ein Skript, welches eine rdp-Verbindung unter Berücksichtigung verschiedener Optionen des mstsc-Befehls ermöglicht.
	
- Setzen Sie mit Hilfe der PS die IP-Adresse eines Rechners. Geben Sie auch die Daten für Gateway, SN-Mask, NIC, DNS-Server

- Mit Hilfe des Befehls net view \\UNC-Name können Sie die Freigaben eines Rechners ermitteln. Schreiben Sie eine Funktion, die als Parameter einen UNC-Namen und einen speziellen Freigabetyp erwartet. Die Funktion soll dann die gefundenen Freigaben zurückgeben. 


- Programmieren eines HangMan-Spiels

    Okay, it is time to turn your attention back to the chapter’s main game project, the PowerShell Hangman game. The PowerShell Hangman game is a word guessing game in which the player is challenged to guess a randomly selected secret word, a letter at a time. To win, the player must guess each letter in the word in 12 guesses or fewer.

    The overall construction of the PowerShell Hangman game will be completed in 11 steps, as outlined here:

    #. Create a new script file using the PowerShell script template.
    #. Define and initialize game variables in the Initialization Section.
    #. Define functions located in the Functions and Filters Section.
    #. Prompt the player for permission to play the game.
    #. Create a loop to control overall gameplay.
	#. Randomly select the game’s secret word.
	#. Create a loop to control the collection and analysis of player input.
	#. Collect and validate player guesses.
	#. Display the result of each guess.
	#. Determine when the game is over.
	#. Challenge the player to play again.

	
	


