﻿https://kevinmarquette.github.io/2016-11-06-powershell-hashtable-everything-you-wanted-to-know-about/#using-the-brackets-for-access


$result = Import-Csv C:\schule\docbook\sphinx_powershell\aufgabe_rst\uebungen_zu_hash\umsatz_bayern.csv -Delimiter ","

$umsatzliste = @{}

foreach($element in $result)
{
    #$umsatzliste[$element.]

    $umsatzliste[$element.Monat] = [double]$element.Umsatz
}


$umsatzliste.values | Measure-Object -Minimum

$values = $umsatzliste.Keys | Sort-Object 

$values



$person = @{
    name = 'Kevin'
    age  = 36
}


$person.Clear()

$person

$person.location = @{}

$person.location.city = "austin"

$person.location.city