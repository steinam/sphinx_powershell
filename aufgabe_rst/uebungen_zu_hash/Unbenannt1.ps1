﻿# What can you do with hashtables

#create one
$computer1 = @{IP= "10.161.8.1"; MAC = '2A:65:65:43:12';BuyDate = '2005-05-12' }


#create some more
$computer2 = @{IP= "10.161.8.2"; MAC = '2A:43:87:43:12';BuyDate = '2002-05-12' }
$computer3 = @{IP= "192.168.3.1"; MAC = '2B:43:87:43:12';BuyDate = '2009-05-12'}


#two hashes, how can you store them ??
$myPC_Hash = @{}
$myPC_Array = @()


$myPC_Hash.Add($computer1.IP, $computer1)
$myPC_Hash.Add($computer2.IP, $computer1)
$myPC_Hash.Add($computer3.IP, $computer1)

$myPC_Array += $computer1
$myPC_Array += $computer2
$myPC_Array += $computer3


$myPC_Hash
$myPC_Array

#Show me the keys of $myPC_Hash
$myPC_Hash.Keys

#Show me the the Values of  $myPC_Hash
$myPC_Hash.Values

#exist 10.161.8.2 ?
$myPC_Hash.ContainsKey('10.161.8.2')
$myPC_Array.Contains('10.161.8.2') #wtf i need a loop and iterating through 


#whats his values ?

$myPC_Hash['10.161.8.2']
$myPC_Array['10.161.8.2'] #wtf i need a loop  and iterating through 


#lets sort the output by BuyDate

$myPC_Hash | Sort-Object {$_.BuyDate} | ForEach-Object {Write-Host  $_.BuyDate} #wtf
#try https://blogs.technet.microsoft.com/heyscriptingguy/2014/09/28/weekend-scripter-sorting-powershell-hash-tables/

$myPC_Array| Sort-Object $($_).IP | % IP  #slow but runs

$myPC_Array | Sort-Object {$_.BuyDate} | ForEach-Object {Write-Host `t $_.IP `t $_.MAC `t $_.BuyDate}


#whats the MAC of 10.161.8.2 ?

$myPC_Hash.'10.161.8.2'.MAC

$myPC_Array ....  #wtf


#lets export to ....


#csv
$myPC_Hash | Export-Csv -LiteralPath "C:\temp\exportHash.csv"   #wtf
$myPC_Hash | Export-Csv "C:\temp\exportHash2.csv"               #wtf
$myPC_Array | Export-Csv -LiteralPath "C:\temp\exportArray.csv" #wtf


#json
$result = $myPC_Hash | ConvertTo-Json

$myPC_Array | ConvertTo-Json  #not exact the same



#import to json

$nwhash = Get-Content  C:\temp\hash_json.json |  ConvertFrom-Json
$nwarray = Get-Content  C:\temp\hash_json.json |  ConvertFrom-Json


$nwhash.'10.161.8.2'.MAC





