Aufgaben_zu_Arrays
===================

Lösungen sind hier: :ref:`loesung_powershell_arrays`:


- Schreiben Sie eine Funktion, die als Parameter einen Dateipfad erwartet und anschließend den Inhalt des Pfades ausgibt.

- Wie überprüfen Sie, ob ein Array einen bestimmten Wert enthält

- Wie überprüfen Sie, ob zwei Arrays gleich sind	

- Ein String ist eigentlich ein Array aus Elementen des Datentyps CHAR.

	.. code-block:: sh
	
		$test = "Hello"
		$test[0].Gettype()
		
		IsPublic IsSerial Name                                     BaseType            
		-------- -------- ----                                     --------            
		True     True     Char                                     System.ValueType    
		
	Die Ansprechen eines einzelnen Elementes ist auch mit $test[1] problemlos möglich. Wie können Sie aber ein einzelnes Element eines bestehenden Strings mit Hilfe der Array-Indiziierung ändern, also etwas so
	
	.. code-block:: sh
		
		$integerarray = 1,2,3
		$integerarray[2] = 345	# geht
	
		$test = "Hello"
		$test[0] = "F"  		#Fehlermeldung
		
- Überprüfen sie, ob ein bestimmter Wert in einem Array enthalten ist, z.B.
	
	.. code-block:: sh
	
		$arrColors = "blue", "red", "green", "yellow", "white", "pink", "orange", "turquoise"
		
	Prüfen Sie, ob der Wert "green" enthalten ist
	
	
- Haben wir Elemente, die mit "bl" beginnen

	.. code-block:: sh
	
		$arrColors = "blue", "red", "green", "yellow", "white", "pink", "orange", "turquoise", "black"

- Sortieren sie den obigen Array alphabetisch

	.. code-block:: sh
	
		$arrColors = "blue", "red", "green", "yellow", "white", "pink", "orange", "turquoise", "black"

		
- Vergleichen Sie die Geschwindigkeit beim Erstellen und Suchen der folgenden Datenstrukturen: Array vs. Hashtable. Welches Ergebnis stellen Sie fest ?
		

- Sie besitzen eine Textdatei mit Familienname und Loginname der Firmenmitarbeiter. Sie umfasst 2000 Einträge und soll dazu hergenommen werden, um "vergessene" Loginnamen zu suchen. Überlegen Sie sich, wie Sie diese Textdatei innerhalb von Powershell-Skripten nutzen können.


- Folgende Noten einer Schulaufgabe sind vorhanden:

  2,3,3,2,4,5,4,3,2,1,6,4,3,2,5,4,1,2,3,6,5,4,3,2,1,4,3,2,1,5,4,4,4,3,2,3,4
  
  Lösen Sie mit Hilfe eines Arrays die folgenden Aufgaben:

  - Ermitteln Sie den Notendurchschnitt
  - Ermitteln sie die Häufigkeit jeder Note (absolut/prozentual)
  - Wie viele Noten liegen unter/über dem Schnitt
  - Erstellen Sie eine sortierte Ausgabe in Form eines Write-Host-Statements

