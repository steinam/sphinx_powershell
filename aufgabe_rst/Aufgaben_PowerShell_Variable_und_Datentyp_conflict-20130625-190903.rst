Variable_und_Datentyp
=====================

- Was ist das Ergebnis des folgenden Skriptes

  .. code-block:: sh

	   [int]$a = -7
	   [int]$b = 3
	   Write-Host($a/$b)
	   Write-Host([int]($a/$b))

- Welche Zeichen sind für das Erstellen eines Variablennamens möglich. Recherchieren Sie falls notwendig auch im Internet
   
		
- Welchen Unterschied macht es, ob sie als Kennzeichen für einen String das Zeichen " oder das Zeichen ' einsetzen.



- Wie kann man erfahren, welcher Datentyp eine Variable bzw. ein Wert hat
   
- Sie möchten einen String als Datum weiterverwenden, falls er ein reguläres Datum ist.

- Welche Vorteile hat das Casten eines Strings in ein DateTime-Objekt

- Welche Wertebereiche kann eine als int16 deklarierte Variable aufnehmen.

- Wie löscht man eine Variable innerhalb einer Powershell-Sitzung

- Welche Variablen sind beim Start von Powershell bereits vorhanden ? Wie kann man sie sichtbar machen

- Folgender Quellcode ist vorhanden

  .. code-block:: sh
   	
   	$a = "Hello"
   	$b = 123
   	
  - Bestimmen Sie die Länge des Wertes der Variable $a
  - An welcher Stelle im String kommt der Buchstabe "e" vor
  - Tauschen Sie den Buchstaben 'e' durch den Buchstaben 'a'
   
	
