Interaktivmodus 
===============

Lösungen sind hier: :ref:`loesung_interaktiv`:


- Welche Aliase gibt es fÃ¼r das Get-ChildItem

- Finden Sie mit Hilfe von Get-Help heraus, wofÃ¼r das Cmdlet Stop-Process eingesetzt wird. Nutzen Sie die Hilfeinformationen, um mit Stop-Process alle Instanzen des Notepad-Editors zu schlieÃŸen. Wie lautet der korrekte Befehl.

- Versuchen Sie wie im vergangenen Beispiel mit Stop-Process einen Prozess zu beenden, der gar nicht vorhanden ist, erhalten Sie eine Fehlermeldung. Wie kann man diese Fehlermeldung unterdrÃ¼cken ?

- Nutzen Sie das Wissen Ã¼ber die Parameter von Write-Host dazu, um einen weiÃŸen Text auf rotem Grund auszugeben.

- Wie kann man sich die Parameter eines Cmdlets per get-help anzeigen lassen.


- Windows hat verschiedene EventLogs, welche man mit Hilfe des Cmdlets **Get-Eventlog** prinzipiell abfragen kann. Aber wie erhÃ¤lt man die Namen der Eventlogs, die man einsehen will. 

  Versuchen Sie es mit Hilfe von **get-help get-eventlog** herauszufinden.
  
  Listen Sie sÃ¤mtliche EintrÃ¤ge des **System**-Eventlogs auf.


.. image:: /images/eventverwaltung.jpg



- Listen Sie nur die Ereignisse aus dem System-Ereignisprotokoll auf, die Fehler anzeigen. Schauen Sie sich dazu die Spalten an, die Get-EventLog liefert. Gibt es einen entsprechenden Parameter.

- Listen Sie alle Error-EintrÃ¤ge des System-Ereignisprotokolls der letzten 24 Stunden auf Welche Parameter kÃ¶nnten den Zeitraum einschrÃ¤nken.

- Listen Sie alle Error-EintrÃ¤ge der letzten 24 Stunden auf, ohne ein konkretes Datum eingeben zu mÃ¼ssen.


- Listen Sie alle Error-EintrÃ¤ge eines anderen Computers der letzten 24 Stunden auf, ohne ein konkretes Datum eingeben zu mÃ¼ssen. 




