##################################################################################
#
#
#  Script name: Add-STHomeFolder.ps1
#  Author:      goude@powershell.nu
#  Homepage:    www.powershell.nu
#
#
##################################################################################

param([string]$Domain, [string]$User, [string]$Share, [string]$HomeDrive, [switch]$help)

function GetHelp() {

$HelpText = @"

DESCRIPTION:

NAME: Add-HomeFolder.ps1
Adds a HomeFolder For a StarTrek User.
The Script also uses the Set-FolderPermission.ps1
found on www.powershell.nu

PARAMETERS: 

-Domain          Name of the Domain (Required)
-User            Users sAMAccountName (Required)
-Share           Name of Share (Required)
-HomeDrive       Users HomeDrive (Required)
-help            Prints the HelpFile (Optional)

SYNTAX:

Add-STHomeFolder.ps1 -Domain powershell.nu -User Worf01 -Share Share -HomeDrive H

Adds a HomeFolder for User with sAMAccountName User01
within the Domain powershell.nu. 

Add-STHomeFolder.ps1 -help

Displays the help topic for the script

"@
$HelpText
}

function Add-HomeFolder ([string]$Domain, [string]$User, [string]$Share, [string]$HomeDrive) {

	# Build up HomeDrive String

	$HomeDirectory = "\\" + $env:COMPUTERNAME + "\" + $Share + "\" + $User

	# Check if User Already Has a HomeFolder

	$GetUser = ./Get-AD.ps1 -Domain $Domain -User $User -Filter sAMAccountName -ToObject

	if ($GetUser.homeDirectory -match $HomeDirecory -AND $GetUser.homeDrive -match $HomeDrive) {

		Write-Host "User: $User HomeDrive Already Set" -ForeGroundColor Yellow

	} else {

		# Create a HomeFolder on Server and give User Full Permissions

		$DomainUser = $Domain + "\" + $User

		./Set-FolderPermission.ps1 -Path $HomeDirectory -Access $DomainUser -Permission FullControl

		# Setting HomeDrive For User

		$GetUser.Put("homeDirectory",$HomeDirectory)
		$GetUser.Put("homeDrive",$HomeDrive)
		$GetUser.SetInfo()

		Write-Host "Added HomeDrive For User: $User" -ForeGroundColor Green
	}
}

if ($help) { GetHelp }

if ($Domain -AND $User -AND $Share -AND $HomeDrive) { Add-HomeFolder -Domain $Domain -User $User -Share $Share -HomeDrive $HomeDrive }