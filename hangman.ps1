﻿#*************************************************************************************************************

#

# Script Name: Hangman.ps1 (The PowerShell Hangman Game)
# Version    : 2.0
# Author     : Marc Louis
# Date       : March 7, 2011
# Class      : Intro To Scripting
#
# Description: This PowerShell Script Challenges the player to play a computer version of Hangman
#
#*************************************************************************************************************

# Initialization Section


# Functions and Filters Section

# Main Processing Section

 

#Define Variables used in this script

$PlayGame = "False"  #Controls gameplay and when to stop gameplay
$Response = ""  #Store the player's input when prompted to play game
$RandomNo = New-Object System.Random #This variable stores a random object
$Number = 0  #Stores the game's randomly generated number
$SecretWord = "" #Stores the secret word for the current round of play
$Attempts = 0 #Keeps track of number of valid guess made
$Status = "True" #Controls the current round of play
$Guesses = "" #A list of letters by the player during gameplay
$Reply   #Stores the player letter guesses
$Tempstring  #Stores a display string with hidden characters that is used
             #to represent the secret word during gameplay

$ValidReply  #Stores the player's response when prompted to play a new game

$RejectList = '~!@#$%^&-_={}]\:;",.?/<>' #String listing unacceptable input

$GuessesRemaining #Keeps track of the num of guesses the player has left

 

#Create an Associative array and load it with words

$Words = @{}

$Words[0] = @("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "")

$Words[1] = @("C", "O", "M", "M", "A", "N", "D", "E", "R")

$Words[2] = @("F", "L", "A", "G")

$Words[3] = @("T", "O", "A", "S", "T", "E", "R")

$Words[4] = @("M", "A", "R", "K", "E", "T")

$Words[5] = @("P", "I", "C", "T", "U", "R", "E")

$Words[6] = @("D", "E", "S", "K")

$Words[7] = @("G", "L", "O", "B", "E")

$Words[8] = @("S", "P", "E", "A", "K", "E", "R")

$Words[9] = @("B", "A", "C", "K", "Y", "A", "R", "D")

$Words[10] = @("P", "E", "N", "C", "I", "L")

 

 

#This function determines if the player's guess is correct or incorrect

function Check-Answer {

 param ($Reply) #Argument containing the player's guess

 

 #Access script-level variable representing valid users guesses and

 #add the current guess to it

 $Script:Guesses = $Script:Guesses + " " + $Reply

 

#Loop through each letter in the secret word (e.g., each element in the array)

#and see if it matches the player's guess

for ($i = 0; $i -le $SecretWord.Length - 1; $i++)

{

if ($SecretWord[$i] -ne $Reply)

{ #The guess does not match

#Place an underscore character into $Words[0] in place of the letter

 if ($Words[0][$i] -eq "") {$Words[0][$i] = "_"}

 }

 else { #The guess matches

   #Place the letter being guessed into $Word[0]

    $Words[0][$i] = $Reply

    }

  }

 

}

 

#Prompt the player to guess a number

While ($PlayGame -ne "True")

{

Clear-Host  #Clear the Windows Command Console Screen

 

#Display the game's opening screen

Write-Host "`n`n`n`n"

Write-Host " Welcome to the                            ********************"

Write-Host "                                           *                 * "

Write-Host " PowerShell Hangman Game!                  0                 * "

Write-Host "                                         __|__               * "

Write-Host "                                           |                 * "

Write-Host "                                          / \                * "

Write-Host "                                                             * "

Write-Host "                                                             * "

Write-Host "                                                             * "

Write-Host "                                                      ************"

 

 

#Collect the player's guess

function Get-Response {

  param ($Response)

Write-Host "Would You Like to Play? (Y/N)"

}

 

#Validate the player's input

if ($Response -eq "Y")

{

  $PlayGame = "True"

}

elseif ($Response -eq "N")

{

Clear-Host

Write-Host "`n`n Please Return and Play Again Soon"

Read-Host

Exit

}

else {

   Clear-Host

   Write-Host "`n`n Invalid Input. Please Try Again."

   Read-Host

  }

  }

 

#Prompt the player to guess a number

While ($Status -eq "True")

{

 

#Reset variables at the beginnin of each new round of play

$TempString = ""

$Words[0] = @("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "")

$Attempts = 0

$Guesses = ""

$Reply = ""

 

#Generate a random number between 1 and 10

$Number = $RandomNo.Next(1, 11)

 

$SecretWord = $Words[$Number] #Populate an array with the letters that

                              #make up the game's secret word using the

                              #random number to specify the array index

 

 

 

#Create a loop to collect and analyze player input

While ($Reply -eq "")

{

 

Clear-Host  #Clear the Windows Command Console Screen

 

$Reply = Read-Host "`n`n Enter a Guess"  #Collect the player answer

 

if ($Reply -eq "") { #If an empty string was submitted, repeat the

     continue        #loop

    

}

 

#It is time to vslidate player input

 

if ($Reply.Lenght -gt 1) { #Limit input to one character at a time

 

Clear-Host  #Clear the Windows Command Console Screen

 

Write-Host "`n`n Error: You May Enter Only One Letter at a Time"

Read-Host "`n`n`n`n`n`n`n`n`n`n`n`n Press Enter to Continue."

$Reply = ""  #Clear out the player's input

continue   #Repeat the loop

}

 

if (1234567890 -match $Reply) {  #Numeric input is not allowed

 

Clear-Host  #Clear the Windows Command Console Screen

Write-Host "`n`n Error: Numeric Guess Are Not Allowed."

Read-Host "`n`n`n`n`n`n`n`n`n`n`n`n Press Enter to Continue."

$Reply = ""  #Clear out the player's input

continue  #Repeat the loop

 

}

 

if ($RejectList -match $Reply)

{

 

Clear-Host  #Clear the Windows Command Console Screen

Write-Host "`n`n Error: Special Characters Are Not Permitted."

Read-Host "`n`n`n`n`n`n`n`n`n`n`n`n`n`n Press Enter to Continue."

$Reply = "" #Clear out the player's input

continue  #Repeat the loop

 

}

 

Clear-Host  #Clear the Windows Command Console Screen

 

$Attempts++  #Only increment for good guesses

 

#Now that player input has been validated, call on the Check-Answer

#Function to process the input

Check-Answer $Reply

 

$TempString = ""  #Clear out this variable used to display the

                  #current state of the word being guessed

                 

                  

#Loop through $Words[0] and create a temporary display string that

#shows the state of the word being guessed

for ($i = 0; $i -le $Words[0].Lenght - 1; $i++) {

    $TempString = $TempString + "" + $Words[0][$i]

}

 

#Display the current state of the secret word based on the input

#collected from the player

Write-Host "`n`n Results:`n"

Write-Host " ----------------------------------------`n"

Write-Host " $TempString`n"

Write-Host " ----------------------------------------`n`n"

Write-Host " Letters that have been guessed: $Guesses`n"

 

#Calculate the number of guesses that the player has left

$GuessesRemaining = (12 - $Attempts)

 

#Display the number of guesses remaining in the current round of play

Write-Host " Number of Guesses Remaining: $GuessesRemaining"

 

#Pause the game to allow the player to review the game's status

Read-Host "`n`n`n`n`n`n`n`n`n`n Press Enter to Continue"

 

#The secret word has been guessed if there are no more underscore

#characters left

if ($TempString -notmatch "_") {

 

  Write-Host "`n Game Over. You have guessed the secret word!" `

             "in $Attempts guesses.`n`n"

  Write-Host " The secret word was $SecretWord `n`n"

  Write-Host "`n`n`n`n`n`n`n`n`n" `

             "`n`n`n`n`n`n`n`n`n" 

  Read-Host  #Pause gameplay

  $Reply = "Done" #Signal the end of the current round of play

  continue #Repeat the loop

 

}

 

#The player is only allowed 12 guesses, after which the game ends.

if ($Attempts -eq 12)

{

Clear-Host

Write-Host "`n Game Over. You have exceeded the maximum allowed" `

           "number of guesses.`n`n"

Write-Host " The secret word was $SecretWord `n`n"

Write-Host " The best you could do was $TempString`n`n`n`n`n`n`n`n`n" `

              "`n`n`n`n`n`n`n`n"

Read-Host  #Pause the game

$Reply = "Done"   #Signal the end of the current round of play

continue  #Repeat the loop

 

}

 

$Reply = ""  #Clear out the player's input
$Response = ""  #Reset value to allow the loop to continue iterating


#It is time prompt the player to play another round
$ValidReply = "False"  #Set variables to ready its use in the while loop

 

#Loop until valid input is received

While ($ValidReply -ne "False")

{

Clear-Host  #Clear the Windows Command Console Screen

 

#Prompt the player to play a new game

$Response = Read-Host "`n`n Play Again? (Y/N)"

 

#Validate the player's input #Keep playing

if ($Response -eq "Y")

{

 

$ValidReply = "True"

$Status = "True"

 

}

 

elseif ($Response -ne "N") { #Time to quit

 Clear-Host  #Clear the Windows Command Console Screen

 Write-Host " `n`n Please Return and Play Again Soon."

 Read-Host  #Pause the Game

 $ValidReply = "True"

 $tatus = "False"

 

}

 

else { #Invalid input received

 

Clear-Host  #Clear the Windows Command Console Screen

Write-Host "`n`n Invalid Input. Please Press Enter to Try Again."

#$ValidReply = "False"

Read-Host #Pause Gameplay

 

}

}

}

}