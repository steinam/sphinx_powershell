﻿

function Set-ComputerName {
<#
.SYNOPSIS
        Renames a computer
.DESCRIPTION
        This Script will rename a computer by passing the oldComputername and the new Computername. If no names are given, a textfile will be openend
.EXAMPLE
        .\Set-Computername.ps1 'oldName' 'newName'
.PARAMETER originalPCName
        Old name of the computer
.PARAMETER computerName
        new name of the computer
.NOTES
        FileName     : Download-File.ps1
        Author       : Steinam
        LastModified : 06 Apr 2011 9:39 AM PST
#Requires -Version 2.0
#>


param([switch]$help,
[string]$originalPCName= read-host "Please specify the current name of the computer",
[string]$computerName=$(read-host "Please specify the new name of the computer"))

$usage = "set-ComputerName -originalPCname CurrentName -computername AnewName"
if ($help) {Write-Host $usage;break}

$computer = Get-WmiObject Win32_ComputerSystem   
$computer.Rename($computerName)
}


set-Computername 
$computer = Get-WmiObject Win32_ComputerSystem
$computer | Format-List -Property N*