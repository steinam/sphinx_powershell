.. include:: /Abbrev.txt

.. index:: Array

Arrays und Hashs
****************

.. topic:: Überblick

    - Erzeugen von Arrays und Hash
    - Ansprechen von Arrays und Hash
    - Durchlaufen von Arrays und HAsh


Arrays erweitern den Grundgedanken einer einfachen Variable um den Aspekt des Speicherns mehrerer Werte unter einem Variablenamen.

Letzlich gibt |PS| bei mehrzeiligen Ausgaben immer einen Array zurück.

.. code-block:: sh

	#die Ausgabe ist kein mehrzeiliger String, sondern ein Array von Strings
	#jede Zeile ist ein Element des Arrays
	$a = ipconfig
	$a
	Windows IP Configuration
	Ethernet adapter LAN Connection
	Media state
	. . . . . . . . . . . : Medium disconnected

	Connection-specific DNS Suffix:
	Connection location IPv6 Address  . : fe80::6093:8889.....
	IPv4 address  . . . . . . . . . . . : 192.168.1.35
	Subnet Mask . . . . . . . . . . . . : 255.255.255.0
	Standard Gateway . . . . . . . . . . : 192.168.1.1

.. image:: images/array_ipconfig.png


Da in diesem Beispiel lediglich Strings gespeichetr werden, bleibt eine Weiterverarbeitung nur mit Hilfe von Stringverarbeitung möglich.

Echte Powershell-Cmdlets reichen jedoch Objekte in die Arays zurück, die sich deutlich einfacher und eleganter bearbeiten lassen. Näheres dazu später.


.. index:: Array, Anlegen


.. raw:: latex

	\newpage

Erzeugen eigener Arrays
=======================

Wenn man Arrays nicht als Ergebnis erhält, so kann man sich recht einfach Arrays selbst erzeugen.

.. code-block:: sh

	#verschiedene Methoden, um Werte in Arrays zu definieren
	$array = 1,2,3,4
	$array = 1..4
	$array
	1
	2
	3
	4


	PS C:\> $array = "Hello", "World", 1, 2, (Get-Date)
	PS C:\> $array
	Hello
	World
	1
	2

	Montag, 16. August 2010 22:18:17



.. raw:: latex

	\newpage

Arrays mit einem oder keinem Element
====================================

.. code-block:: sh

	$array = ,1
	$array.Length
	1

	$array = @()
	$array.Length
	0
	$array = @(12)
	$array
	12
	$array = @(1,2,3,"Hello")
	$array


.. raw:: latex

	\newpage

Ansprechen von Array-Elementen
==============================

Die Inhalte des Arrays werden über den Index angesprochen

.. code-block:: sh

	# Create your own new array:
	$array = -5..12
	# Access the first element:
	$array[0]
	-5
	# Access the last element (several methods):
	$array[-1]
	12
	$array[$array.Count-1]
	12
	$array[$array.length-1]
	12
	# Access a dynamically generated array that is not stored in a variable:
	(-5..12)[2]
	-3



.. raw:: latex

	\newpage

.. index:: Array, Index

Ansprechen mehrerer Elemente eines Arrays
=========================================

Man kann mehrere Elemente eines Arrays durch eine kommaseparierte Liste von Indexwerten ansprechen. Das Ergebnis ist wiederum ein Array, der nur aus diesen Werten besteht.

.. code-block:: sh

	PS C:\> $list = Dir
	PS C:\> $list[1,4,7,12]

    Verzeichnis: C:\

    Mode                LastWriteTime     Length Name
    ----                -------------     ------ ----
    d----        20.07.2010     23:00            data
    d----        31.07.2009     22:40            Dokumente und Einstellungen
    d----        18.08.2009     20:54            inetpub
    d----        14.07.2009     04:37            PerfLogs


.. index:: Array, Hinzufügen, Entfernen


.. raw:: latex

	\newpage

Hinzufügen und Entfernen von Elementen
=======================================

Da |PS| die Länge von Arrays nicht ändern kann, muss beim Hinzufügen und Entfernen jeweils ein neuer Array angelegt werden und die Werte des alten Arrays in den neuen kopiert werden.

Das Hinzufügen erfolgt mit Hilfe des *+=* - Operators.

.. code-block:: sh

	PS C:\> $array = 1..5
	PS C:\> $Array += 6
	PS C:\> $array
	1
	2
	3
	4
	5
	6


Das Hinzufügen von Arrays hat jedoch "teure" Kopieraktionen im Hintergrund zur Folge

.. figure:: images/array_add.jpg
	:width: 50 px

.. figure:: images/Notizzettela.png
	:width: 500 px


Da |PS| aus Arrays keine Werte entfernen kann, hilft nur ein (umständliches) Umkopieren des Alten in einen neuen Array.

.. code-block:: sh

	PS C:\> $array = 1..5
	PS C:\> $Array += 6
	PS C:\> $array
	1
	2
	3
	4
	5
	6

	PS C:\> $array = $array[1..2] + $array[4..5]
	PS C:\> $array
	2
	3
	5
	6


.. raw:: latex

	\newpage

.. index:: Array, Kopieren

Kopieren von Arrays
===================

Das Kopieren von Arrays erfolgt durch Zuweisen der alten Arrayvariable an eine neue Arrayvariable. Dabei muss jedoch beachtet werden, dass nicht die Werte kopiert werden, sondern lediglich die Referenzvariablen. Dies kann gewünschte/unerwünschte Nebeneffekte besitzen.

.. code-block:: sh

	$array1 = 1,2,3
	$array2 = $array1
	$array2[0] = 99
	$array1[0]
					??


Um diesen Effekt nicht zu erhalten, müssen die Arrays mit Hilfe der **clone()**-Methode kopiert werden.

.. code-block:: sh

	$array1 = 1,2,3
	$array2 = $array1.Clone()
	$array2[0] = 99
	$array1[0]
					??


Immer wenn in einer solchen Situation neue Elemente hinzugefügt werden, wird hinter den Kulissen der gleiche Ablauf stattfinden.

.. code-block:: sh

	# Create array and store pointer to array in $array2:
	$array1 = 1,2,3
	$array2 = $array1
	# Assign a new element to $array2. A new array is created in the process and stored
	in $array2:
	$array2 += 4
	$array2[0]=99
	# $array1 continues to point to the old array:
	$array1[0]
	1

.. index:: Array, typisiert


.. raw:: latex

	\newpage

Arrays by val/by ref
=====================


Verweise auf Arrays bzw. ein lesender Zugriff auf Einzelwerte haben führen zu unterschiedlichen Ergebnissen, wenn die Werte anschließend geändert werden.

.. sidebar:: Demo

	.. only:: html

		.. figure:: images/array_val_ref.jpg
			:width: 50 px

	.. code-block:: sh

		$messwerte = 1,2,3,4,4,3,5,6
		#Indexpos    0 1 2 3 4 5 6 7
		# 0 = Temp in Celsius
		# 1 = Luftfeuchtigkeit in %
		# 2 = Wassertemp

		$kopie = $messwerte
		$messwerte[1] = 20
		$kopie[1]
		$kopie[1]= 30
		$messwerte[1]

		$luftfeuchtigkeit = $messwerte[1]
		$luftfeuchtigkeit = 40


.. only:: html

	.. figure:: images/array_val_ref.jpg
		:width: 50 px

.. figure:: images/Notizzettela.png
	:width: 460 px



**By val:**

Werden einzelne Werte aus einem Array in eine Variable kopiert, geschieht dies immer als echte Kopie. Die ausgelagerten Werte haben keinerlei Bezug mehr zu den Werten im Array.

**By ref:**

Wird der gesamte Array einer neuen Variable zugewiesen, so verweisen beide Variablen anschließend auf die gleiche Speicherstelle im Heap. Eine Änderung der Werte über eine Variable führt automatisch auch zu einer Veränderung in der anderen Variablen, weil beide auf die gleichen Speicherstellen im Heap zeigen.



.. raw:: latex

	\newpage

Typisierte Arrays
=================

Ähnlich wie bei Variablen können Arrays typisiert werden. Sie können dann nur noch bestimmte Datentypen aufnehmen.

.. code-block:: sh

	# Create a strongly typed array that can store whole numbers only:
	[int[]]$array = 1,2,3
	# Everything that can be converted into a number is allowed
	# (including strings):
	$array += 4
	$array += 12.56
	$array += "123"
	# If a value cannot be converted into a whole number, an error
	# will be reported:
	$array += "Hello"
	The value "Hello" cannot be converted into the type "System.Int32".
	Error: "Input string was not in a correct format."
	At line:1 char:6
	+ $array  <<<< += "Hello"


Zusammenfassung
================

Arrays can store as many separate elements as you like. Arrays assign a sequential index number to elements that always begin at 0. You create new arrays with @(Element1, Element2, ...). You can also leave out @() for arrays and only use the comma operator.

You can address single elements of an array or hash able by using square brackets. Specify either the index number (for arrays) of the desired element in the square brackets. Using this approach you can select and retrieve several elements at the same time.



.. raw:: latex

	\newpage

.. index:: Hashtable, Hash

Hash
===========================

.. figure:: images\hashmap_schueler.jpg
	:width: 600


Hashmaps sind eine weitere Möglichkeit, Daten in einer strukturierten Form zu speichern.
Im Gegensatz zum Array verwenden diese keinen Index zur Positionierung der Daten, sondern einen frei wählbaren Schlüssel, der durch eine Hashfunktion auf eine Indexposition umgerechnet wird.

.. image:: images/Hash_table.png


Vorteile der Hashtable im Vergleich zum Array
---------------------------------------------

- Freie Wahl des Schlüsselwertes
- Beliebing erweiterbar (ohne die "Verwaltungskosten" des Array)
- Schnellere Suche, da direkter Zugriff über den Schlüssel


http://powershell.com/cs/blogs/ebookv2/archive/2012/02/07/chapter-4-arrays-and-hashtables.aspx#using-hash-tables

http://technet.microsoft.com/en-us/library/ee692803.aspx

http://blogs.technet.com/b/heyscriptingguy/archive/2011/05/19/create-custom-objects-in-your-powershell-script.aspx

http://gallery.technet.microsoft.com/scriptcenter/925a517d-f400-4773-a79d-22ba9e91618e

Zusammenfassende Übung
----------------------

Ihre Firma möchte sicherstellen, dass sich in einem bestimmten Ordner der Windows-Server-Landschaft nur Dateien befinden, denen man *trauen* kann, d.h. die Integrität der Dateien ist sichergestellt. Der Einfachheit gehen wir davon aus, dass dieser Ordner keine Unterordner hat. Sie hat dazu einen garantiert sicheren Referenzordner als Vergleichsordner geschaffen, welcher alle Dateien des jeweiligen Ordners enthält. Andere Dateien sind nicht zugelassen; ebenso ist es wichtig, dass die Inhalte der Dateien gleich sind. Es handelt sich dabei sowohl um Textdateien als auch um Dateien im Binärformat (exe, dll, etc.). Sie können davon ausgehen, dass die Dateistruktur der Windows-Server identisch ist.

Realisieren Sie eine Lösung in Powershell.


| Vertical bars
| like this

siehe: http://blog.ideri.com/?p=1382

https://www.windowspro.de/script/dateien-vergleichen-powershell


http://www.ct-systeme.com/ct/tipps/Seiten/MD5Hashes.aspx

https://www.windowspro.de/script/dateien-vergleichen-powershell

http://www.tecchannel.de/server/windows/2054277/powershell_40_im_ueberblick/index3.html


http://blog.ideri.com/?p=1382

http://blog.brianhartsock.com/2008/12/13/using-powershell-for-md5-checksums/






**Lösungsvorschlag**


- Speichern der Servernamen in einer Textdatei, welche später per Schleife ausgelesen wird.
- Einlesen aller Dateien aus dem Referenzordner
- Einlesen aller Dateien aus dem Ordner des jeweiligen Rechners
- Findet sich Rechnerdateiname in Referenzdateiname ==> alles gut
      ansonsten: Rechnerdatei in Quarantäneordner schieben zur weiteren Behandlung
                 Ausgabe der Rechnerdatei in csv-Datei mit komplettem Pfad
- Kontrolle über Hash-Wert
      ==> gleicher Hashwert: Datei scheint integer zu sein
      ==> ungleicher Hashwert: Datei ist nicht integer;
                               ==> verschieben in Quarantäne (s.o.)

- Kontrolle des Inhalts:
   Compare-Object der Powershell kan auf Inhaltsgleichheit prüForegroundColor
   Bei Ungleichhekt verschieben in Quarantäne-Ordner (s.o.)


- Vergleich
