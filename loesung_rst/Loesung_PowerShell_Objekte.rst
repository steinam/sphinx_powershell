Objekte
=======

Aufgabe 1:
----------

.. sidebar:: Arbeitsblatt

	  - :download:`A1 Unkommentiert </images/customobjects/objekte_aufgabe1.pdf>`.
	  
	  - :download:`A1 Kommentiert </images/customobjects/objekte_aufgabe1_comment.pdf>`.


.. code-block:: sh

	$strComputer = "."
	
	# Neues leeres Objekt erstellen
	$Infos= New-object -TypeName PSObject
		
	# Wert an das Objekt anfügen
	Add-Member -InputObject $Infos -Name Manufacturer -Value 1 -MemberType NoteProperty
	# Wert an das Objekt anfügen
	Add-Member -InputObject $Infos -Name Model -Value 2 -MemberType NoteProperty
	# Wert an das Objekt anfügen
	Add-Member -InputObject $Infos -Name Memory -Value 3 -MemberType NoteProperty
	Add-Member -InputObject $Infos -Name BiosDescription -Value 3 -MemberType NoteProperty
	Add-Member -InputObject $Infos -Name SerialNumber -Value 3 -MemberType NoteProperty
	Add-Member -InputObject $Infos -Name OperatingSystem -Value 3 -MemberType NoteProperty
	 
	 Function SysInfo($Infos)
	 {
	     $colItems = Get-WmiObject Win32_ComputerSystem `
	     -Namespace "root\CIMV2" -ComputerName $strComputer

	     foreach($objItem in $colItems) {
	       $Infos.Manufacturer =  $objItem.Manufacturer
	       $Infos.Model = $objItem.Model
	      $Infos.Memory =  $objItem.TotalPhysicalMemory
	     }
	  }
	
	  Function BIOSInfo($Infos) 
	  {

	     $colItems = Get-WmiObject Win32_BIOS -Namespace "root\CIMV2" `
	     -computername $strComputer
	     foreach($objItem in $colItems) {
		     $Infos.BiosDescription = $objItem.Description
		     $Infos.SerialNumber =  $objItem.SerialNumber
	     }
	  }
	
	
          Function OSInfo($Infos)
          {
	     $colItems = Get-WmiObject Win32_OperatingSystem -Namespace ` "root\CIMV2" -Computername $strComputer 

	     foreach($objItem in $colItems) {
		     $Infos.OperatingSystem =  $objItem.Name
	     }
         }
	
	
	
        #*=============================================================
        #* SCRIPT BODY
        #*=============================================================
        #* Connect to computer
    

        #* Call SysInfo Function
        Write-Host "Sytem Information"
        SysInfo $Infos
    

       #* Call BIOSinfo Function
       Write-Host "System BIOS Information"
       BIOSInfo $Infos
       Write-Host

       #* Call OSInfo Function
       Write-Host "Operating System Information"
       OSInfo $Infos
       Write-Host


       $Infos | Select-Object Name, Model
	
       # CSV Export der Daten
       Export-Csv -InputObject $Infos -Path "C:\Temp\infos.csv"

	`
	
Aufgabe 2:
----------


.. sidebar:: Arbeitsblatt

	  - :download:`A2 Unkommentiert </images/customobjects/objekte_aufgabe2.pdf>`.
	  
	  - :download:`A2 Kommentiert </images/customobjects/objekte_aufgabe2_comment.pdf>`.



.. code-block:: csharp
	
	Measure-Command{
	$site_urls = Import-CSV C:\temp\weekly_stats.csv
	
	function bla
	{
	
	    foreach($url in $site_urls)
	    {
		$obj_avg = New-Object -TypeName psobject
		Add-Member -InputObject $obj_avg -MemberType NoteProperty -Name "site_url" -Value $url.site_url
		Add-Member -InputObject $obj_avg -MemberType NoteProperty -Name "url_avg" `
		    -Value ([int](([int]$url.monday + [int]$url.tuesday + [int]$url.wednesday + [int]$url.thursday + [int]$url.friday + [int]$url.saturday + [int]$url.sunday)/7))
		$obj_avg
	    }
	
	}
	
	bla | Sort-Object url_avg
	
	}
	
	
	
	
	Measure-Command {
	$site_urls2 = Import-CSV C:\temp\weekly_stats.csv
	
	$site_urls2 | Select @{name="site_url";expression={$_.site_url}}, `
	@{name="avg";expression={([int](([int]$_.monday + [int]$_.tuesday + [int]$_.wednesday + [int]$_.thursday + [int]$_.friday + [int]$_.saturday + [int]$_.sunday)/7))}} | `
	Sort-Object avg
	}
	
	



	
	
	add-type @"
	using System;
	public class siteurl{
		
		public string url = "";
		public double durchschnitt = 0;
	
		public void seturl(string _url)
		{
		    this.url = _url;
		}
	
		public void avg(int mon, int tue, int wed, int thu, int fri, int sat, int sun)
		{
			this.durchschnitt =  ((monday+tuesday+wednesday+ thursday + friday+ saturday + sunday)/7); 
		}
	}
	"@
	
	Measure-Command {
	function bla2
	{
	
	$site_urls = Import-CSV C:\temp\weekly_stats.csv
	
	    foreach($url in $site_urls)
	    {
		$net_avg = New-Object -typename siteurl
		$net_avg.seturl($url.site_url)
		$net_avg.avg($url.monday,$url.tuesday,$url.wednesday,$url.thursday,$url.friday,$url.saturday,$url.sunday)
		$net_avg
	    }
	
	}
	
	bla2 |select url, durchschnitt | Sort-Object durchschnitt
	 `
	
Aufgabe 3:
----------


Gegeben ist folgendes PS-Skript.

.. code-block:: sh

	Function Get-NetworkConfiguration
	{
	    param (
		[parameter(
		    ValueFromPipeline=$true,
		    ValueFromPipelineByPropertyName=$true,
		    Position=0)]
		[Alias('__ServerName', 'Server', 'Computer', 'Name')]   
		[string[]]
		$ComputerName = $env:COMPUTERNAME,
		[parameter(Position=1)]
		[System.Management.Automation.PSCredential]
		$Credential
	    )
	    process
	    {
		$WMIParameters = @{
		
		

- Wählen Sie statt der Write-Host-Ausgabe einen objektorientieten Ansatz

  Der foreach-teil muss wie folgt geändert werden:
  
  .. code-block:: sh
  
  	foreach ($adapter in (Get-WmiObject @WMIParameters))
        {
            $AdapterProperties = @{
                Server = $adapter.DNSHostName
                Adapter =  $adapter.Description
                IPAddress = $adapter.IpAddress
                SubnetMask = $adapter.IPSubnet
                DefaultGateway = $adapter.DefaultIPGateway
                DNSServers = $adapter.DNSServerSearchOrder
                DNSDomain = $adapter.DNSDomain
            }
           
            New-Object PSObject -Property $AdapterProperties
        }




- Sie wollen nicht alle Informationen ausgeben, sondern beispielsweise nur die IP-Adresse und die Subnet-Maske

   .. code-block:: sh
   
       Get-NetworkConfiguration ‘Server1’, ‘Server2’, ‘Server3’ | Format-Table Server, Adapter, SubnetMask, DefaultGateway –auto –wrap



- Können Sie mit diesem Skript mehrere Rechner abfragen ?

  ja



- Welche Rechner ihres Netzes benutzen den gleichen DNS-Server mit der 
  IP-Adresse ***.***.***.***. Eine Liste der Computer erhalten Sie mit Hilfe des CommandLets get-adcomputer

  .. code-block:: sh

  	Get-ADComputer –filter * | Get-NetworkConfiguration | Where-Object {$_.DNSServers –contains $IPofMyTroublesomeDNSServer}


- Wie viele Rechner benutzen den gleichen Default-Gateway

  .. code-block:: sh
  
  	Get-ADComputer -filter * | Get-NetworkConfiguration | Group-Object DefaultGateway -NoElement
  	
  	

