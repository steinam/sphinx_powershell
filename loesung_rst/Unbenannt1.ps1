﻿$site_urls = Import-CSV C:\temp\weekly_stats.csv

function bla
{

    foreach($url in $site_urls)
    {
        $obj_avg = New-Object -TypeName psobject
        Add-Member -InputObject $obj_avg -MemberType NoteProperty -Name "site_url" -Value $url.site_url
        Add-Member -InputObject $obj_avg -MemberType NoteProperty -Name "url_avg" `
            -Value ([int](([int]$url.monday + [int]$url.tuesday + [int]$url.wednesday + [int]$url.thursday + [int]$url.friday + [int]$url.saturday + [int]$url.sunday)/7))
        $obj_avg
    }

}

bla | Sort-Object url_avg


$site_urls2 = Import-CSV C:\temp\weekly_stats.csv

$site_urls2 | Select @{name="site_url";expression={$_.site_url}}, `
@{name="avg";expression={([int](([int]$_.monday + [int]$_.tuesday + [int]$_.wednesday + [int]$_.thursday + [int]$_.friday + [int]$_.saturday + [int]$_.sunday)/7))}} | `
Sort-Object $_.avg