Variable_und_Datentyp
=====================


- Was ist das Ergebnis des folgenden Skriptes

	.. code-block:: sh
		
	   [int]$a = -7
	   [int]$b = 3
	   Write-Host($a/$b)
	   Write-Host([int]($a/$b))
	   -2,3333
	   -2 
   
Das Ergebnis der Division zweier Ganzzahlen ist ein Wert vom Typ double. Erst durch das Casten des Wertes in ein Integer wird eine Ganzzahl als Ergebnis zurÃ¼ckgegeben. Bei Komma 5 wird der Integer-Wert auf die hÃ¶here Zahl aufgerundet

- Welche Zeichen sind für das Erstellen eines Variablennamens möglich. Recherchieren Sie falls notwendig auch im Internet

	You can use virtually any variable name you choose, names are not case sensitive. Note: there are illegal characters such as; ! @ # % & , . and spaces. PowerShell will throw an error if you use an illegal character.

	I stated virtually any name as PowerShell does have a set of keywords that are reserved and cannot be used as variable names:

    	* break
		* continue
		* do
		* else
		* elseif
		* filter
		* foreach
		* function
		* if
		* in
		* return
		* switch
		* until
		* where
		* while


		
- Welchen Unterschied macht es, ob sie als Kennzeichen für einen String das Zeichen " oder das Zeichen ' einsetzen.

	Beide Zeichen sind erlaubt, beim '-Zeichen wird nicht interpoliert. Eine Variable innerhalb des Strings würde nicht mit dem Wert dargestellt. Beispiel:
	
	.. code-block:: sh
	
		$Teacher1 = "Steinam"
		$TEacher2  = 'Sierl'
		
		$ausgabe = "Hallo $Teacher1 "
		$ausgabe2 = "Hallo $Teacher2 "
		
- Wie kann man erfahren, welcher Datentyp eine Variable bzw. ein Wert hat
  	
	.. code-block:: sh
	
		(42).GetType()
		
				
		IsPublic IsSerial Name               BaseType
		-------- -------- ----               --------
		True     True     Int32              System.ValueType
		
		
		(-7/3).GetType()
		(9/3).GetType()
		
		
Die Werte müssen erst berechnet werden. Dies wird durch die Klammerung erreicht.

- Sie möchten einen String als Datum weiterverwenden, falls er ein reguläres Datum ist.

	.. code-block:: sh

		[DateTime]"03/13/1964"



- Welche Vorteile hat das Casten eines Strings in ein DateTime-Objekt

	Man verfügt anschließend über die Methoden des DateTime-Objektes, was eine Weiterbearbeitung häufig einfacher macht, z.B. bei Datumsberechnungen.
	
	.. code-block:: sh
		
		$result = ((get-date).subtract([DateTime]"03/13/1964"))
		($result.Days/366).gettype()
	
- Welche Variablen sind beim Start von Powershell bereits vorhanden ? Wie kann man sie sichtbar machen ?

	.. code-block:: sh
	
		dir variable:
		
	zeigt alle in der PS-Sitzung verwendeten bzw. bereits vorhandenen Variablen
	
- Folgender Quellcode ist vorhanden

   .. code-block:: sh
   	
   	$a = "Hello"
   	$b = 123
   	
   - Bestimmen Sie die Länge des Wertes der Variable $a
   - An welcher Stelle im String kommt der Buchstabe "e" vor
   - Tauschen Sie den Buchstaben 'e' durch den Buchstaben 'a'
   
   .. code-block:: sh
   
   	   $a.Length

   	   $a.IndexOf("e")

   	   $a.Replace("e", "a")
   

   	  

