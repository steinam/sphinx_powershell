Bedingungen
===========

.. _loesung_bedingungen:

- Mit welchen Vergleichsoperatoren können Sie in Bedingungen arbeiten ?

.. code-block:: sh

	-eq,  -ne,  -ge,  -gt,  -lt,  -le,  -like,  -notlike,  -match,  -notmatch,  -contains, -notcontains, -is, -isnot

	-and, -or, -xor, -not

- Folgende Variablen sind gegeben:

	.. code-block:: sh

		$user = 'steinam'
		$pass = 'passwort'

   Formulieren Sie eine Bedingung in Powershell, die gleichzeitig auf einen korrekten Usernamen und Passwort abfragt.

   .. code-block:: sh

   	if($user -ieq 'STEINAM' -and $pass -eq 'passwort')
   	{
   		Write-Host "OK"
   	}
   	else
   	{
   		Write-Host "Fehler"
   	}




* Mit Hilfe von WMI können Sie feststellen, welche Rolle ein Computer innerhalb einer Domäne spielt.
  Es gibt 6 verschiedene Rollen:

	- 0 = Stand alone workstation
	- 1 = Member Workstation
	- 2 = Stand Alone Server
	- 3 = Member Server
	- 4 = Backup Domain Controller
	- 5 = Primary Domain Controller

	Falls andere Werte zurückkommen, kann die Rolle nicht eindeutig definiert werden

	Erstellen Sie ein Skript, welches die jeweilige Rolle in Textform ausgibt.

	.. code-block:: sh

		$wmi = get-wmiobject win32_computersystem
		"computer " + $wmi.name + " is: "

		switch ($wmi.domainrole)
		{
			 0 {" Stand alone workstation"}
			 1 {" Member workstation"}
			 2 {" Stand alone server"}
			 3 {" Member server"}
			 4 {" Back up domain controller"}
			 5 {" Primary domain controller"}
			 default {" The role can not be determined"}
		}

- Drucker auf verschiedenen Betriebssystemen ermitteln

	Verschiedene Betriebssysteme nutzen manchmal unterschiedliche WMI Klassen und/oder Eigenschaften, so z.B. Windows XP, Windows 2003 und Windows 2000.
	Sie wollen die auf diesen Betriebssystemen verwendeten Drucker auflisten und benötigen ein universell einsetzbares Skript.
	Verwenden Sie ein if-Statement zum Ermitteln der korrekten OS-Version und verwenden Sie den dafür vorgesehenen Code.

	Benutzen Sie die WMI-Klasse win32_OperatingSystem zum Ermitteln der OS-Version sowie win32_Printer auf XP und 2003 und win32_PrintJob auf Win2000-Systemen

	.. code-block:: sh

		$strComputer = Read-Host "Printer Report â€“ Enter Computer Name"
		$OS = Get-WmiObject -Class win32_OperatingSystem -namespace "root\CIMV2" -ComputerName $strComputer

		# if statement to run code for Windows XP and Windows 2003 Server.
		if (($OS.Version -eq "5.1.2600") -or ($OS.Version -eq "5.2.3790"))
		{
			write-host "Computer Name: " $strComputer
			#nested if statement
			if ($OS.Version -eq "5.1.2600") {write-host "OS Version: Windows XP"}
			elseif ($OS.Version -eq "5.2.3790") {write-host "OS Version: Windows 2003"}
				$colPrinters = Get-WmiObject -Class win32_Printer -namespace "root\CIMV2" -computerName $strComputer
					foreach ($objPrinter in $colPrinters) {
					write-host "Name: " $objPrinter.Name
					write-host "Description: " $objPrinter.Description
					write-host
					}
		}

		# if statement to run code for Windows 2000 Server
		elseif ($OS.Version -eq "5.0.2195")
		{
			write-host "Computer Name: " $strComputer
			write-host "OS Version: Windows 2000 Server"
				$colPrinters = Get-WmiObject -Class win32_PrintJob -namespace "root\CIMV2" -computername $strComputer
				foreach ($objPrinter in $colPrinters) {
				write-host "Name: " $objPrinter.Name
				write-host "Description: " $objPrinter.Description
				write-host
				}
		}
		# if OS not identified
		else {write-host "The OS for: $strComputer is not supported."}
		write-host "END OF REPORT"
