
Interaktivmodus
===============

.. _loesung_interaktiv:

- Welche Aliase gibt es f�r das Get-ChildItem-Cmdlet

  .. code-block:: sh
	
	get-alias -definition "*childitem*
		
- Finden Sie mit Hilfe von Get-Help heraus, wof�r das Cmdlet Stop-Process eingesetzt wird. Nutzen Sie die Hilfeinformationen, um mit Stop-Process alle Instanzen des Notepad-Editors zu schlie�en. Wie lautet der korrekte Befehl

  .. code-block:: sh
  
  	Get-help Stop-Process -Detailed
  	
  	Stop-Process -Name notepad
  	
- Versuchen Sie wie im vergangenen Beispiel mit Stop-Process einen Prozess zu beenden, der gar nicht vorhanden ist, erhalten Sie eine Fehlermeldung. Wie kann man diese Fehlermeldung unterdr�cken ?

  Zum Unterdr�cken von Fehlermeldungen kann man den Parameter **-ErrorAction** mit dem Wert *SilentlyCOntinue* verwenden 
	
  .. code-block:: sh
	
	Stop-Process -Name notepad -ErrorAction SilentlyContinue
		
- Nutzen Sie das Wissen �ber die Parameter von Write-Host dazu, um einen wei�en Text auf rotem Grund auszugeben.

  Die f�r die farbigen Textausgaben g�ltigen Parameter hei�en *ForeGroundColor* und *BackgroundColor*
   
  .. code-block:: sh
   
  	get-help Write-Host -Parameter *color
   		
  	Write-Host "Hallo" -ForeGroundColor KeineAhnung
   		
   		
- Wie kann man sich die Parameter eines Cmdlets per get-help anzeigen lassen.

  .. code-block:: sh
	
	get-help get-eventlog -Parameter *
  
- Windows hat verschiedene EventLogs, welche man mit Hilfe des Cmdlets **Get-Eventlog** prinzipiell abfragen kann. Aber wie erh�lt man die Namen der Eventlogs, die man einsehen will. 

  .. image:: /images/eventverwaltung.jpg

  Versuchen Sie es mit Hilfe von **get-help get-eventlog** herauszufinden.

  .. code-block:: sh
   
 	get-eventlog -List
   		
- Listen Sie s�mtliche Eintr�ge des **System**-Eventlogs auf

  .. code-block:: sh
   
 	  get-eventlog -LogName System
   		
- Listen Sie nur die Ereignisse aus dem System-Ereignisprotokoll auf, die Fehler anzeigen. Schauen Sie sich dazu die Spalten an, die Get-EventLog liefert. Gibt es einen entsprechenden Parameter


  Die Spalte mit dem Typ des Ereignisses hei�t *EntryType*; es gibt ebenso eine gleichnamigen Parameter.
  
  .. code-block:: sh
  
  	Get-EventLog -LogName System -EntryType Error
  
- Listen Sie alle Error-Eintr�ge des System-Ereignisprotokolls der letzten 24 Stunden auf Welche Parameter könnten den Zeitraum einschr�nken.

  .. code-block:: sh
	
		  	Get-EventLog -LogName System -EntryType Error -Newest 20
		  	Get-EventLog -LogName System -EntryType Error -After "20.9.2011"
		  	
- Listen Sie alle Error-Eintr�ge der letzten 25 Stunden auf, ohne ein konkretes Datum eingeben zu m�ssen.

  Mit Hilfe der CmdLets Get-Date und New-TimeSpan kann man einen variablen Zeitraum schaffen
		
  .. code-block:: sh
				
	$zeitraum = (get-date) - (New-TimeSpan -Day 1)
	Get-EventLog -LogName System -EntryType Error -After $zeitraum
		
- Listen Sie alle Error-Eintr�ge eines anderen Computers der letzten 24 Stunden auf, ohne ein konkretes Datum eingeben zu m�ssen. 

  .. code-block:: sh
		
	Get-EventLog -LogName System -EntryType Error -After $zeitraum -computername 10.10.10.10
		
