    function Get-CountDown() {
        param(
        [Parameter(
        Mandatory=$false)]
        [int]$hours=0,
     
        [Parameter(
        Mandatory=$false)]
        [int]$minutes=0,
     
        [Parameter(
        Mandatory=$false)]
        [int]$seconds=0
        )
       
        #if no time passed then default to 1 hour
        if($hours-eq0 -and $minutes-eq0 -and $seconds-eq0){
        $hours=1
        }
       
        #setup timespan variables
            $startTime = get-date
            $endTime = $startTime.addHours($hours)
            $endTime = $endTime.addMinutes($minutes)
            $endTime = $endTime.addSeconds($seconds)
     
        $Check=0
        $timeSpan = new-timespan $(get-date) $endTime
       
        #loop to update progress bar
        while ($timeSpan -gt 0) {
                    $timeSpan = new-timespan $(get-date) $endTime
            if($Check -ne 1){$timeSpan2=$timeSpan;$Check=1}
                     
            #generate time remaining string                
                    $TimeRemaining = $([string]::Format("Time Remaining: {0:d2}:{1:d2}:{2:d2}", `
                    $timeSpan.hours, `
                    $timeSpan.minutes, `
                    $timeSpan.seconds))
           
            #calc percent time elapsed
            $percentRem = ([math]::round($timeSpan.ticks/$timeSpan2.ticks*100,0))
            if($percentRem-lt0){$percentRem=0}
            $percentRem=100-$percentRem
           
            write-progress -activity "Shutdown" -status $TimeRemaining -percentcomplete $percentRem
           
                    sleep 1
                    }
            }
     
    get-countdown
