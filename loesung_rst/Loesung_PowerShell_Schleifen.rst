Schleifen
=========

.. _loesung_schleifen:


- **Ping**

	Schreiben Sie eine Programm, welches Ihnen alle per PING erreichbaren Rechner eines Netzes ermittelt. Das Programm soll alle Adressen von 192.168.0.1 bis 192.168.0.255 pingen. Wenn ein PINg erfolgreich war, soll dies als Ausgabe angezeigt werden.
	
	.. code-block:: sh
	
		$ping = New-Object System.Net.NetworkInformation.Ping
		$i = 0
		$liste = 1..255
		for($y=0;$y -le 255;$y++)
		{
			$ip = "192.168.0." + [string]$liste[$y]
			$Res = $ping.send($ip)
			if ($Res.Status -eq "Success")
			{
					$result = $ip + " = Success"
					Write-Host $result
					$y++
			}
		}
		$Hosts = [string]$y + " Hosts is pingable"
		Write-Host $Hosts
		
		#Eine andere Lösung ist powershell-typischer (per pipeline)
		
		$ping = New-Object System.Net.NetworkInformation.Ping
		$i = 0
		1..255 | foreach 
		{ 
			$ip = "192.168.0.$_"
			$Res = $ping.send($ip)
		
			if ($Res.Status -eq "Success")
			{
				$result = $ip + " = Success"
				Write-Host $result
				$i++
			}
		 }
		 $Hosts = [string]$i + " Hosts is pingable"
		 Write-Host $Hosts

- **ZahlenRaten**

  Erstellen sie ein Skript, welches folgende Aufgabe erfüllt: Es muss eine Meldung anzeigen, in der der Benutzer aufgefordert wird, eine Zahl zwischen 1 und 50 einzugebeenn. Das Skript muss die vom Benutzer eingegebene Zahl mit einer zufällig generierten Zahl vergleichen. Wenn die Zahlen nicht übereinstimmen, muss das Skript eine Meldung anzeigen, in der angegeben wird, ob die geratene Zahl zu hoch oder zu niedrig war, und der Benutzer aufgefordert wird, noch einmal zu raten.

  Wenn der Benutzer richtig rät, muss das Skript die Zufallszahl sowie die Anzahl der Rateversuche anzeigen. An diesem Punkt ist das Spiel beendet, das Skript muss also auch beendet werden. 

  .. code-block:: sh

	 $guesses = 0

	 $low = 1
	 $high = 50

	 $a = New-Object Random
	 $a = $a.Next($low,$high)

	 while ($true)
	 {
		 $guess = read-host "Enter a number between 1 and 50: "
		 $guesses++

		 if ($guess -eq $a)
		 {
				 "Random Number: " + $guess
				 "Number of Guesses: " + $guesses
				 break
		 }
		 elseif ($guess -gt $a)
		 {
				 "Too high"
		 }
		 else
		 {
				 "Too low"
		 }
	 }

	
  .. image:: /images/zahlenraten.png

	
- **Dateien kopieren**

	Diese Skripts sollen folgende Aufgaben ausführen:

	- Durchsuchen des Ordners "C:\Scripts" und dessen Unterordnern.
	- Durchsuchen jedes Ordners nach sämtlichen Textdateien (Dateien mit der Erweiterung .txt) und Prüfen des Erstellungsdatums jeder Datei.
	- Kopieren/Verschieben jeder .txt-Datei, die mehr als 10 Tage zuvor erstellt wurde, in den Ordner "C:\Old".
	- Ausgeben des Dateinamens (kein vollständiger Pfad, nur Dateiname) jeder kopierten Datei.
	- Ausgeben der Anzahl der kopierten Dateien. 

	.. code-block:: sh

		foreach ($i in Get-ChildItem C:\Scripts -recurse)
		{
			if (($i.CreationTime -lt ($(Get-Date).AddDays(-10))) -and ($i.Extension -eq ".txt"))
			{
				#Copy-Item $i.FullName C:\old
				$i.Name
				$x = $x + 1
			}
		}
		"Total Files: " + $x

		
- **Fonts finden**

.. code-block:: sh

	$total = 0

	$a = get-item "hklm:\\Software\Microsoft\WindowsNT\CurrentVersion\Fonts"
	$f = $a.GetValueNames()

	foreach ($i in $f)
	{
		if ($i.contains("TrueType"))
		{
			$total++
			$i
		}
	}


	
- **Schleifen optimieren**

	Die Ausgabe des folgenden Powershell-Befehls dauert u.U. sehr lange. Verbessern Sie das Statement im Hinblick auf die Auführungsgeschwindigkeit

	.. code-block:: sh
	
		# Foreach loop lists each element in a collection:
		Foreach ($element in Dir C:\ -recurse) { $element.name }

		
	Lösung: Ersetze Foreach durch das ForEach-Objekt, das die Elemente der Pipeline sofort bearbeiten kann.

	.. code-block:: sh
	
		# ForEach-Object lists each element in a pipeline:
		Dir C:\ -recurse | ForEach-Object { $_.name }

	.. note:
	
		Foreach ist dann schneller als das ForEach-Objekt, wenn die Elemente einer Liste bereits vorliegen. Ein Vergleich der Ausführungszeiten zeigt teilweise dramatische Unterschiede.

		.. code-block:: sh
		
			# Create your own array:
			$array = 3,6,"Hello",12
			# Read out this array element by element:
			Foreach ($element in $array) {"Current element: $element"}
			Current element: 3
			Current element: 6
			Current element: Hello
			Current element: 12
			
			
			ForEach-Object and the pipeline could also iterate through an 
			array:
			$array = 3,6,"Hello",12
			$array | ForEach-Object { "Current element: $_" }
			
			But Foreach is significantly quicker. You can find out how dramatic the time 
			advantage is by using Measure-Command cmdlet:
			
			(Measure-Command {
			  $array | ForEach-Object { "Current element: $_" }}).totalmilliseconds
			2.8
			
			(Measure-Command {
			  Foreach ($element in $array) {"Current element: $element"}}).totalmilliseconds
			0.2


- **Dateien per ftp hochladen**

	Sie sollen alle Dateien eines zu spezifizierenden Ordners auf einen ftp-server hochladen. Übergeben Sie den Ordner auf der Kommandozeile

	.. code-block:: sh
	
		#we specify the directory where all files that we want to upload  
		#$Dir="C:/Dir"    
		
		if(test-path $args[0])
		{
		
			$Dir=$args[0]
			
			#ftp server 
			$ftp = "ftp://ftp.server.com/dir/" 
			$user = "user" 
			$pass = "Pass"  
			 
			$webclient = New-Object System.Net.WebClient 
			 
			$webclient.Credentials = New-Object System.Net.NetworkCredential($user,$pass)  
			 
			#list every sql server trace file 
			foreach($item in (dir $Dir "*.trc")){ 
				"Uploading $item..." 
				$uri = New-Object System.Uri($ftp+$item.Name) 
				$webclient.UploadFile($uri, $item.FullName) 
			 } 
		}

	Unter-Windows kann man auch das BitsTransfer-Cmdlet benutzen. 
	Muss mit Import-Module BitsTransfer in die PS geladen werden (PS V. 2.0). Siehe dazu auch http://msdn.microsoft.com/en-us/library/ee663885%28v=VS.85%29.aspx
	
- **Übersicht über laufende Dienste**

	Verschaffen Sie sich einen Überblick über die laufenden Prozesse. Falls die Prozesse einen bestimmten Namen besitzen, stoppen Sie diese Prozesse bzw. geben das Wort "Gefunden <Dienstname> aus.
	
	Erweitern Sie das Skript, indem Sie die laufenden Dienste in eine Datei schreiben, um sie später wie oben bereits geschehen, auszuwerten. Welche Schwierigkeiten können bei diesem Vorhaben auftreten.	
	
	.. code-block:: sh
	
		Get-Service | ForEach-Object { 
			if($_.ServiceName -match "RpcSs") 
			{
				Stop-Service $_.ServiceName
				Write-Host "Gefunden: "  $_.Name
			}
		}
		
		Get-Service | Export-Clixml C:\dienste.xml
		foreach ($service in Import-Clixml C:\dienste.xml) 
		{ 
			if($service.ServiceName -match "RpcSs") 
			{
				Write-Host "Gefunden: "  $service.Name
			}
		}
		
- **Schätzen der Festplattenauslastung**

	Wie lange wird der Speicherplatz einer Festplatte ausreichen, wenn ihr Inhalt jeden Monat um ca. 7,5% wächst. Die Platte hat eine Kapazität von 2 TBiT, eine Startbelegung von 100 MBiT

	.. code-block:: sh
	
		$Zuwachs = 7.5
		$FestplatteMax = 2TB
		$StartKap = 100MB
		$FestplatteCurKap = $StartKap
		$i = 0		
		do
		{
		
			$FestplatteCurKap *= 1 + $Zuwachs/100
			$i += 1
			Write-Host "Monat: " $i 	
		
		}while($FestplatteCurKap -lt $FestplatteMax)

		$result = Get-Date
		$result = $result.AddMonths($i)
		$result

		
- **Berechnung des Notendurchnschnitts und Notenverteilung eines Testes**

	Ein Test ergab folgende Einzelnoten:
	6,3,4,2,1,2,3,4,1,2,3,2,1,3,4,5,3,5,4,3,2,2,2,1,2,3
	
	Berechnen Sie die Anzahl jeder Note sowie den Durchschnitt

	Die folgende Lösung setzt in einigen Teilen die Pipeline der Powershell ein. Dies hätte auch durch eine weitere Schleife erledigt werden können.
	Bei der Zuweisung der Häufigkeiten pro Noten wird ein 2. Array eingesetzt, ders ich pro Indexposition (ab 1) die absolute Zahl der jeweiligen Note merkt. In Indexposition 0 wird später der Durchschnitt abgelegt
	
	.. code-block:: sh
	
		$noten_sa1 = 6,3,4,2,1,2,3,4,1,2,3,2,1,3,4,5,3,5,4,3,2,2,2,1,2,3
		$notenwerte = 1,2,3,4,5,6
		$notenstat = 0,0,0,0,0,0,0

		1..6 | ForEach{
			$anzahl = 0
			for($i = 0;$i -lt $noten_sa1.length; $i++)
			{
				if($noten_sa1[$i] -eq $_)
				{
					Write-Host "Note "  $_ "vorhanden an Position " + ($i + 1)
					$anzahl++
				}
				else
				{
					Write-Host "Note nicht vorhanden"
				}
			}
			$notenstat[$_] = $anzahl 

		}
		Write-Host $notenstat

		$durchschnitt = 0
		$quersumme=0
		for ($i = 1; $i -lt $notenstat.length;$i++)
		{
			$quersumme += $notenstat[$i]*$i
		}
		$durchschnitt = $quersumme / $noten_sa1.length
		$notenstat[0] = $durchschnitt
		$quersumme
		$durchschnitt
		
	Eine weitere und vielleicht elegantere Möglichkeit wäre die Verwendung einer Hashmap anstelle des Arrays $notenstat. Damit müsste man die Indexpositionen nicht *missbrauchen*, sondern könnte die Noten als Key und die Anzahl als Values einsetzen.
	
	Eine Lösungsansatz:
	
	.. code-block:: sh
	
		$noten_sa1 = 6,3,4,2,1,2,3,4,1,2,3,2,1,3,4,5,3,5,4,3,2,2,2,1,2,3
		$notenwerte = 1,2,3,4,5,6
		$notenhash = @{}

		1..6 | ForEach{
			$anzahl = 0
			for($i = 0;$i -lt $noten_sa1.length; $i++)
			{
				if($noten_sa1[$i] -eq $_)
				{
					#Write-Host "Note "  $_ "vorhanden an Position " + ($i + 1)
					$anzahl++
				}
				else
				{
					# Write-Host "Note nicht vorhanden"
				}
			} 
			$notenhash.Add($_, $anzahl)
		}
		
		$durchschnitt = 0
		$quersumme = 0
		foreach($key in $notenhash.get_Keys())
		{
			Write-Host $key $notenhash[$key]
			$quersumme += $key * $notenhash[$key]
		}
		$durchschnitt = $quersumme / $noten_sa1.length
		$durchschnitt

		

