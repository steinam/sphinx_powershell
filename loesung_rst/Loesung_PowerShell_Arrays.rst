Arrays
======


.. _loesung_arrays:

- Schreiben Sie eine Funktion, die als Parameter einen Pfad erwartet und anschließend den Inhalt des Pfades ausgibt

	.. code-block:: sh
	
		function listDirectory($pfad)
		{
	   
		cd $pfad
		Dir $pfad |Out-File  C:\temp\Ausgabe.txt
	
		}
    

- Wie überprüfen Sie, ob ein Array einen bestimmten Wert enthält


	Mit Hilfe des -contains - Operators
	
	

- Wie überprüfen Sie, ob zwei Arrays gleich sind	
	
	Folgende Funktion überprüft zwei Arrays auf Gleichheit
	
	
	
	.. code-block:: sh
	
		function AreArraysEqual($a1, $a2) {
			if ($a1 -isnot [array] -or $a2 -isnot [array]) {
			  throw "Both inputs must be an array"
			}
			if ($a1.Rank -ne $a2.Rank) {
			  return $false 
			}
			if ([System.Object]::ReferenceEquals($a1, $a2)) {
			  return $true
			}
			for ($r = 0; $r -lt $a1.Rank; $r++) {
			  if ($a1.GetLength($r) -ne $a2.GetLength($r)) {
					return $false
			  }
			}
		
			$enum1 = $a1.GetEnumerator()
			$enum2 = $a2.GetEnumerator()   
		
			while ($enum1.MoveNext() -and $enum2.MoveNext()) {
			  if ($enum1.Current -ne $enum2.Current) {
					return $false
			  }
			}
			return $true
		} 
		
		
- Ein String ist eigentlich ein Array aus Elementen des Datentyps CHAR.

	.. code-block:: sh
	
		$test = "Hello"
		$test[0].Gettype()
		
		IsPublic IsSerial Name                                     BaseType            
		-------- -------- ----                                     --------            
		True     True     Char                                     System.ValueType    
		
	Die Ansprechen eines einzelnen Elementes ist auch mit $test[1] problemlos möglich. Wie können Sie aber ein einzelnes Element eines bestehenden Strings mit Hilfe der Array-Indiziierung ändern, also etwas so
	
	.. code-block:: sh
		
		$integerarray = 1,2,2
		$integerarray[2] = 345	# geht
	
		$test = "Hello"
		$test[0] = "F"  		#Fehlermeldung
		
			
	**Lösung:**
	
	Das Umwandeln mit Hilfe der String-Methode ToCharArray() erzeugt einen Array aus CHAR-Elementen, die verändert werden können.
	
	.. code-block:: sh
	
		$ergebnis = $test.ToCharArray()
		$ergebnis[0] = "F"
		

- Überprüfen sie, ob ein bestimmter Wert in einem Array enthalten ist, z.B.
	
	.. code-block:: sh
	
		$arrColors = "blue", "red", "green", "yellow", "white", "pink", "orange", "turquoise"
		
	Prüfen Sie, ob der Wert "green" enthalten ist
	
	.. code-block:: sh
	
		foreach($element in $arrColors)
		{
			if($element -eq "green")
			{
				Write-Host
			}
			else
			{
				Write-Host "Not found"
			}
		}
		
	Es geht natürlich in Powershell auch einfacher
	
	.. code-block:: sh
	
		$arrColors = "blue", "red", "green", "yellow", "white", "pink", "orange", "turquoise"
		
		$result = $arrColors -contains "green"
		
	
- Haben wir Elemente, die mit "bl" beginnen

	.. code-block:: sh
	
		$arrColors = "blue", "red", "green", "yellow", "white", "pink", "orange", "turquoise", "black"
		
		$arrColors -like "bl*"
	
- Sortieren sie den obigen Array alphabetisch

	.. code-block:: sh
	
		$arrColors = "blue", "red", "green", "yellow", "white", "pink", "orange", "turquoise", "black"
	
		$arrColors = $arrColors | Sort-Object
		
- Vergleichen Sie die Geschwindigkeit beim Erstellen und Suchen der folgenden Datenstrukturen: Array vs. Hashtable. Welches Ergebnis stellen Sie fest ?

	.. code-block:: sh
	
		$testarray = @()
		$testhash = @{}

		Write-Host "Array anlegen"
		Measure-Command {
			for($i = 1; $i -lt 10000;$i++)
			{
				$testarray += $i	
			}
		}

		Write-Host "Hash anlegen"
		Measure-Command {
			for($i = 1; $i -lt 10000;$i++)
			{
				$testhash.Add($i, $i)
			}
		}

		Write-Host "Array enthält 9000"
		Measure-Command{
			$testarray -contains 9000
		}

		Write-Host "Hash enthält 9000"
		Measure-Command{
			$testhash.get_Item(9000)
		}
	
	Das Skript erzeugt folgende Ergebnisse auf einem ASUS Netbook (N270, 2 GByte Ram, Windows 7):
	
	.. code-block:: sh
	
		Array anlegen
		...
		TotalSeconds      : 27,2567811
		TotalMilliseconds : 27256,7811

		Hash anlegen
		TotalSeconds      : 0,3869414
		TotalMilliseconds : 386,9414

		Array enthält 9000
		.....
		TotalSeconds      : 0,0359344
		TotalMilliseconds : 35,9344

		Hash enthält 9000
		...
		TotalSeconds      : 0,0001328
		TotalMilliseconds : 0,1328


	**Fazit:** 
	
	Gerade wenn Listen zur Laufzeit wachsen ist ein Array schon bei kleinen Datenmengen relativ langsam. Auch die Suche innerhalb von kleinen Datenmengen wird über eine Hashtable deutlich beschleunigt. 
	
- Sie besitzen eine Textdatei mit Familienname und Loginname der 
  Firmenmitarbeiter. Sie umfasst über 9000 Einträge und soll dazu hergenommen werden, um "vergessene" Loginnamen zu suchen. Überlegen Sie sich, wie Sie diese Textdatei innerhalb von Powershell-Skripten nutzen können.

  .. code-block:: sh

	$fam_login = @{}

	Import-Csv user_passwd.csv | ForEach-Object { $fam_login[$_.username] = $_.passwort}
	$fam_login
	$fam_login["?9d8k6L2"]
	
	$fam_login.GetEnumerator() | select key, value | export-csv .\testexport.csv
	
	$fam_login | export-clixml -path fam_login.xml
	
	
	$fam2 = import-clixml -path fam_login.xml
	

  Durch die Nutzung der export-clixml und import-clixml - Commandlets werden Dateien im XML-Format geschrieben. Diese machen es einfacher möglich, Metainformationen zu lesen und zu schreiben.
	
- Folgende Noten einer Schulaufgabe sind vorhanden:

  2,3,3,2,4,5,4,3,2,1,6,4,3,2,5,4,1,2,3,6,5,4,3,2,1,4,3,2,1,5,4,4,4,3,2,3,4
  
  Lösen Sie mit Hilfe eines Arrays die folgenden Aufgaben:

  - Ermitteln Sie den Notendurchschnitt
  - Ermitteln sie die Häufigkeit jeder Note (absolut/prozentual)
  - Wie viele Noten liegen unter/über dem Schnitt
  - Erstellen Sie eine sortierte Ausgabe in Form eines Write-Host-Statements
  
  .. code-block:: sh
  
  	$noten = 2,3,3,2,4,5,4,3,2,1,6,4,3,2,5,4,1,2,3,6,5,4,3,2,1,4,3,2,1,5,4,4,4,3,2,3,4

	Write-Host "Anzahl der Noten: "  $noten.count
	
	#Durchschnitt berechnen
	$summe = 0
	foreach($note in $noten)
	{
	   $summe = $summe + $note 
	
	}
	$durschnitt = $summe/$noten.Count
	
	
	#Häufigkeit jeder Note berechnen
	#Mit Hilfe eines 2. Arrays; 
	#Index dient als Notenzahl
	#das erste Element(0) bleibt frei
	$haeufigkeit = 0,0,0,0,0,0,0
	
	#Schleife hätte man auch in der ersten Lösung
	#novch mit reinnehmen können
	foreach($note in $noten)
	{
	    $haeufigkeit[$note]+= 1
	}
	
	for($i = 1; $i -lt $haeufigkeit.Count;$i++)
	{
	    Write-Host "Note $i = " $haeufigkeit[$i]
	    #$prozent = 0
	    $prozent = 100/$noten.Count*$haeufigkeit[$i]
	
	    Write-Host "Note $i = " $prozent
	}
	
	
	#Wie viele Noten liegen unter/über dem Schnitt
	
	
	$zahl = 0
	#Ermittelt die Zahl besser als der Durchschnitt
	for($i = 1; $i -lt $durschnitt;$i++)
	{
	    $zahl = $haeufigkeit[$i]   
	
	}
	
	$besser = $zahl
	$schlechter = $noten.Count - $zahl
	
	Write-Host "besser sind $besser"
	Write-Host "schlechter sind $schlechter" 
	
	
	#Sortieren der Noten
	#Über den Hilfsarray Hauefigkeit
	
	$AusgabeString = "|"
	
	for($i = 1; $i -lt $haeufigkeit.Count; $i++)
	{
	   $AusgabeString += [string]$i * $haeufigkeit[$i] + "|"
	
	} 
	
	$AusgabeString




  


