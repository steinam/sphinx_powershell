Funktionen
==========


.. _loesung_dir:

- Schreiben Sie eine Funktion, die als Parameter einen Pfad erwartet und anschließend den Inhalt des Pfades ausgibt

	.. code-block:: sh
	
		function listDirectory($pfad)
		{
	   
		cd $pfad
		Dir $pfad |Out-File  C:\temp\Ausgabe.txt
	
		}
    
    
.. _loesung_dir_rekursiv:
		
- Erweitern Sie die obige Funktion um eine rekursive Variante. Sollte der Pfad Unterordner besitzen, sollen auch diese ausgegeben werden.

	.. code-block:: sh
		
			function listDirectory($pfad)
			{
		   
			cd $pfad
			Dir $pfad -recurse |Out-File  C:\temp\Ausgabe.txt
		
			}
    

.. _loesung_dir_rekursiv_extension:

- Erweitern Sie die obige Funktion: Es soll jetzt möglich sein, nach bestimmten Dateikennungen die Ausgabe zu gestalten 

	.. code-block:: sh
	
		function listDirectory($pfad, $ext)
		{
			cd $pfad
			Dir $pfad -Recurse  -include *.$ext |Out-File  C:\temp\Ausgabe.txt
		}

- Wie überprüfen Sie, ob ein Array einen bestimmten Wert enthält

	
	Folgende Funktion überprüft zwei Arrays auf Gleichheit
	
	
	
	.. code-block:: sh
	
		function AreArraysEqual($a1, $a2) {
			if ($a1 -isnot [array] -or $a2 -isnot [array]) {
			  throw "Both inputs must be an array"
			}
			if ($a1.Rank -ne $a2.Rank) {
			  return $false 
			}
			if ([System.Object]::ReferenceEquals($a1, $a2)) {
			  return $true
			}
			for ($r = 0; $r -lt $a1.Rank; $r++) {
			  if ($a1.GetLength($r) -ne $a2.GetLength($r)) {
					return $false
			  }
			}
		
			$enum1 = $a1.GetEnumerator()
			$enum2 = $a2.GetEnumerator()   
		
			while ($enum1.MoveNext() -and $enum2.MoveNext()) {
			  if ($enum1.Current -ne $enum2.Current) {
					return $false
			  }
			}
			return $true
		} 

- Folgendes Powershell-Skript berechnet den Wochentag mit Hilfe eines eigenen Algorythmus. Verbessern Sie die Lesbarkeit des Skriptes, indem Sie die verschiedenen Teile des Quellcode mit Hilfe von Funktionen kapseln.

	.. code-block:: sh
	
		<#
		.SYNOPSIS
		Fragt nach dem Teil eines Datums
	
		.DESCRIPTION
		Dient zum Erfassen der Bestandteile eines Datums. Macht keine Sicherheitsüberprüfungen 
		
		.PARAMETER Frage
		Specifiziert die dem User zu stellende Frage.
	
		.OUTPUTS
		System.String. Gibt die von der Eingabeaufforderung übergebenen Zeichen zurück
	
		.LINK
		Read-Host
		#>
		function get-DatumsTeil([string]$Frage)
		{
			return Read-Host -prompt $Frage
		}
		
				
		function set-Monat()
		{
			if($m -le 2)
			{
			   $m += 10;
				$j -= 1;
			}
			else
			{
			   $m -= 2;
			}
		
			[int]$c = $j/100;
			[int]$y = $j%100;
		
			[int]$h = (((26 * $m -2)/10) + $t + $y + $y/4 + $c/4 -2 * $c)%7
			
			if($h -lt 0)
			{
				$h +=7;
			}
			
			return $h
		}
		
		
		function get-Day($value)
		{
		   [string]$Tag = "";
			switch($h)
			{
			   0 { $Tag = "Sonntag"; break}
							
			   1 { $Tag = "Montag"; break}
		
			   2 { $Tag = "Dienstag"; break}
		
			   3 { $Tag = "Mittwoch"; break}
		
			   4 { $Tag = "Donnerstag";break}
		
			   5 { $Tag = "Freitag"; break}
		
			   6 { $Tag = "Samstag"; break}
			} 
			
			return $Tag
		}
		
		
		$t = get-DatumsTeil("Geben Sie den Tag ein")
		$m = get-DatumsTeil("Geben Sie den  Monat ein")
		$j = get-DatumsTeil("Geben Sie das Jahr ein")
		
		$MerkeMonat = $m
		$h = set-Monat
		$Tag = get-Day($h)
		Write-Host "Der " + $t +"." + $MerkeMonat + "." + $j + " ist ein " + $Tag
		
		
		
- Schreiben Sie eine Funktion, die ihnen die Größe des vorhandenen physikalischen und virtuellen RAMs ausgibt. Benutzen Sie zum Ermitteln der Größe die WMI-Klasse WIN32_OperatingSystem und deren Eigenschaften **FreePhysicalMemory** sowie **FreeVirtualMemory**. Als kleine "Zugabe" möchten Sie die Angabe der Größe in verschiedenen Formaten (Byte, KByte, MByte) ermöglichen. 


.. code-block:: sh

	function get-memory ([string]$units = "b") {
		 $comp = get-wmiobject Win32_OperatingSystem
		 $mem = $comp.FreePhysicalMemory + $comp.FreeVirtualMemory
		 switch ($units) {
			  "b" {$mem}
			  "k" {[int]($mem / 1024)}
			  "m" {[int](($mem * 100) / 1048576) / 100}
		 }
	}

	
	

- Schreiben Sie eine Funktion zum Umbenennen eines Computernamens. Der Name soll entweder per Eingabeaufforderung übergeben werden können oder aus einer Textdatei gelesen werden, falls keine Eingabe vorgenommen wird. Kommentieren Sie die Funktion über die Powershell-typische Hilfesyntax. Prüfen Sie innerhalb der Funktion, ob Sie überhaupt die administrativen Rechte für die Umbenennung haben. Zum Abschluss des Vorgangs müssen Sie einen Reboot vornehmen.

siehe: http://www.gangleri.net/2009/11/25/ChangingComputerNameAndWorkgroupWithPowerShell.aspx

.. code-block:: sh

	function Set-ComputerName
	{
		<#
		.SYNOPSIS
			Sets the name of the computer
		.DESCRIPTION
			Uses WMI to set the name of the computer, if the name is successfully changed the user will be prompted to reboot and apply the changes
		.NOTES
			File Name: ComputerName.psm1
			Aurthor: Alan Bradley
			Requires: PowerShell 2.0
		.LINK
			http://www.gangleri.net
		.EXAMPLE
			Set-ComputerName -Name "{New name for computer}"
		.EXAMPLE
			Set-ComputerName "{New name for computer}"
		.EXAMPLE
			Set-ComputerName -Name "{New name for computer}" -AutoReboot
		.EXAMPLE
			Set-ComputerName "{New name for computer}" -AutoReboot
		.PARAMETER Name
			The new name that the computer will be known by
		#>
		param(
			[Parameter(Position=0, Mandatory=$true,ParameterSetName="Name")]
			[string]$Name,
			[Switch]$AutoReboot
		)
		process
		{
			$sysInfo = Get-WmiObject -Class Win32_ComputerSystem
			$result = $sysInfo.Rename($Name)
		 
			switch($result.ReturnValue)
			{       
				0
				{
					Write-Host -ForegroundColor Green "Success"
					if($AutoReboot -eq $false)
					{                   
						$yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes", 
							"Reboots the machine."
						$no = New-Object System.Management.Automation.Host.ChoiceDescription "&No",
							"Does not reboot the machine, you will manually have to reboot this machine for changes to take effect."                         
						$options = [System.Management.Automation.Host.ChoiceDescription[]]($yes, $no)   
						$ShouldReboot = $Host.UI.PromptForChoice("Reboot machine", 
							"You must reboot for change to take effect. Do you want to reboot now?", $options, 0)
					}
					if($ShouldReboot -eq 0 -or $AutoReboot -eq $true)
					{
						Restart-Computer
					}
				}
				5 { Write-Host -ForegroundColor Red "This cmdlet must be execute with administrative privileges" }           
				default { Write-Host -ForegroundColor Red "Error" }
			}
		}
	}



.. code-block:: sh

	<#
	.SYNOPSIS
		Renames a computer
	.DESCRIPTION
		This Script will rename a computer by passing the oldComputername and the new Computername. If no names are given, a textfile will be openend 
	.EXAMPLE
		.\Set-Computername.ps1 'oldName' 'newName'
	.PARAMETER originalPCName
		Old name of the computer
	.PARAMETER computerName
		new name of the computer
	.NOTES
		FileName     : Download-File.ps1
		Author       : Steinam
		LastModified : 06 Apr 2011 9:39 AM PST
	#Requires -Version 2.0
	#>
	function Set-ComputerName {
		param([switch]$help,
			  [string]$originalPCName=$(read-host "Please specify the current name of the computer"),
			  [string]$computerName=$(read-host "Please specify the new name of the computer"))
			
		$usage = "set-ComputerName -originalPCname CurrentName -computername AnewName"
		if ($help) {Write-Host $usage;break}
		
		$computer = Get-WmiObject Win32_ComputerSystem -OriginalPCname OriginalName -computername $originalPCName
		$computer.Rename($computerName)
		}
    
			
		#set-Computername		
		$computer = Get-WmiObject Win32_ComputerSystem
		$computer | Format-List -Property N*
	
	

- Powershell kann mit Hilfe des CommandLets **Get-Eventlog** die Ereignisanzeige von lokalen und remote-Computern anzeigen und filtern. Schreiben Sie eine Funktion *getLogInfos*, die Ihnen folgende Auswahlmöglichkeiten zulässt:
	- Wahl des Computers
	- Wahl des jeweiligen EventLogs
	- Wahl des jeweiligen Eintrags, z.B. Error, Warnig, Information
	
	Informieren Sie sich mit Hilfe von get-help über die weiteren Möglichkeiten von Get-Eventlog

	.. code-block:: sh
	
		function getLogInfo([string]$Computer=".", [string]$EventLog, [string]$ErrorType)
		{
			
		
		}
	

- Verändern von Attributen einer Datei

  Sie haben als Administrator einige Dateien, auf die sie von Zeit zu Zeit zugreifen müssen. Um die Dateien vor versehentlichen Änderungen zu schützen, versehen Sie diese mit ReadOnly - Flag. Leider ist aber jetzt ein Bearbeiten dieser Dateien mühsam, weil Sie nun immer erst dieses Flag ändern müssen.
  
  Schreiben Sie ein parametrisiertes Skript, welches alle Dateien eines Ordners ReadOnly bzw. Nicht-ReadOnly setzen kann. Schreiben Sie zusätzlich eine Funktion, die Ihnen alle Dateien mit ReadOnly-Flag auflistet.
  Eine Hilfe-Funktion soll Sie bei einer falschen Eingabe unterstützen.
  
  .. code-block:: sh
  
  	http://blogs.technet.com/b/heyscriptingguy/archive/2009/09/24/hey-scripting-guy-september-24-2009.aspx
  
  	Param ([string]$path='c:\fso',
       [switch]$get,
       [switch]$clear,
       [switch]$set,
       [switch]$help
	)#end param
		 
	# *** Functions here ***
	
	Function Get-ReadOnlyFiles([string]$path)
	{
	 "Getting readonly attribute on files in $path"
	 Get-ChildItem -Path $path |
	 Where-Object { $_.attributes -match 'readonly' }
	} #end get-Readonlyfiles
	
	Function Clear-ReadOnlyFiles([string]$path)
	{
	  "Clearing readonly attribute from files in $path"
	 New-Variable -Name read_only -Value 1 -Option readonly
	 Get-ChildItem -Path $path |
	 Where-Object { $_.attributes -match 'readonly' } |
	 ForEach-Object {
	   $_.attributes = $_.attributes -Bxor $read_only }
	}#end Clear-ReadonlyFiles
	
	Function Set-ReadOnlyFiles([string]$path)
	{
	 "Setting readonly attribute on files in $path"
	 New-Variable -Name read_only -Value 1 -Option readonly
	 Get-ChildItem -Path $path |
	 ForEach-Object {
	   $_.IsReadOnly = $True 
	 }
	}#end Set-ReadOnlyFiles
	
	Function Get-HelpText
	{
	 $helpText = @"
	  WorkWithReadOnlyFiles.ps1 -path c:\fso -get
	  Gets a list of all readonly files in c:\fso folder
	 
	  WorkWithReadOnlyFiles.ps1 -path c:\fso -set
	  Sets all files in the c:\fso folder to readonly
	 
	  WorkWithReadOnlyFiles.ps1 -path c:\fso -clear
	  Clears the readonly attribute from all files in c:\fso folder
	"@
	 $helpText
	}#end Get-HelpText
	

	# *** Entry Point to Script ***
	#$clear = $true
	if($help) { Get-HelpText ; exit }
	if(!$path) { "A path is required." ; Get-HelpText ; exit }
	if($get) { Get-ReadOnlyFiles -path $path ; exit }
	if($clear) { Clear-ReadOnlyFiles -path $path ; exit }
	if($set) { Set-ReadonlyFiles -path $path ; exit }

	
- Ein Außendienstmitarbeiter erhält ein neues Firmen-Laptop. Auf seinem alten Laptop sind viele WLAN-Profile gespeichert und Sie suchen einen Weg, um diese Profile auf den neuen Rechner ohne manuelles Neuanlegen zu übernehmen. Ein Kollege erzählt Ihnen, dass Sie mit Hilfe des Befehls netsh Profile sowie exportieren als auch importieren können.

  - Informieren Sie sich über die Möglichkeiten des Befehls netsh
  - Schreiben Sie ein Skript, welches sowohl den Export als auch den Import der WLAN-Profile ermöglicht.
			
  	.. code-block:: sh
  
		function Export-WLAN {
		<#
		.SYNOPSIS
			Exports all-user WLAN profiles
		.DESCRIPTION
			Exports all-user WLAN profiles to Xml-files to the specified directory using netsh.exe
		.PARAMETER XmlDirectory
			Directory to export Xml configuration-files to
		.EXAMPLE
			Export-WLAN -XmlDirectory c:\temp\wlan
		#>
		
		[CmdletBinding()]
			param (
				[parameter(Mandatory=$true)]
				[string]$XmlDirectory
				)
		
		#Export all WLAN profiles to specified directory
		$wlans = netsh wlan show profiles | Select-String -Pattern "Profil für alle Benutzer" | Foreach-Object {$_.ToString()}
		$exportdata = $wlans | Foreach-Object {$_.Replace("    Profil für alle Benutzer : ",$null)}
		$exportdata | ForEach-Object {netsh wlan export profile $_ $XmlDirectory}
		}
	
		function Import-WLAN {
		<#
		.SYNOPSIS
			Imports all-user WLAN profiles based on Xml-files in the specified directory
		.DESCRIPTION
			Imports all-user WLAN profiles based on Xml-files in the specified directory using netsh.exe
		.PARAMETER XmlDirectory
			Directory to import Xml configuration-files from
		.EXAMPLE
			Import-WLAN -XmlDirectory c:\temp\wlan
		#>
	
		[CmdletBinding()]
			param (
				[parameter(Mandatory=$true)]
				[string]$XmlDirectory
				)
		
		#Import all WLAN Xml-files from specified directory
		Get-ChildItem $XmlDirectory | Where-Object {$_.extension -eq ".xml"} | ForEach-Object {netsh wlan add profile filename=($XmlDirectory+"\"+$_.name)}
		}
		
- Aufruf von Remote-Desktop-Sitzungen

	Erstellen Sie ein Skript, welches eine rdp-Verbindung unter Berücksichtigung verschiedener Optionen des mstsc-Befehls ermöglicht.
	
	.. code-block:: sh
	
		    ########################################################################################################################
			# NAME
			#     Start-RDP
			#
			# SYNOPSIS
			#     Opens a remote desktop connection to another computer.
			#
			# SYNTAX
			#     Start-RDP [[-Server] <string>] [[-Width] <int>] [[-Height] <int>]
			#     Start-RDP -Path <string> [[-Width] <int>] [[-Height] <int>]
			#
			# DETAILED DESCRIPTION
			#     The Start-RDP cmdlet opens a new Remote Desktop connection using the Microsoft Terminal Services Client.
			#     Connection settings can be specified by argument or read from a standard RDP file.
			#
			# PARAMETERS
			#     -Server <string>
			#         Specifies the name of the server to connect to.  May also include an IP address, domain, and/or port.
			#
			#         Required?                    false
			#         Position?                    1
			#         Default value                
			#         Accept pipeline input?       true
			#         Accept wildcard characters?  false
			#
			#     -Width <int>
			#         Specifies the desired width of the resolution for the connection (for non-full screen connections).
			#
			#         Required?                    false
			#         Position?                    2
			#         Default value                
			#         Accept pipeline input?       false
			#         Accept wildcard characters?  false
			#
			#     -Height <int>
			#         Specifies the desired height of the resolution for the connection (for non-full screen connections).
			#
			#         Required?                    false
			#         Position?                    3
			#         Default value                
			#         Accept pipeline input?       false
			#         Accept wildcard characters?  false
			#
			#     -Path <string>
			#         Specifies the path to an RDP file to connect with (resolution settings can be overridden using the
			#         -Width and -Height parameters.
			#
			#         Required?                    false
			#         Position?                    4
			#         Default value                
			#         Accept pipeline input?       true
			#         Accept wildcard characters?  false
			#
			#     -Console <SwitchParameter>
			#         Connect to a Windows Server 2003 console session.
			#
			#         Required?                    false
			#         Position?                    named
			#         Default value                false
			#         Accept pipeline input?       false
			#         Accept wildcard characters?  false
			#
			#     -Admin <SwitchParameter>
			#         Connect to a Windows Server 2008 administrator session.
			#
			#         Required?                    false
			#         Position?                    named
			#         Default value                false
			#         Accept pipeline input?       false
			#         Accept wildcard characters?  false
			#
			#     -Fullscreen <SwitchParameter>
			#         Open connection in full screen mode.
			#
			#         Required?                    false
			#         Position?                    named
			#         Default value                false
			#         Accept pipeline input?       false
			#         Accept wildcard characters?  false
			#
			#     -Public <SwitchParameter>
			#         Run Remote Desktop in public mode.
			#
			#         Required?                    false
			#         Position?                    named
			#         Default value                false
			#         Accept pipeline input?       false
			#         Accept wildcard characters?  false
			#
			#     -Span <SwitchParameter>
			#         Span the Remote Desktop connection across multiple monitors.  Each monitor must have the same height
			#         and be arranged vertically.
			#
			#         Required?                    false
			#         Position?                    named
			#         Default value                false
			#         Accept pipeline input?       false
			#         Accept wildcard characters?  false
			#
			# INPUT TYPE
			#     String,System.IO.FileInfo
			#
			# RETURN TYPE
			#    
			#
			# NOTES
			#
			#     -------------------------- EXAMPLE 1 --------------------------
			#
			#     C:\PS>Start-RDP
			#
			#
			#     This command opens the Terminal Services Client connection dialog to specify a connection.
			#
			#
			#     -------------------------- EXAMPLE 2 --------------------------
			#
			#     C:\PS>Start-RDP -Server myserver -Width 1024 -Height 768
			#
			#
			#     This command opens a new Remote Desktop connection to the server named "myserver" in a window with 1024x768 resolution.
			#
			#
			#     -------------------------- EXAMPLE 3 --------------------------
			#
			#     C:\PS>Start-RDP -Server myserver -Fullscreen
			#
			#
			#     This command opens a new full screen Remote Desktop connection to the server named "myserver".
			#
			#
			#     -------------------------- EXAMPLE 4 --------------------------
			#
			#     C:\PS>Start-RDP -Path C:\myserver.rdp
			#
			#
			#     This command opens a new Remote Desktop connection using the specified RDP file.
			#
			#
			 
			#Function global:Start-RDP {
				param(
					[string]$Server = "",
					[int]$Width = "",
					[int]$Height = "",
					[string]$Path = "",
					[switch]$Console,
					[switch]$Admin,
					[switch]$Fullscreen,
					[switch]$Public,
					[switch]$Span
				)
			 
				begin {
					$arguments = ""
					$dimensions = ""
					$processed = $false
			 
					if ($admin) {
						$arguments += "/admin "
					} elseif ($console) {
						$arguments += "/console "
					}
					if ($fullscreen) {
						$arguments += "/f "
					}
					if ($public) {
						$arguments += "/public "
					}
					if ($span) {
						$arguments += "/span "
					}
			 
					if ($width -and $height) {
						$dimensions = "/w:$width /h:$height"
					}
				}
			 
				process {
					Function script:executePath([string]$path) {
						Invoke-Expression "mstsc.exe '$path' $dimensions $arguments"
					}
					Function script:executeArguments([string]$Server) {
						Invoke-Expression "mstsc.exe /v:$server $dimensions $arguments"
					}
			 
					if ($_) {
						if ($_ -is [string]) {
							if ($_ -imatch '\.rdp$') {
								if (Test-Path $_) {
									executePath $_
									$processed = $true
								} else {
									throw "Path does not exist."
								}
							} else {
								executeArguments $_
								$processed = $true
							}
						} elseif ($_ -is [System.IO.FileInfo]) {
							if (Test-Path $_.FullName) {
								executePath $_.FullName
								$processed = $true
							} else {
								throw "Path does not exist."
							}
						} elseif ($_.Path) {
							if (Test-Path $_.Path) {
								executePath $_.Path
								$processed = $true
							} else {
								throw "Path does not exist."
							}
						} elseif ($_.DnsName) {
							executeArguments $_.DnsName
							$processed = $true
						} elseif ($_.Server) {
							executeArguments $_.Server
							$processed = $true
						} elseif ($_.ServerName) {
							executeArguments $_.ServerName
							$processed = $true
						} elseif ($_.Name) {
							executeArguments $_.Name
							$processed = $true
						}
					}
				}
			 
				end {
					if ($path) {
						if (Test-Path $path) {
							Invoke-Expression "mstsc.exe '$path' $dimensions $arguments"
			 
						} else {
							throw "Path does not exist."
						}
					} elseif ($server) {
						Invoke-Expression "mstsc.exe /v:$server $dimensions $arguments"
					} elseif (-not $processed) {
						Invoke-Expression "mstsc.exe $dimensions $arguments"
					}
				}
			#}
			
- Setzen Sie mit Hilfe der PS die IP-Adresse eines Rechners. Geben Sie auch die Daten für Gateway, SN-Mask, NIC, DNS-Server

	.. code-block:: sh
	
		    # corrected version - $mask variable corrected to match in both places
		    function Set-IPAddress {
                param(  [string]$networkinterface =$(read-host "Enter the name 		   
                		of the NIC (ie Local Area Connection)"),
                		[string]$ip = $(read-host "Enter an IP Address (ie 10.10.10.10)"),
                		[string]$mask = $(read-host "Enter the subnet mask (ie 255.255.255.0)"),
                		[string]$gateway = $(read-host "Enter the current name of the NIC you want to rename"),
                		[string]$dns1 = $(read-host "Enter the first DNS Server (ie 10.2.0.28)"),
                		[string]$dns2,
                		[string]$registerDns = "TRUE"
				)
                    
				$dns = $dns1
                if($dns2){$dns ="$dns1,$dns2"}
                $index = (gwmi Win32_NetworkAdapter | where {$_.netconnectionid -eq $networkinterface}).InterfaceIndex
                $NetInterface = Get-WmiObject Win32_NetworkAdapterConfiguration | where {$_.InterfaceIndex -eq $index}
                $NetInterface.EnableStatic($ip, $mask)
                $NetInterface.SetGateways($gateway)
                $NetInterface.SetDNSServerSearchOrder($dns)
                $NetInterface.SetDynamicDNSRegistration($registerDns)
                   
            }

            
- Mit Hilfe des Befehls **net view \\UNC-Name** können Sie die Freigaben eines Rechners ermitteln. Schreiben Sie eine Funktion, die als Parameter einen UNC-Namen und einen speziellen Freigabetyp erwartet. Die Funktion soll dann die gefundenen Freigaben zurückgeben. 

.. code-block:: sh

	

	function Get-ShareNames([string]$server, [string]$ShareType)
	{
	<#
		.SYNOPSIS
			Get-Sharenames
	
		.DESCRIPTION
			Gets all printers from specified server
			using intern the net view command
		   
		.Parameter Server
			Specified Servername who holds the shares
		   
		.Parameter ShareType
			Specify type of shares, e.g. "Drucker" or "Platte"
	
	
	#>
	
		$result = net view $server | Select-String $ShareType
		#$result = net view "\\win-srv11" | Select-String "Drucker"
	
		$ausgabe = @()
	
	
		foreach($element in $result)
		{
			 #nächste Zeile ist notwendig, da $element kein string
			 #sondern ein matchInfo-Objekt ist
			 #Wahrscheinlich wegen dem Select-String-ComandLet
			 $zeile = $element.toString()
			 $name = $zeile.split("Drucker")
			 $ergebnis = $name[0].TrimEnd()
			 $ausgabe += $ergebnis
		}
	
	
		return $ausgabe
	
	}
	
	
	Get-Sharenames "\\win-srv11" "Drucker"
	
	
	Get-ShareNames

