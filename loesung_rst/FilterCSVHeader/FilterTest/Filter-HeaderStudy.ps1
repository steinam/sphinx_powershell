#Filter Function filters out records that start with "Time"
Filter Filter-Header
{
    if($_ -match "^Time")
    {
        $_ | out-null
    }
    else
    {
        $_
    }
}

cd "C:\Users\Klaus\Desktop\TO PLANET\WeatherStation\FilterTest"
  
#Initializing yearly file
$YearlyFileName = "Weather2009.csv"
#Set the header only once in the yearly file
Set-Content -Path $YearlyFileName -value "Time,TemperatureF,DewpointF,PressureIn,WindDirectionDegrees,WindSpeedMPH,WindSpeedGustMPH,Humidity,HourlyPrecipIn" -force -encoding "UTF8"
        
#Open all csv files get-content and add it to the yearly file       
$Files = dir -Path "C:\Users\Klaus\Desktop\TO PLANET\WeatherStation\FilterTest" -Filter "*.csv"

#Filter in Action
$Files | ForEach-Object `
{
    Get-Content -Path $_.Name -Encoding "UTF8" | Filter-Header | Add-Content -path $YearlyFileName -Encoding "UTF8"
}


<#
################ so gehts auch

cd "C:\FilterTest"

#Initializing yearly file
$YearlyFileName = "C:\temp\Weather2013.txt"
#Set the header only once in the yearly file
Set-Content -Path $YearlyFileName -value "Time,TemperatureF,DewpointF,PressureIn,WindDirectionDegrees,WindSpeedMPH,WindSpeedGustMPH,Humidity,HourlyPrecipIn" -force -encoding "UTF8"

#Open all csv files get-content and add it to the yearly file
$Files = dir -Path "C:\FilterTest" -Filter "*.csv"

#Filter in Action
$Files | ForEach-Object `
{
        Get-Content -Path $_.Name -Encoding "UTF8" | Where-Object {$_ -notmatch "^Time"} | Add-Content -path $YearlyFileName -Encoding "UTF8"
}

#>

