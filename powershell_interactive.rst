.. include:: /Abbrev.txt

PowerShell im Interaktiv-Modus
******************************
 
|PS| hat 2 Gesichter: zum Einen kann es zum Erstellen von Skripten dienen, zum Anderen kann man damit sofort Befehle ausführen.


PS als Rechner
==============

|PS| kann auf der Konsole wie ein normaler Rechner verwendet werden. 

.. code-block:: sh

	2 + 4 (Enter)
	6
	
Alle grundlegenden arithmethischen Operationen sowie Klammerrechnung sind möglich:

.. code-block:: sh

	(12+5) * 3 / 4.5 (Enter)
	11.3333333333333
	
	(Dir *.txt).Count (Enter)
	12
	
	
Ein Komma statt eines Punktes scheint ein falsches Ergebnis zu bringen

.. code-block:: sh

	4,2 + 2 (Enter)

	4
	2
	2

	4.2 + 2 (Enter)
	
	6,2
	
	
.. index:: Array
	
Kommas erzeugen immer einen Array. Der Punkt dient bei Gleitkommazahlen als Trennzeichen





Rechnen mit Zahlensystemen und Einheiten
========================================

Eine interessante Erweiterung in |PS| ist die Möglichkeit, mit den gängigen Einheiten der IT-Branche direkt zu rechnen. Powershell unterstützt die Größen GB, MB, KB. Sie müssen direkt hinter dem Wert stehen

.. code-block:: sh

	4GB / 720MB (Enter)
	5.68888888888889

	1kb
	
Powershell erkennt hexadezimale Zaheln, wenn Sie mit 0x eingeleitet werden

.. code-block:: sh

	12 + 0xAF (Enter)
	187
	
	0xAFFE (Enter)
	45054

Siehe Übersicht arithmetische Operatoren. :ref:`mathematischeoperatoren`:
	
Ausführen externer Befehle
==========================

|PS| kann wie die klassische Shell auch beliebige externe Befehle ausführen. 

Selbst die alte Konsole kann gestartet werden.

.. code-block:: sh

	cmd (Enter)
	


.. image:: images/ps_chg_konsole.jpg

Sicherheitseinschränkungen
--------------------------

.. image:: images/ps_notepad_wordpad.png

Wie die Abbildung zeigt, kann |PS| offenbar nicht jedes Programm starten. Powershell sucht die Programme in der PATH-Umgebungsvariable. Dies sind sog. "**vertrauenswürdige**" Pfade. 


.. index:: cmdlet

Cmdlets: Powershell-eigene Befehle
==================================

Powershell's interne Befehle werden "cmdlet" genannt. Sie bestehen häufig aus der Kombination eines Verbs mit einem Befehl, z.B. **Get-Comamnd**.

.. code-block:: sh

	Get-Command -commandType cmdlet
	


Folgende Tabelle listet die wichtigsten Möglichkeiten auf:

.. image:: images/cmdlet.jpg

Über das CmdLet **Get-Help** kann zu jedem Befehl eine ausführliche Hilfe herbeigeholt werden.

.. code-block:: sh

	Get-Help Get-Command -detailed
	
	
	
.. index:: Alias

Aliase
======

|PS| kennt für bestimmte Befehle Abkürzungen, was als Alias bezeichnet wird. Diese können anstelle der häufig langen Befehle eingegeben werden.

.. code-block:: sh

	get-alias get-command

Dateien und Skripte aufrufen
=============================

Im Regelfall werden Powershell-Skripte in einem Editor geschrieben und entweder direkt darin ausgeführt oder die gespeicherten Skriptdateien über die Powershell-Konsole aufgerufen. Dem Aufruf der Skriptdatei muss in der Konsole der **"."** vorangehen bzw. der komplette Pfad zur Datei angegeben werden.

.. image:: images/skript_console.jpg


 
