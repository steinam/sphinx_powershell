.. include:: Abbrev.txt

Funktionen
**********


.. sidebar:: Zusammenfassung

    :Release: |release|
    :Datum: |today|
    :Autor: **Steinam**
    :Target: Schüler FI SYS
    :status: In Bearbeitung
    :vollständig: 20 %

	.. todo::     
		
		Die urls noch komplett einarbeiten
		Einarbeiten des pdfs

	
.. topic:: Zusammenfassung

	Die untenstehenden Äußerungen sind eigentlich eine Demonstration bzw. Aufgabe und sollten als Aufgabe ausgelagert werden


Häufig ist es sinnvoll, seinen Quellcode in abgeschlossenen Strukturen zu definieren. Die rein sequenzielle Abfolge wird ersetzt durch den Aufruf von Programmcontainern, die eine gewisse Funktionalität bereitstellen. Auf diese Funktionalität kann dann innerhalb des Skriptes immer wieder zugegriffen werden.






Folgende Elemente werden beinhaltet:

+ Funktionskopf (Signatur)
+ Rückgabewerte
+ Parameterübergabe


Grundsätzlicher Aufbau
======================

.. index:: Funktion, function

Funktionen sind selbst definierte Kommandos, die im Prinzip drei Aufgaben haben:

- Shorthand: Abkürzungen für einfache Befehle zur Arbeitserleichterung 
- Combining: Funktionen können Arbeit erleichtern, indem Sie mehrere Arbeitsschritte zusammenfassen können
- Kapselung und Erweiterung:  

Der grundlegende Aufbau einer Funktion ist immer gleich.

Nach dem Funktion-Statement folgt der Name der Funktion und anschließpend der Powershell-Codeblock in geschweiften Klammern. Ein Beispiel:

.. code-block:: sh

	Function Cd.. { Cd .. }
	Cd..

	#Für immer wiederkehrende Aufgaben
	
	Function myPing { ping.exe -w 100 -n 1 10.10.10.10 }
	myPing
	Pinging 10.10.10.10 with 32 bytes of data:
	Reply from 88.70.64.1: destination host unreachable.

	#mit einem Argument
	Function myPing { ping.exe -w 100 -n 1 $args }
	myPing www.microsoft.com
	Pinging lb1.www.ms.akadns.net [207.46.193.254] with 32 bytes of data:
	Request timed out.
	Ping statistics for 207.46.193.254:
    Packets: Sent = 1, Received = 0, Lost = 1 (100% Loss),


Gerade bei längeren Funktionen kann die Eingabe innerhalb einer Zeile etwas umständlich sein. Eingaben können in |PS| allerdings über mehrere Zeilen gehen. Dies erreicht man durch Drücken von Return, wenn die jeweils aktuelle Zeile noch nicht fertig geschreiben ist.

.. image:: images/funktionen_auf_konsole.jpg

Spätestens jetzt wird der Einsatz eines Editors bzw. der grafischen Version der |PS| überlegenswert.

Eine Darstellung des gesamten Funktion kann man mit folgender Zeile erreichen

.. code-block:: sh
	
	$function:NextFreeDrive
	
.. image:: images/function_nextfreedrive.jpg


Die Ausgabe einer Funktion in eine Textdatei kann man z.B. mit dem Out-File-Commandlet erreichen

.. code-block:: sh

	$function:NextFreeDrive | Out-File -Encoding utf8 nextfreedrive.ps1
	notepad .\nextfreedrive.ps1
	
.. image:: images/function_nextfreedrive_script.jpg



Übergabe von Argumenten
=======================

.. index:: Argumentübergabe

Häufig will man einer Funktion Informationen mit übergeben, die diese dann weiterverarbeiten soll. Dies wird mit Hilfe von sog. **Argumenten** bewerkstelligt. Es gibt 4 verschiedene Arten der Argumentübergabe


.. index:: single: Arguments; Arbitrary

**Arbitrary arguments:** 
	die $args variable (Array) beinhaltet alle Parameter, die einer Funktion übergeben werden. Damit ist sie eine gute Lösung, wenn eine flexible Anzahl von Parametern übergeben werden soll.
	
	.. code-block:: sh
	
		function add()
		{
			$i = 0
			foreach($_ in $args)
			{
				$i = $i +  $_
			
			}
			
			Write-Host $i
		
		}
	
	
	
.. index:: single: Arguments; Named	
	
**Named arguments:** 
	Eine Funktion kann auch pro Parameter einen Namen vergeben. Damit ist die Reihenfolge der Parameter beliebig, weil Sie über den Namen aufgelöst werden. 
	
	.. code-block:: sh
	
		function add1($value1, $value2)
		{	
			Write-Host ($value1 + $value2)
		}
		add1 -value2 10 -value1 10
		
		
	.. image:: images/function_named_parameters.png
		
		 
	

.. index:: single: Arguments; Predefined
	
**Predefined arguments:**
	Parameter können mit default-Werten versehen sein. Falls der Benutzer keine eigenen Werte übergibt, werden die default-Werte genommen.
	
	.. code-block:: sh
	
		function add2($value1=10, $value2=20)
		{	
			Write-Host ($value1 + $value2)
		}
		
		add -value2 10 -value1 10
		
		
	.. image:: images/function_default_value.png
	

.. index:: single: Arguments; Typed	

**Typed arguments:** 
	Parameter können mit einem bestimmten Datentyp definiert werden, um sicherzustellen, dass nur die Argumente aus "richtigen" Datentypen bestehen. 
	
	.. code-block:: sh

		function add([int] $value1, [int] $value2)
		{
			$value1 + $value2
		}
		
	
	.. image:: images/function_type_misinterpretation.png	


.. index:: single: Arguments; Special	
	
**Special argument types:**
	aside from conventional data types, arguments can also act like a switch: if a switch (i.e., the name of the argument) is specified, the argument has the $true value. 


.. index:: Rückgabewert
	
Rückgabewerte
=============

In |PS| geben Funktionen nie nur einen Wert zurück sondern immer alles. Wenn man die Funktion lediglich aufruft, gibt die Funktion die Ausgabe über die Konsole aus. Die Ausgabe kann aber auch in einer Variablen gefangen werden.

.. code-block:: sh

	function add3($value1=10, $value2=20)
	{
		$value1 + $value2	
	}

	$result = add3 -value1 10 -value2 10
	$result

.. image:: images/function_returnvalue.png


**Solange die Funktion nur einen Wert zurückliefert, ist der Rückgabetyp quasi eine Variable. Falls mehrere Ausgaben erfolgen würden, wird die Ausgabe in einen Array gekapselt. Dieser kann von der aufrufenden Seite aus beliebig angepackt werden.**	
	

.. code-block:: sh

	function VAT([double]$amount=0)
	{
		$amount * 0.19
	}
	# An interactively invoked function
	# output results in the console:
	VAT 130.67
	24.8273
	# But the result of the function can
	# also be assign to a variable:
	$result = VAT 130.67
	$result
	24.8273
	# The result is a single number value
	# of the "double" type:
	$result.GetType().Name
	Double

	Function VAT([double]$amount=0)
	{
		$factor = 0.19
		$total = $amount * $factor
		"Value added tax {0:C}" -f $total
		"Value added tax rate: {0:P}" -f $factor
	}

	The function returns two results:
	VAT 130.67
	Value added tax $24.83
	Value added tax rate: 19.00%
	# All results are stored in a single variable:
	$result = VAT 130.67
	$result
	Value added tax $24.83
	Value added tax rate: 19.00%
	# Several results are automatically stored in an array:
	$result.GetType().Name
	Object[]
	# You can get each separate result of the
	# function by using the index number:
	$result[0]
	Value added tax $24.83
	# The data type of the respective array element
	# corresponds to the included data:
	$result[0].GetType().Name
	String

.. image:: images/function_return_array.png
	

.. index:: Return	
	
Return Statement
================

Das RETURN-Statement hat in der |PS| nicht die selbe Bedeutung wie in anderen Programmiersprachen, da Funktionen immer alles zurückgeben. Es wurde dennoch implementiert, vor allem aus 2 Gründen:

Style:
	Man folgte damit den Vorgaben anderer Programmiersprachen, die ebenfalls ein RETURN-Statement haben
	
Beenden einer Funktion:
	Return beenedet eine Funktion; alle Ausgaben unterhalb des RETURN-Statements würden von der Funktion nicht zurückgegeben werden.
	
.. code-block:: sh

	Function Add([double]$value1, [double]$value2)
	{
		# This time the function returns a whole
		# series of oddly assorted results:
		"Here the result follows:"
		1
		2
		3
		# Return also returns a further result:
		return $value1 + $value2
		
		# This statement will no longer be executed
		# because the function will exit when return is used:
		"Another text"
	}
	Add 1 6
	Here the result follows:
	1
	2
	3
	7
	$result = Add 1 6
	$result
	Here the result follows:
	1
	2
	3
	7


	
Textausgeben verhindern
=======================

Häufig hat man bestimmte Ausgaben nur für Kontrollzwecke bzw. Debug-Ausgaben in eine Funktion integriert. Im Produktiveinsatz will man diese Ausgaben nicht sehen und benötigt sie auch nicht.

Folgendes Beispiel zeigt mögliche Alternativen:

.. code-block:: sh

	Function Test
	{
		"Calculation will be performed"
		$a = 12 * 10
		"Result will be emitted"
		"Result is: $a"
		"Done"
	}
	Test
	Calculation will be performed
	Result will be emitted
	Result is: 120
	Done

	$result = Test
	$result

Für diesen Zweck gibt es mehrere Möglichkeiten.


.. index:: Write-Host

Write-Host benutzen:
	Das Cmdlet Write-Host gibt die Ausgabe sofort an die Konsole weiter

	.. code-block:: sh
		
		Function Test
		{
			Write-Host "Calculation will be performed"
			$a = 12 * 10
			Write-Host "Result will be emitted"
			"Result is: $a"
			Write-Host "Done"
		}
		# This time your debugging reports will already
		# be output when the function is executed:
		$result = test
		Calculation will be performed
		Result will be emitted
		Done
		# The result will no long include your debugging reports:
		$result
		Result is: 120

		
.. index:: Debug-Write, $DebugPreference 		
	
Debug-Ausgabe benutzen
	Mit Hilfe des Write-Debug-Cmdlets können Ausgaben nur zu Debug-Zwecken ausgegeben werden. Voraussetzung st allerdings, dass man die globale Variabel $DebugPrefeence auf "SilentlyContinue" stellt

	.. code-block:: sh
	
		Function Test
		{
			Write-Debug "Calculation will be performed"
			$a = 12 * 10
			Write-Debug "Result will be emitted"
			"Result is: $a"
			Write-Debug "Done"
		}
		# Debugging reports will remain completely
		# invisible in the production environment:
		$result = Test
		# If you would like to debug your function,
		# turn on reporting:
		$DebugPreference = "Continue"
		# Your debugging reports will now be output
		# with the "DEBUG:" prefix and output in yellow:
		$result = Test
		DEBUG: Calculation will be performed
		DEBUG: Result will be emitted
		DEBUG: Done
		# They are not contained in the result:
		$result
		Result is: 120
		# Everything is running the way you wish;
		# turn off debugging:
		$DebugPreference = "SilentlyContinue"
		$result = Test

.. index:: $errorActionPreference 		
		
Fehlermeldungen unterdrücken:
	Fehlermeldungen werden normalerweise immer sofort ausgegeben, was natürlich den Ablauf eines Skriptes stören kann. Auch hier gibt es wieder eine globale Variable, welches die Ausgabe von Fehlermeldungen steuern kann.

	.. code-block:: sh
	
		Function Test
		{
			# Suppress all error messages from now on:
			$ErrorActionPreference = "SilentlyContinue"
			Stop-Process -name "Unavailableprocess"
			# Immediately begin outputting all error messages again:
			$ErrorActionPreference = "Continue"
			1/$null
		}
		# Error messages will be suppressed in certain
		# areas but not in others:
		$result = Test

	
.. index:: Inline-Help
		
Inline-Help von Funktionen
============================

.. TODO:: Hilfebeschreibung für Funktionen erstellen

Um den Nutzer einer Funktion auch innerhalb der Powershell mit Informationen über die Funktion versorgen zu können, kann man sowohl Funktionen als auch das ganze Skript mit einer sog. **Inline-Help** ausstatten. Im Grunde handelt es sich dabei um eine Abfolge von Kommentarzeilen, die mit bestimmten Schlüsselworten versehen worden sind.

Details der Anwendung gibt es unter http://technet.microsoft.com/en-us/library/dd819489.aspx


.. code-block:: sh

	<# 
	 .SYNOPSIS 
		 Dieses Skript gibt eine Liste der nicht funktionierenden HArdware aus mit Hilfe von WMI. 
	 .DESCRIPTION  
		 Per WMI werden zunächst die Systemdetails ermittelt und dann die Hardware mit Fehlern. 
	 .NOTES 
		 File Name  : Get-BrokenHardware.ps1 
		 Author     : Karl Steinam - teetscher 
		 Requires   : PowerShell Version 2.0 
	 .LINK 
		 Siehe auch: 
			 http://www.meineFirma.de 
	 .EXAMPLE 
		 PSH [C:\foo]: Get-BrokenHardware.ps1 
		 Computer Details: 
		 Manufacturer: Dell Inc. 
		 Model:        Precision WorkStation T7400 
		 Service Tag:  6Y84C3J 
	  
		 Hardware thats not working list 
		 Description:  WD My Book Device USB Device 
		 Device ID:    USBSTOR\OTHER&VEN_WD&PROD_MY_BOOK_DEVICE&REV_1010\7&2A4E07C&0&575532513130303732383932&1 
		 Error ID:     28 
	 #> 
	  
	 # Display Computer details 
	 "Computer Details:" 
	 $comp = gwmi Win32_ComputerSystem 
	 "Manufacturer: {0}" -f $comp.Manufacturer 
	 "Model:        {0}" -f $comp.Model 
	 $computer2 = Get-WmiObject Win32_ComputerSystemProduct 
	 "Service Tag:  {0}" -f $computer2.IdentifyingNumber 
	 "" 
	  
	 #Get hardware that is errored 
	 "Hardware that's not working list"  
	 $broken = Get-WmiObject Win32_PnPEntity | where {$_.ConfigManagerErrorCode -ne 0} 
	  
	 #Display broken hardware 
	 foreach ($obj in $broken){   
	 "Description:  {0}" -f  $obj.Description 
	 "Device ID:    {0}" -f  $obj.DeviceID 
	 "Error ID:     {0}" -f  $obj.ConfigManagerErrorCode 
	 "" 
	 } 


.. image:: images/inline-help_1.jpg

.. image:: images/inline-help_2.jpg

Das nächste Beispiel fügt einer Funktion eine Inline-Help hinzu.

.. code-block:: sh
	
	<#
		.SYNOPSIS 
		Adds a file name extension to a supplied name.

		.DESCRIPTION
		Adds a file name extension to a supplied name. 
		Takes any strings for the file name or extension.

		.PARAMETER Name
		Specifies the file name.

		.PARAMETER Extension
		Specifies the extension. "Txt" is the default.

		.INPUTS
		None. You cannot pipe objects to Add-Extension.

		.OUTPUTS
		System.String. Add-Extension returns a string with the extension or file name.

		.EXAMPLE
		C:\PS> extension -name "File"
		File.txt

		.EXAMPLE
		C:\PS> extension -name "File" -extension "doc"
		File.doc

		.EXAMPLE
		C:\PS> extension "File" "doc"
		File.doc

		.LINK
		http://www.fabrikam.com/extension.html

		.LINK
		Set-Item
		#>
	function Add-Extension2 
	{
		param ([string]$Name,[string]$Extension = "txt")
		$name = $name + "." + $extension
		$name

	
	}

Je nach Laune kann die Inline-Help auch innerhalb der Funktion geschrieben werden.

	  
Größeres Beispiel bzgl. Funktionen
==================================

Siehe: 

http://www.powershellpro.com/powershell-tutorial-introduction/powershell-scripting-with-wmi/

http://www.powershellpro.com/computernames-activedirectory/149/

http://www.computerperformance.co.uk/powershell/powershell_example_basic.htm#Example_4:_PowerShell_Real-life_Task

http://www.powershellpro.com/powershell-tutorial-introduction/powershell-scripting-with-wmi/

http://technobeans.wordpress.com/2010/11/25/powershell-getting-inventory-details/

**Vorausetzung:**

Installiertes CIM-Studio zur Abfrage des WMI

Im folgenden Beispiel sollen mit Hilfe von WMI verschiedene Informationen des lokalen Rechners abgerufen werden. In weiteren Schritten wird die Suche auf beliebige Rechner ausgeweitet. Im letzten Schritt werden Informationen des ActiveDirectory genutzt, um eine komfortable Liste der Rechner zu erhalten.


**Scenario:** 

Ihr Chef will eine Inventur der Hardware aller Server und Rechner im Netzwerk. Insbesondere will er folgende Informationen.

    * Machine manufacturer, model number, and serial number.
    * BIOS information to determine if updates are required,
    * OS type
    * CPU information: Manufacturer, type, speed, and version.
    * Amount of memory in each server.
    * Disk information: Size, interface type, and media type.
    * Network Information: IP settings and MAC address.


.. index:: WMI
    
Benutzung von WMI
=================

WMI ist im Grunde eine Datenbank von Informationen, das auf jeden Windows System vorhanden ist. Über den sog. WMI-Service kann man die jeweils gewünschten Daten abrufen.

.. image:: images/cimcpu.JPG


Von Powershell aus ist die Vorgehensweise relativ einfach: :-)

.. code-block:: sh  

    Get-WmiObject -List -Namespace "root\CIMV2"

.. image:: images/wmi_classes.JPG
    
Eine etwas kleinere Ausgabe gibt es, wenn man die Ausgabe auf bestimmte Inhalte beschränkt, in unserem Beispiel 

.. code-block:: sh  

    Get-WmiObject Win32_ComputerSystem | Format-List *

    
.. image:: images/wmi_win32_suche.png    
    

Welche Informationen entsprechen den Anforderungen ?

    * Manufacturer = Manufacturer property.
    * Model Number = Model property.
    * Serial Number = Nicht vorhanden; sie muss anderweitig gesucht werden
    * Gesamtspeicher = TotalPhysicalMemory-Eigenschaft existiert in der Klasse und genügt unseren Ansprüchen.


**Schritt 1: Machine manufacturer, model number, and serial number**

.. code-block:: sh
   :linenos:
   
	#sets the computer to local
	$strComputer ="."
	
	#Creates a variable called $colItems which contains the WMI Object
	$colItems = Get-WmiObject Win32_ComputerSystem -Namespace "root\CIMV2" -ComputerName $strComputer
	
	#Use a foreach loop to iterate the $colItems (collection).
	#Store the properties information in the $objItem Variable.
	foreach($objItem in $colItems) {
		#Use the Write-Host cmdlet to output required property information
		Write-Host "Computer Manufacturer: " $objItem.Manufacturer
		Write-Host "Computer Model: " $objItem.Model
		Write-Host "Total Memory: " $objItem.TotalPhysicalMemory "bytes"
	}


	
**Schritt 2: BIOS-Information**	
	
Die entsprechende Klasse im WMI ist Win32_BIOS

.. code-block:: sh
	
	Get-WmiObject Win32_BIOS | Format-List *
	
.. code-block:: sh
   :linenos:

   $strComputer = "."

	$colItems = Get-WmiObject Win32_BIOS -Namespace "root\CIMV2" -computername $strComputer
	foreach($objItem in $colItems) 
	{
		Write-Host "BIOS:"$objItem.Description
		Write-Host "Version:"$objItem.SMBIOSBIOSVersion"."
		$objItem.SMBIOSMajorVersion"."$objItem.SMBIOSMinorVersion
		Write-Host "Serial Number:" $objItem.SerialNumber
	}

**Schritt 3: Übrige Informationen**

*OS TYPE:*

.. code-block:: sh
   :linenos:

	$strComputer = "."
	$colItems = Get-WmiObject Win32_OperatingSystem -Namespace "root\CIMV2" -Computername $strComputer
	
	foreach($objItem in $colItems) {
		Write-Host "Operating System:" $objItem.Name
	}

	
*CPU Info:*

.. code-block:: sh
   :linenos:

	$strComputer = "."
	
	$colItems = Get-WmiObject Win32_Processor -Namespace "root\CIMV2" -Computername $strComputer
	
	foreach($objItem in $colItems) {
		Write-Host "Processor:" $objItem.DeviceID $objItem.Name
	}

	
*DISK Info:*

.. code-block:: sh
   :linenos:

	$strComputer = "."

	$colItems = Get-WmiObject Win32_DiskDrive -Namespace "root\CIMV2" -ComputerName $strComputer
	
	foreach($objItem in $colItems) 
	{
		Write-Host "Disk:" $objItem.DeviceID
		Write-Host "Size:" $objItem.Size "bytes"
		Write-Host "Drive Type:" $objItem.InterfaceType
		Write-Host "Media Type: " $objItem.MediaType
	}
	

*NETWORK Info:*

.. code-block:: sh
   :linenos:

	$strComputer = "."
	
	$colItems = Get-WmiObject Win32_NetworkAdapterConfiguration -Namespace "root\CIMV2"
	-ComputerName $strComputer | where{$_.IPEnabled -eq "True"}
	
	foreach($objItem in $colItems) {
		Write-Host "DHCP Enabled:" $objItem.DHCPEnabled
		Write-Host "Subnet Mask:" $objItem.IPSubnet
		Write-Host "Gateway:" $objItem.DefaultIPGateway
		Write-Host "MAC Address:" $ojbItem.MACAddress
	}
	
.. index:: Datei, Alias, notation, Random


**Kapseln der Code-Schnipsel in Funktionen**

Zur übersichtlicheren Handhabung werden wir die einzelnen Codeelemente in Funktionen kapseln. Alle Funktionen werden in die Datei ServerInventory.ps1 geschrieben.

.. code-block:: sh
   :linenos:

	#* FileName:  ServerInventory.ps1
	#*=============================================================================
	#* Script Name: [ServerInventory]
	#* Created:     [07/08/2010]
	#* Author:      Karl Steinam
	#* Company:     Inuit Corp.
	#* Email:
	#* Web:         http://www.inuit.com
	#* Reqrmnts:
	#* Keywords:
	#*=============================================================================
	#* Purpose:    Server Hardware Inventory
	#*=============================================================================
	
	#*=============================================================================
	#* REVISION HISTORY
	#*=============================================================================
	#* Date:        [DATE_MDY]
	#* Time:        [TIME]
	#* Issue:
	#* Solution:
	#*=============================================================================
	
	#*=============================================================================
	#* FUNCTION LISTING
	#*=============================================================================
	#* Function:    SysInfo
	#* Created:     [12/14/07]
	#* Author:       Karl Steinam
	#* Arguments:
	#*=============================================================================
	#* Purpose: WMI Function that enumerate win32_ComputerSystem properties
	#*
	#*
	#*=============================================================================
	
	Function SysInfo {
	
	$colItems = Get-WmiObject Win32_ComputerSystem -Namespace "root\CIMV2" 
	-ComputerName $strComputer
	
	foreach($objItem in $colItems) {
		Write-Host "Computer Manufacturer: " $objItem.Manufacturer
		Write-Host "Computer Model: " $objItem.Model
		Write-Host "Total Memory: " $objItem.TotalPhysicalMemory "bytes"
	}
	
	}
	
	#*=============================================================================
	#* FUNCTION LISTING
	#*=============================================================================
	#* Function:    BIOSInfo
	#* Created:     [07/08/2010]
	#* Author:       Karl Steinam
	#* Arguments:
	#*=============================================================================
	#* Purpose: WMI Function that enumerate win32_BIOS properties
	#*
	#*
	#*=============================================================================
	
	Function BIOSInfo {
	
		$colItems = Get-WmiObject Win32_BIOS -Namespace "root\CIMV2" -computername $strComputer
		foreach($objItem in $colItems) {
			Write-Host "BIOS:"$objItem.Description
			Write-Host "Version:"$objItem.SMBIOSBIOSVersion"."
			$objItem.SMBIOSMajorVersion"."$objItem.SMBIOSMinorVersion
			Write-Host "Serial Number:" $objItem.SerialNumber
		}
	
	}
	
	#*=============================================================================
	#* FUNCTION LISTING
	#*=============================================================================
	#* Function:    OSInfo
	#* Created:     [07/08/2010]
	#* Author:       Karl Steinam
	#* Arguments:
	#*=============================================================================
	#* Purpose: WMI Function that enumerate win32_OperatingSystem properties
	#*
	#*
	#*=============================================================================
	
	Function OSInfo {
	
		$colItems = Get-WmiObject Win32_OperatingSystem -Namespace "root\CIMV2" -Computername $strComputer
		
		foreach($objItem in $colItems) {
			Write-Host "Operating System:" $objItem.Name
		}
	
	}
	
	#*=============================================================================
	#* FUNCTION LISTING
	#*=============================================================================
	#* Function:    CPUInfo
	#* Created:     [07/08/2010]
	#* Author:       Karl Steinam
	#* Arguments:
	#*=============================================================================
	#* Purpose: WMI Function that enumerate win32_Processor properties
	#*
	#*
	#*=============================================================================
	
	Function CPUInfo {
	
		$colItems = Get-WmiObject Win32_Processor -Namespace "root\CIMV2" -Computername $strComputer
		
		foreach($objItem in $colItems) {
			Write-Host "Processor:" $objItem.DeviceID $objItem.Name
		}
	
	}
	
	#*=============================================================================
	#* FUNCTION LISTING
	#*=============================================================================
	#* Function:    DiskInfo
	#* Created:     [07/08/2010]
	#* Author:       Karl Steinam
	#* Arguments:
	#*=============================================================================
	#* Purpose: WMI Function that enumerate win32_DiskDrive properties
	#*
	#*
	#*=============================================================================
	
	Function DiskInfo {
	
		$colItems = Get-WmiObject Win32_DiskDrive -Namespace "root\CIMV2" -ComputerName $strComputer
		
		foreach($objItem in $colItems) {
			Write-Host "Disk:" $objItem.DeviceID
			Write-Host "Size:" $objItem.Size "bytes"
			Write-Host "Drive Type:" $objItem.InterfaceType
			Write-Host "Media Type: " $objItem.MediaType
		}
	
	}
	
	#*=============================================================================
	#* FUNCTION LISTING
	#*=============================================================================
	#* Function:    NetworkInfo
	#* Created:     [07/08/2010]
	#* Author:       Karl Steinam
	#* Arguments:
	#*=============================================================================
	#* Purpose: WMI Function that enumerate win32_NetworkAdapterConfiguration
	#*         properties
	#*
	#*=============================================================================
	
	Function NetworkInfo {
	
		$colItems = Get-WmiObject Win32_NetworkAdapterConfiguration -Namespace "root\CIMV2" -ComputerName $strComputer | where{$_.IPEnabled -eq "True"}
		
		foreach($objItem in $colItems) {
			Write-Host "DHCP Enabled:" $objItem.DHCPEnabled
			Write-Host "IP Address:" $objItem.IPAddress
			Write-Host "Subnet Mask:" $objItem.IPSubnet
			Write-Host "Gateway:" $objItem.DefaultIPGateway
			Write-Host "MAC Address:" $ojbItem.MACAddress
		}
	
	}
	
In Powershell müssen die Funktionen vor der ersten Benutzung definiert worden sein. Der Aufruf des Funktionen erfolgt dann einfach **am Ende** des Skriptes. 

.. code-block:: sh
   :linenos:

	#*=============================================================================
	#* SCRIPT BODY
	#*=============================================================================
	#* Connect to computer
	$strComputer = "."
	
	#* Call SysInfo Function
	Write-Host "Sytem Information"
	SysInfo
	Write-Host
	
	#* Call BIOSinfo Function
	Write-Host "System BIOS Information"
	BIOSInfo
	Write-Host
	
	#* Call OSInfo Function
	Write-Host "Operating System Information"
	OSInfo
	Write-Host
	
	#* Call CPUInfo Function
	Write-Host "Processor Information"
	CPUInfo
	Write-Host
	
	#* Call DiskInfo Function
	Write-Host "Disk Information"
	DiskInfo
	Write-Host
	
	#* Call NetworkInfo Function
	Write-Host "Network Information"
	NetworkInfo
	Write-Host
	
	#*=============================================================================
	#* END OF SCRIPT: [ServerInventory]
	#*=============================================================================
		

**Abfrage eines anderen Computers**

Zur Zeit fragt das Skript nur die Daten des eigenen Computers ab. Um auch remote arbeiten zu können, muss die Variable *$strComputer* ersetzt werden.

.. code-block:: sh

	$strComputer = Read-Host "Enter Computer Name"
	Write-Host "Computer:" $strComputer


**Optimierungen**

Erzeugen Sie eine Textdatei und fügen Sie Computernamen ein. Erweiteren Sie das Skript so, dass es die Textdatei ausliest und den jeweiligen Computer abfragt.

.. image:: images/computers.JPG

.. code-block:: sh
   :linenos:

	#*=============================================================================
	#* SCRIPT BODY
	#*=============================================================================
	#* Create and array from C:\MyScripts\Computers.txt
	
	$arrComputers = get-Content -Path "C:\MyScripts\Computers.txt"
	
	foreach ($strComputer in $arrComputers)
	{ #Function Calls go here
	
		Write-Host "Computer Name:" $strComputer
		Write-Host "======================================"
		
		#* Call SysInfo Function
		Write-Host "Sytem Information"
		SysInfo
		Write-Host
		
		#* Call BIOSinfo Function
		Write-Host "System BIOS Information"
		BIOSInfo
		Write-Host
		
		#* Call OSInfo Function
		Write-Host "Operating System Information"
		OSInfo
		Write-Host
		
		#* Call CPUInfo Function
		Write-Host "Processor Information"
		CPUInfo
		Write-Host
		
		#* Call DiskInfo Function
		Write-Host "Disk Information"
		DiskInfo
		Write-Host
		
		#* Call NetworkInfo Function
		Write-Host "Network Information"
		NetworkInfo
		Write-Host "End of Report for $strComputer"
		Write-Host "======================================"
		Write-Host
		Write-Host
	
	} #End function calls.

	
**Auslesen der Computer aus dem AD**

Um sich die Eingabe der Computer in eine Textdatei zu ersparen, kann man auch innerhalb einer Windows-Domäne das ActiveDirectory befragen. Folgendes Skript liefert die Daten der Computernamen innerhalb des AD.




.. code-block:: sh
   :linenos:
	
	#GetPCNames.ps1
	
	$strCategory = "computer"

	$objDomain = New-Object System.DirectoryServices.DirectoryEntry

	$objSearcher = New-Object System.DirectoryServices.DirectorySearcher
	$objSearcher.SearchRoot = $objDomain
	$objSearcher.Filter = ("(objectCategory=$strCategory)")
	
	$colProplist = "name"
	foreach ($i in $colPropList)
	{
		$objSearcher.PropertiesToLoad.Add($i)
	}
	
	$colResults = $objSearcher.FindAll()
	
	foreach ($objResult in $colResults)
	{
		$objComputer = $objResult.Properties; $objComputer.name
	}
	

Speichern Sie das Skript als GetPCNames.ps1, starten Sie es innerhalb ihrer Domäne und speichern Sie das Ergebnis in der Datei Computers.txt 

.. code-block:: sh

	.\GetPCNames.ps1 > "C:\MyScripts\Computers.txt"


