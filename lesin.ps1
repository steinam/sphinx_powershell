﻿$strComputer ="."


function Manufacturer{
    
    $colItems = Get-WmiObject Win32_ComputerSystem -Namespace "root\CIMV2" -ComputerName $strComputer

    foreach($objItem in $colItems) { 
         
         $manufacturer = $objItem.Manufacturer
         $model = $objItem.Model
         $memory = $objItem.TotalPhysicalMemory
    }

    $information = @{"Manufacturer" = $manufacturer;"Model" = $model;"Memory"=$memory}

    return $information
}

function BIOS{
    
    $colItems = Get-WmiObject Win32_BIOS -Namespace "root\CIMV2" -computername $strComputer
     
    foreach($objItem in $colItems){
     
         $bios = $objItem.Description
         $version = $objItem.SMBIOSBIOSVersion, $objItem.SMBIOSMajorVersion, $objItem.SMBIOSMinorVersion
         $serialnumber= $objItem.SerialNumber
     }

     $biosinfo = @{"Bios" = $bios; "Version" = $version; "Serialnumber" = $serialnumber}

     return $biosinfo
}

function OStype{

    $colItems = Get-WmiObject Win32_OperatingSystem -Namespace "root\CIMV2" -Computername $strComputer
    
    foreach($objItem in $colItems) {
        
        $OS = @{"OS" = $objItem.Name}
    }
    return $OS
}

function CPU{
    
    $colItems = Get-WmiObject Win32_Processor -Namespace "root\CIMV2" -Computername $strComputer

    foreach($objItem in $colItems) {
        
        $cpu = @{"CPU"=$objItem.DeviceID, $objItem.Name}
    }

    return $cpu
}


function DISK{

    $colItems = Get-WmiObject Win32_DiskDrive -Namespace "root\CIMV2" -ComputerName $strComputer

    foreach($objItem in $colItems){

        $disk = $objItem.DeviceID
        $size = $objItem.Size
        $drivetype = $objItem.InterfaceType
        $mediatyp = $objItem.MediaType
    }
    
    $disk = @{"Disk" = $disk; "Size"=$size;"Drivetype"=$drivetype; "Mediatyp"=$mediatyp}
    
    return $disk
}

function NETWORK{

    $colItems = Get-WmiObject Win32_NetworkAdapterConfiguration -Namespace "root\CIMV2" -ComputerName $strComputer | where{$_.IPEnabled -eq "True"}

    foreach($objItem in $colItems) {
        
        $dhcp = $objItem.DHCPEnabled
        $ipadress = $objItem.IPAddress
        $subnetmask = $objItem.IPSubnet
        $gateway = $objItem.DefaultIPGateway
        $mac = $ojbItem.MACAddress
    }

    $network = @{"DHCP" = $dhcp; "IP" = $ipadress ;"Subnetmask" = $subnetmask; "Gateway" = $gateway; "MAC" = $mac}

    return $network
}

#Funktionen aufrufen und in einer Variablen speichern 

$Nutzerinformation =  Manufacturer
$Nutzerinformation += OStype
$Nutzerinformation += BIOS
$Nutzerinformation += CPU
$Nutzerinformation += DISK
$Nutzerinformation += NETWORK

$Nutzerinformation.
