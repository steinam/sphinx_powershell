Diagramme der strukturierten Programmierung
********************************************



Zur Darstellung von Sachverhalten der strukturierten Programmierung wurden vor 
vielen Jahren verschiedene Notationsformen entwickelt. Die wichtigsten sind  
der **Programmablaufplan** und das **Struktorgramm**.

Programmablaufplan
==================

.. image:: images/pap_notation.jpg
	

Struktogramm 
============

Ein Programm besteht aus einer Folge von Anweisungen, die häufig nicht
nur sequenziell(hintereinander) ablaufen, sondern die wiederholt oder
evtl. auch gar nicht ausgeführt werden sollen.
Viele Problemstellungen sind umfangreich und kompliziert und erfordern
deshalb eine systematische Vorarbeit. Beim Entwurf werden grafische
Darstellungsmittel für die Logik des Programmablaufes verwendet.

Die Logik eines Programmablaufes kann mit den untenstehenden Konzepten realisiert werden. 

-  Anweisung
-  Auswahl
-  Wiederholung

Anweisung
-----------

Die Anweisung ist der grundlegende Befehl innerhalb einer Programmiersprache. Im Struktogramm wird er durch ein Rechteck abgebildet. Durch die Folge von Anweisungen entsteht ein sequentieller Ablauf eines Programms.



Einseitige/Zweiseitige Auswahl
--------------------------------

Ein Programm kann, abhängig von einem Kriterium, eine Entweder-oder-Entscheidung 
treﬀen, d.h. entweder einen bestimmten Programmteil "A" ausführen oder einen 
anderen bestimmten Programmteil "B". Der nötige Code lässt sich sehr intuitiv 
verstehen. Sehen wir uns als Beispiel den Code an, der, abhängig vom Wert der 
Variablen note, den Text "Bestanden" oder "Durchgefallen" ausgibt:

.. code-block:: sh

	if ($note -lt 5) {
		Write-Host("Bestanden");
	} else {
		Write-Host("Durchgefallen");
	}
	Write-Host("Verzweigung beendet");
	
Zuvorderst steht ein neues Schlüsselwort: if (wenn). Dann folgt in Klammern
das Kriterium: "Wenn note kleiner als 5 ist". Wenn das der Fall ist, werden alle
Anweisungen in dem folgenden, von geschweiften Klammern eingeschlossenen
Block ausgeführt. Dann folgt ein weiteres Schlüsselwort else (sonst), gefolgt
von einem weiteren Block, der ausgeführt wird, wenn note eben nicht kleiner
als 5 ist. Danach ist die Verzweigung beendet, das heißt, der folgende Code wird
auf jeden Fall ausgeführt und hängt nicht mehr von der if-Bedingung ab. Im
Struktogramm sieht das so aus:


.. image:: images/kontrollstrukturen/note_zweiseitig.jpg


Man kann auch den else-Teil komplett weglassen. Wenn man zum Beispiel die
Meldung "Durchgefallen" nicht braucht, kann man schreiben:

.. code-block:: sh

	if ($note -lt 5) {
	Write-Host("Bestanden");
	}
	Write-Host("Verzweigung beendet");
	
	  
Das ist die sogenannte Einseitige Auswahl im Gegensatz zur Zweiseitigen Aus-
wahl, die wir eben kennengelernt haben. Das Struktogramm hat jetzt folgendes
Aussehen:

.. image:: images/kontrollstrukturen/note_einseitig.jpg


Wenn wir anders herum den "ja-Zweig" (korrekt würde man sagen: if-Zweig)
weglassen und nur den "nein-Zweig" (korrekt: else-Zweig) implementieren wollen, 
wird es etwas schwieriger. Wir wandeln unser Beispiel so ab, dass nur der Text
"Durchgefallen" ausgegeben werden soll. Möglich wäre folgendes:

.. code-block:: sh

	if ($note -lt 5) { //kein guter Stil
	} else {
		Write-Host("Durchgefallen");
	}

Das läuft zwar, ist aber kein guter Stil. Besser ist es, die Bedingung umzudrehen:

.. code-blocK:: sh

	if ($note -ge 5) {
		Write-Host("Durchgefallen");
	}

Kommen wir schließlich noch zur Bedingung, die in den Klammern steht. Was
darin steht, ist das Ergebnis der Rechenoperation note<5. Die Rechenoperation
ist eine sogenannte Vergleichsoperation und das Ergebnis ist ein Wahrheitswert.
Ein Wahrheitswert kann nur die beiden Werte wahr und falsch annehmen.


Die öﬀnende und die schließende geschweifte Klammer darf in der if-Anweisung
weggelassen werden. Dies ist jedoch immer schlechter Stil und kann leicht
zu schwer auffindbaren Fehlern führen. Werden die Klammern weggelassen, besteht der if-Block aus der Zeile, die der if-Anweisung folgt und der else-Block
besteht aus der Zeile, die der else-Anweisung folgt. Beispiel:

.. code-block:: sh

	if ($wert%2 -eq 0)
		Write-Host("Die Zahl ist gerade");
	
Die Gefahr darin zeigt sich in folgendem Codeausschnitt:

.. code-block:: sh

	if ($wert%2 -eq 0)
		Write-Host("Die Zahl ist gerade");
		Write-Host("Die Zahl ist durch 2 teilbar");

Entgegen dem Anschein wird die zweite WriteLine-Zeile auch bei ungeraden Zahlen
ausgeführt, denn Java verwendet wegen der fehlenden geschweiften Klammern
nur die erste WriteLine-Zeile für den if-Block. Dies ist ein nachträglich schwer
zu ﬁndender Fehler, der von vornherein vermieden werden kann, wenn man konsequent Klammern für den if und den else-Block setzt.
Ein weiteres schwer auﬃndbarer Fehler ist:

.. code-block:: sh

	if ($wert%2 -eq 0);
		Write-Host("Die Zahl ist gerade");

Diesen Code muss man folgendermaßen interpretieren: Falls die Bedingung wahr
ist, werden die Anweisungen bis zum nächsten Semikolon ausgeführt, also bis
zum Semikolon am Ende der if-Zeile. Anschließend ist die if-Verweigung zu
Ende. Die WriteLine-Anweisung wird also immer ausgeführt, gleichgültig ob wert
gerade oder ungerade ist.


**if-else-Kaskaden**

In einem Sonderfall lässt man teilweise die geschweiften Klammern aber doch
weg. Manchmal gibt es mehr als zwei F¨alle, die unterschiedlich behandelt werden
müssen. In diesem Fall schachtelt man mehrere if-Anweisungen ineinander und
erhält die sogenannte if-else-Kaskade. Im nachfolgenden Beispiel bauen wir die
Ausgabe einer Schulnote so weit aus, dass für jede Note ein individueller Text
ausgegeben wird.

.. code-block:: sh

	if ($note -eq 1) {
		Write-Host("sehr gut");
	} else if ($note -eq 2) {
		Write-Host("gut");
	} else if ($note -eq 3) {
		Write-Host("befriedigend");
	} else if ($note -eq 4) {
		Write-Host("ausreichend");
	} else if ($note -eq 5) {
		Write-Host("mangelhaft");
	} else {
		Write-Host("Fehler im Programm");
	}
	
	


Mehrseitige Auswahl
--------------------

In allen neueren Sprachen, gibt es eine Verzweigung, die, abhängig von einer
Integer-Variablen, einen von mehreren Programmblöcken anspringt. Das Notenprogramm, das im vorigen Kapitel mit einer if-else-Kaskade gelöst wurde,
ist ein gutes Beispiel dafür. Das Struktogramm dazu ist:



.. figure:: images/kontrollstrukturen/note_mehrseitig.jpg



**switch-Anweisung**

Die entsprechende Anweisung heißt in C# switch-Anweisung. In anderen Sprachen ist sie als case- oder select-Anweisung bekannt. Sie beginnt mit einer Zeile switch, gefolgt von der Variablen, deren Wert für die Verzweigung herangezogen wird:

.. code-block:: sh

	switch ($note) {

Dann folgt für jede Note ein sogenannte case-Block. Die erste Zeile eines case-
Blocks wird eingeleitet durch den Vergleichswert. Dann kommen die Anweisungen für den Block. Es gibt keine geschweiften Klammern. Ein Block wird mit dem Befehl break abgeschlossen.

.. code-block:: sh

	switch(note) {
	 1 {
		Write-Host("sehr gut");
		break;
	   }
	 2 {
		SWrite-Host("gut");
		break;
	   }
	 3 {
		Write-Host("befriedigend");
		break;
	   }
	 4 {
		Write-Host("ausreichend");
		break;
	   }
	 5 {
		Write-Host("mangelhaft");
		break;
	   }
	default{
		Write-Host("Fehler.");
		}
	}//switch
	
Am Ende der switch-Anweisung darf man noch einen sogenannten default-Block
unterbringen, der immer dann ausgeführt wird, wenn keiner der vorigen case-
Blöcke zutreﬀend war. Man kann ihn auch weglassen. Dann wird statt dessen
der switch-Block komplett übersprungen.
Die switch-Anweisung hat ihre Tücken. Vor allem darf man das break am
Ende nicht vergessen. Man kann das so verstehen: Die case- Zeilen sind An-
sprungstellen. Das heißt, wenn jetzt zum Beispiel die Note gleich 2 ist, wird
die Zeile mit case 2 angesprungen. Dann läuft das Programm Zeile für Zeile
weiter. Wird ein break erreicht, springt das Programm aus dem switch-Block
heraus.
Haben wir jetzt beispielsweise das break nach case 2 vergessen, dann läuft das
Programm einfach Zeile für Zeile weiter, bis der switch-Block zu Ende ist oder
ein break erreicht wurde. In unserem Beispiel würde dann

gut
befriedigend
ausgegeben.

Diesen Eﬀekt kann man durch geschickte Programmierung auch ausnutzen und
Zweige für ganze Bereiche deﬁnieren. Im folgenden Beispiel wird bei den Noten
1-4 der Text "bestanden" ausgegeben.

.. code-block:: sh

	switch(note) {
	case 1:
	case 2:
	case 3:
	case 4: Write-Host("bestanden");
	break;
	case 5: Write-Host("mangelhaft");
	break;
	default: Write-Host("Fehler.");
	}//switch


Wiederholung
---------------

**Zählschleife**

Die Zählschleife ist eine Schleifenart, bei der von Anfang an feststeht, wieviele Wiederholungen der Schleife ausgeführt werden. Es gibt einen Zähler (Laufvariable) der von einem Anfangswert bis zu einem Endwert läuft und sich bei
jedem Durchlauf um einen festen Betrag ändert. Das Struktogramm der Zählschleife ist:


.. figure:: images/kontrollstrukturen/wdholung_for.jpg
.. figure:: images/kontrollstrukturen/wdholung_for_alternative.jpg

Zählschleifen werden in Java mit dem Schlüsselwort *for* eingeleitet.

.. code-block:: sh

	for ($i=10; $i<=20; i++) {
		Write-Host(i);
	}
	
Dem Schlüsselwort folgt eine Parameterliste, die in einer runden Klammer zu-
sammengefasst ist und aus 3 Teilen besteht, die jeweils durch ein Semikolon
getrennt sind. Die 3 Teile sind:

#. Initialisierung der Laufvariablen.
#. Abbruchbedingung (kein Abbruch, solange die Bedingung erfüllt ist).
#. Veränderung der Laufvariablen.

Die Reihenfolge, in der die Teile abgearbeitet werden, veranschaulicht das fol-
gende Flussdiagramm:


.. figure:: images/kontrollstrukturen/wdholung_for_flussdiagramm.jpg


**Änderung der Laufvariablen im Schleifenkörper**

Es ist möglich, die Laufvariable im Schleifenkörper, also zwischen den geschweiften Klammern, zu ändern. Ein Beispiel dafür ist:

.. code-block:: sh

	for ($i = 10; §i -gt 0; i++) {
		$i=$i-2;
		Write-Host($i);
	}
Damit ist die Schleife aber keine reine Zählschleife mehr. In anderen Sprachen
(z.B. Pascal) ist das auch verboten. 


**Deklaration der Laufvariablen in der Schleife**

Es ist möglich, die Laufvariable im Schleifenkopf zu deklarieren:

.. code-block:: sh

	for ($i=10; $i -gt 0; i--)

Dann ist die Laufvariable nur in der Schleife gültig und kann nach Beendigung
der Schleife nicht mehr angesprochen werden. Diese Form ist die üblichste Form
einer for-Schleife. 




**Schleife mit Anfangsabfrage (Kopfgesteuerte Schleife)**

Bei manchen Schleifen steht zu Anfang die Anzahl der Durchläufe noch nicht
fest. Es kann sein, dass es mehrere Abbruchbedingungen gibt oder dass die Lauf-
variable ihre Werte unvorhersehbar verändern kann. Hier benutzt man entweder
die kopfgesteuerte oder die fußgesteuerte Schleife. Die kopfgesteuerte Schleife
hat das Aussehen

.. code-block:: sh

	while(Bedingung) {
		Anweisungs-Block
	}

Das bedeutet, dass der Anweisungsblock ausgeführt wird, solange die Bedingung
in der while-Zeile den Wert true ergibt. Das entsprechende Struktogramm hat
das Aussehen:


.. figure:: images/kontrollstrukturen/wdholung_while.jpg


Wir nehmen ein Countdown-Programm, dass von 100 rückwärts bis 10 zählt, aber alle durch 7 teilbaren Zahlen auslässt:

.. code-block:: sh

	$i=100;
	while ($i -gt 10) {
		Write-Host($i);
		$i--;
		if ($i%7 -eq 0) {
		   $i--; //Durch 7 teilbare Zahlen überspringen
		}
	}

**Schleife mit Endabfrage (Fußgesteuerte Schleife)**


Die Schleife mit Endabfrage ähnelt der Schleife mit Anfangsabfrage. Allerdings
wird die fußgesteuerte Schleife mindestens einmal durchlaufen, während demgegenüber die kopfgesteuerte Schleife gar nicht durchlaufen wird, wenn die Anfangsbedingung vor dem 1. Durchlauf nicht erfüllt ist. Die Schleife mit Endabfrage hat folgendes Aussehen:

.. code-block:: sh

	do {
	//Anweisungsblock
	} while (Bedingung)


Das Struktogramm ist:

.. figure:: images/kontrollstrukturen/wdholung_do.jpg
	

Das Countdown-Beispiel hat hier folgendes Aussehen:

.. code-block:: sh

	$i=10;
	do {
		Write-Host($i);
		$i--;
	} while ($i -gt 0);
	
	
In C und Java wird diese Schleife auch do-while-Schleife genannt, in Unter-
scheidung zur kopfgesteuerten while-Schleife. In Pascal spricht man stattdessen
von einer repeat-until-Schleife. Da die fußgesteuerte Schleife sich immer mit einer kopfgesteuerten nachbilden lässt, besitzen manche Sprachen (z.B. Fortran,
Python) keine fußgesteuerte Schleife.




**continue**

Innerhalb einer Schleife kann mit dem Befehl continue der aktuelle Schleifen-
durchlauf abgebrochen werden, d.h. das Programm wird mit dem Beginn des
nächsten Schleifendurchlaufs fortgesetzt. Die continue-Anweisung kann in allen
Schleifenvarianten benutzt werden. Das folgende Countdown-Programm ist mit
continue so abgewandelt, dass die Zahl 3 ausgelassen wird.

.. code-block:: sh

	for ($i=10; $i -gt 0; $i--) {
		if (4i -eq 3) {
			continue;
		}
		Write-Host($i);
	}

**break**

Der break-Befehl bewirkt, dass eine Schleife komplett abgebrochen wird. Das
Programm

.. code-block:: sh

	for ($i=10; $i -gt 0; $i--) {
		if ($i -eq 3) {
			break;
		}
		Write-Host($i);
	}

zählt nur bis zur Zahl 4 herunter.


break und continue wirken sich nur auf die jeweilige Schleife aus, in der sie definiert wurden.


**Mehrere verschachtelte Schleifen**

Mehrere verschachtelte Schleifen sind möglich, wie am nachfolgenden Beispiel
mehrerer verschachtelter for-Schleifen zu sehen ist. Es gibt ein Dreieck aus Sternen aus:

.. code-block:: sh


	*
	**
	***
	****
	*****

	$max=5;
	for ($i=0; i -lt $max; $i++) {
		for ($j = 0; $j -lt $i; $j++) {
			Write-Host("*");
		}
		Write-Host();
	} //for i
	
	
	



